# EyetoHand DOCKER

# FROM pytorch/pytorch:1.6.0-cuda10.1-cudnn7-runtime
ARG CUDA_VERSION=10.1
FROM nvidia/cuda:$CUDA_VERSION-cudnn7-devel-ubuntu16.04

ARG PYTHON_VERSION=3.7
ARG PYTORCH_VERSION=1.2

RUN apt-get update && apt-get install -y --no-install-recommends \
         build-essential \
         cmake \
         git \
         curl \
         git \
         ca-certificates \
         libjpeg-dev \
         libpng-dev \
         wget \
         sudo \
         unzip \
         vim && \
     rm -rf /var/lib/apt/lists/*


RUN curl -o ~/miniconda.sh https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
     chmod +x ~/miniconda.sh && \
     ~/miniconda.sh -b -p /opt/conda && \
     rm ~/miniconda.sh && \
     /opt/conda/bin/conda install -y python=$PYTHON_VERSION && \
     /opt/conda/bin/conda clean -ya
ENV PATH /opt/conda/bin:$PATH


RUN pip install torch==$PYTORCH_VERSION && \
    pip install torchvision==0.4.0
    # pip install --upgrade tensorflow

# Kaolin
WORKDIR /kaolin
RUN ls -a
RUN git clone https://github.com/NVIDIAGameWorks/kaolin.git .
COPY requirements.txt .
ENV KAOLIN_HOME "/kaolin"
RUN pip install -r requirements.txt
RUN TORCH_CUDA_ARCH_LIST="5.2 6.0 6.1 7.0 7.5+PTX" \
    # python setup.py develop
    pip install -e .
    # Enables modification of kaolin dir
WORKDIR /EyeToHand
# END of Kaolin

# PCL 1.8.1
COPY scripts/ .
RUN bash /EyeToHand/pcl_install.sh
# END of PCL

# Python-PCL
WORKDIR /
RUN git clone https://github.com/strawlab/python-pcl.git
WORKDIR /python-pcl
RUN git checkout v0.2.0
RUN sed -i -e 's/\"-1.7\"/\"-1.8\", \"-1.7\"/g' setup.py
RUN pip install -e .
WORKDIR /EyeToHand
# End of python-pcl


# Install LATEX 
RUN sudo apt-get install -y texlive-full
RUN sudo apt update
RUN sudo apt-get --reinstall install ttf-mscorefonts-installer --fix-missing
# To view .eps files on vs-code using eps-preview extension 
RUN sudo apt-get install pdf2svg