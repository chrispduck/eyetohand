# Eye to Hand - Code Library
## Overview
This repository is presented as part of my Masters thesis titled "Eye to Hand" on visuo-tactile recognition.
The library contains snippets of the entire pipeline: from the tactile control system,
 to the generation datasets, filtering of point clouds, and the subsequent cross-modal recognition. 
 


#### Control system
A ROS based tactile control system is presented for a panda 7 DOF manipulator ([more info](https://www.franka.de/technology)) 
for integration with the Cyskin capacitive tactile technology.
([further details](https://www.cyskin.com/cyskin-the-flexible-artificial-skin/)). *See the franka_tactile_control/README*
for more info on its usage.

#### Datasets & Generation
Both Tactile and Vision datasets are included in the raw and processed formts. 
Additional scripts for the generation of synthetic models are included, along with the unification filters.

#### Recognition
The training engine, ETH_classificaton_engine.py, is provided for the training and testing of models.
The implementation of the custom dataloaders, transforms and curriculum learning classes are provided.
All losses and accuracies for training, validation and testing are logged to a tensorboard generated in the recognition/runs/
 subdirectory.
Additionally, the confusion matrices and precision recall curves generated at each epoch and can be viewed in tensorboard or in the saved _.eps_ format. 


## Folders
* Camera & Segmentation - /camera
* Control System - /franka_tactile_control 
* Datasets  - /datasets
* Synthetic tactile model generation - /synthetic_data
* Filtering of point clouds - /unified_representation
* Recognition work with pointnet - /recognition

## Usage
* Please *unzip* all datasets in the /dataset folder before running code
* Visualisation scripts are provided in the camera, synthetic data and unified representation folders.
These python scripts are prefixed with `view'

#### Recognition:
To run the recognition engine.

 
 ```
 $ cd src/
 $ python recognition/ETH_classificaton_engine.py --tensorboad-name basic_pointnet_run
```
 
 And view the tensorboard results:
```
$ tensorboard --logdir=recognition/runs
```
The tensorboards for the best performing runs (using regular and curriculum training) are included in the recognition/runs directory,
 and can be viewed using the above tensorboard commmand.


<!-- ## Requirements
* python 3.7
* [pytorch 1.4](https://pytorch.org/) 
* [kaolin 1.0](https://github.com/NVIDIAGameWorks/kaolin) - only required for recognition module
* [python-pcl](https://github.com/strawlab/python-pcl) - only required for unified repr. module
* [PCL 1.9.0 (C++)](https://github.com/PointCloudLibrary) - only required for unified repr. module -->
 

## Installation
### Docker Container
Requires the installation of nvidia-docker2 - see [installation guide](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html). Note that nvidia-docker requires the Community Edition (CE) of docker - see [install guide](https://docs.docker.com/engine/install/ubuntu/). 

1) Build the container:
 `$ docker build . -t "eyetohand"`

2) Run the container run:
 `$ docker run -it --rm --gpus all -v "$(pwd)":/EyeToHand eyetohand`
3) Check the GPU is enabled run:
  `$ nvidia-smi`

4) Test the kaolin and pytorch builds: 
  `$ bash scripts/tests.sh`
