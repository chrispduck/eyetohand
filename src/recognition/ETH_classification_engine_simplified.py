from numpy.lib.shape_base import column_stack
import torch, argparse, pandas as pd
from PointNetMedium import PointNet
from kaolin.models import dgcnn, PointNet2 
from CloudDataloading import SingleModelDataset
import kaolin.transforms as tfs
import CloudTransforms as mytfs
from torch.utils.data import DataLoader
from PointClassificationEngine import ClassificationEngine as Engine
import matplotlib as mpl

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--vision-root', type=str, default='../datasets/single/vision/real/filtered_chopped/',
                        help='Root directory of the Vision dataset.')
    parser.add_argument('--tactile-root', type=str, default='../datasets/single/tactile/real/filtered/',
                        help='Root directory of the real Tactile dataset.')
    parser.add_argument('--gen-tactile-root', type=str, default='../datasets/single/tactile/gen/filtered/',
                        help='Root directory of the gen Tactile dataset.')
    parser.add_argument('--cats', type=str, nargs='+',
                        default=['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'shampoo', 'spam',  'tape'],
                        # default=['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'rugby_ball', 'soda_can', 'shampoo', 'spam', 'tape'],
                        help='list of object classes to use.')
    parser.add_argument('--num-points', type=int, default=1024,
                        help='Number of points to sample from meshes.')
    parser.add_argument('--epochs', type=int, default=80,
                        help='Number of train epochs.')
    parser.add_argument('-lr', '--learning-rate', type=float,
                        default=1e-2, help='Learning rate.')
    parser.add_argument('--batch-size', type=int,
                        default=12, help='Batch size.')
    parser.add_argument('--device', type=str, default='cuda',
                        help='Device to use.')
    parser.add_argument('--tensorboard-name', type=str, default='full_w_pictures',
                        help='The tensorboard filename')
    parser.add_argument('--training-noise', type=float, default=0.05,
                        help='Standard dev of noise added to all training samples')
    parser.add_argument('-scf', '--training-scaling', type=float, default=1.03,
                        help='Standard dev of noise added to all training samples')
    args = parser.parse_args()

    model = PointNet(classes=len(args.cats)).to(args.device)
    # model = dgcnn.DGCNN(output_channels=len(args.cats)).to(args.device)
    # model = PointNet2.PointNet2Classifier(num_classes=len(args.cats)).to(args.device)
    
    tf_train = tfs.Compose([
        # mytfs.SampleKMeans(p=1),
        mytfs.SamplePoints(scheme='random', nb_points=args.num_points, device=args.device),
        mytfs.RandomRotatePointCloud(device=args.device),
        mytfs.WhiteNoise(sd=args.training_noise, device=args.device),
        mytfs.RandomScalePointCloud(scf=args.training_scaling, device=args.device)
    ])
    tf_test = tfs.Compose([
        mytfs.SamplePoints(scheme='random', nb_points=args.num_points, device=args.device),
        mytfs.RandomRotatePointCloud(device=args.device),
    ])

    vision_dataset_train = SingleModelDataset(
        args.vision_root, args.cats, transform=tf_train, device=args.device, n_repeats=200)
    # vision_dataset_val = SingleModelDataset(
        # args.vision_root, args.cats, transform=tf_train, device=args.device, n_repeats=84)

    vision_dataset_val = SingleModelDataset(
            args.tactile_root, obj_names=args.cats, transform=tf_test, device=args.device, n_repeats=84)

    train_loader = DataLoader(
        vision_dataset_train, batch_size=args.batch_size, shuffle=True, drop_last=True)
    val_loader = DataLoader(
        vision_dataset_val, batch_size=args.batch_size, shuffle=True, drop_last=True)
    optimiser = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    criterion = torch.nn.CrossEntropyLoss().to(args.device)
    tensorboard_params = {
        'name': args.tensorboard_name,
        'dir': "runs/"+ args.tensorboard_name,
        'cats': args.cats,
        'nb_cats': len(args.cats)
    }
    engineparams = {
        'epochs': args.epochs
    }

    engine = Engine(model=model, train_loader=train_loader, val_loader=val_loader,
                    tensorboard=tensorboard_params,
                    optimizer=optimiser, device=args.device, engineparams=engineparams,
                    )
    try:
        mpl.rcParams['font.family'] = ['DejaVu Sans']
        mpl.rcParams['font.serif'] = ['Times']
        mpl.rcParams['font.size'] = 14

        engine.fit()
        engine.save_model()
        # engine.load_model('PN_CJ_V2T_n0.05_e50')

        # # gen Tactile test
        # print("Generated Tactile Test")
        # gen_tactile_dataset = SingleModelDataset(
        #     args.gen_tactile_root, obj_names=args.cats, transform=tf_test, device=args.device, n_repeats=200)
        # gen_tactile_dataloader = DataLoader(
        #     gen_tactile_dataset, batch_size=args.batch_size, shuffle=False, num_workers=0)
        # engine.predict(gen_tactile_dataloader, 'gen_filtered_tactile')

        # Real tactile test
        print("Filtered Tactile Test")
        real_tactile_dataset = SingleModelDataset(
            args.tactile_root, obj_names=args.cats, transform=tf_test, device=args.device, n_repeats=200)
        real_tactile_dataloader = DataLoader(real_tactile_dataset, batch_size=args.batch_size,
                                            shuffle=False, num_workers=0)
        engine.predict(real_tactile_dataloader, 'real_filtered_tactile')

        print("Raw Tactile Test")
        real_raw_tactile_dataset = SingleModelDataset(
            path="../datasets/single/tactile/real/raw/", obj_names=args.cats, transform=tf_test, device=args.device, n_repeats=200)
        real_raw_tactile_dataloader = DataLoader(real_raw_tactile_dataset, batch_size=args.batch_size)
        engine.predict(real_raw_tactile_dataloader, 'real_raw_tactile' )
        
        print("Vision Test")
        vision_dataset_test = SingleModelDataset(
            args.vision_root, args.cats, transform=tf_test, device=args.device, n_repeats=200)
        vision_test_loader = DataLoader(
            vision_dataset_test, batch_size=args.batch_size, shuffle=True, drop_last=True)
        final_val = engine.predict(vision_test_loader, "vision_filtered")


        ### Test over a range of proportions/ nb unique
        col_names = ["tensorboard_name", "scheme", "scheme-param", "tac_nof", "tac_filt", "vis_filt", 'final_val']
        df = pd.DataFrame(columns=col_names)
        # proportions = torch.linspace(1, 0.2, 1)
        # nb_points = torch.logspace(5,11,1,2) # 64->2048 in 13 in 13 steps.
#
        proportions = torch.linspace(1, 0.2, 17)
        nb_points = torch.logspace(4,10,13,2) # 64->2048 in 13 in 13 steps.

        # print("PROPORTIONS")
        # tf_test.transforms[0].scheme = "proportion"
        # for p in proportions:
        #     tf_test.transforms[0].proportion = float(p)
        #     # print(tf_test.transforms[0])
        #     # engine.visualise_batch(real_tactile_dataloader, savefig=True, fname='tac_filt_'+str(p))
        #     tac_nof = engine.predict(real_raw_tactile_dataloader,'tac_nof_'+str(p) )
        #     tac_filt = engine.predict(real_tactile_dataloader, 'tac_filt_'+str(p))
        #     vis_filt = engine.predict(vision_test_loader, 'vis_filt' +str(p))
        #     df_itr = pd.DataFrame([[args.tensorboard_name, 'proportion', float(p), tac_nof, tac_filt, vis_filt, final_val]], columns=col_names)
        #     df = pd.concat([df, df_itr])
        #     df.to_csv('runs/' + args.tensorboard_name + '/results.csv')

        print("NB-POINTS")
        tf_test.transforms[0].scheme = "random"
        # print(tf_test.transforms[0])
        for nb in nb_points:
            tf_test.transforms[0].nb_points = int(nb)
            # engine.visualise_batch(real_tactile_dataloader, savefig=True, fname='tac_filt_'+str(nb_points))
            tac_nof = engine.predict(real_raw_tactile_dataloader,'tac_nof_'+str(int(nb)) )
            tac_filt = engine.predict(real_tactile_dataloader, 'tac_filt_'+str(int(nb)))
            vis_filt = engine.predict(vision_test_loader, 'vis_filt' +str(int(nb)))
            df_itr = pd.DataFrame([[args.tensorboard_name, 'nb_points', int(nb), tac_nof, tac_filt, vis_filt, final_val]], columns=col_names)
            df = pd.concat([df, df_itr])
            df.to_csv('runs/' + args.tensorboard_name + '/results.csv')

    except KeyboardInterrupt:
        print("####### Keyboard interrupt detected #########")
        print("####### Saving board #########")
        engine.save_model()