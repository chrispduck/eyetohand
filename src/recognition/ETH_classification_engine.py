import argparse
import os

import kaolin.transforms as tfs
import torch
from kaolin.models.PointNet import PointNetClassifier
from kaolin.models.PointNet2 import PointNet2Classifier
from kaolin.models.dgcnn import DGCNN
from torch.utils.data import DataLoader

import CloudTransforms as mytfs
from CloudDataloading import CloudDataset, ComputePartition
from Curriculum import Curriculum
from PointClassificationEngine import ClassificationEngine as PCEngine

parser = argparse.ArgumentParser()
parser.add_argument('--vision-root', type=str, default=os.getcwd() + '/datasets/VisionProcessed',
                    help='Root directory of the Vision dataset.')
parser.add_argument('--tactile-root', type=str, default=os.getcwd() + '/datasets/TactileProcessed',
                    help='Root directory of the Tactile dataset.')
parser.add_argument('-model', type=str, default='PointNet',
                    help='Select PointNet, PointNet++ or DGCNN model')
parser.add_argument('--train-type', type=str, default='vision',
                    help='Choose training mode (tactile or vision)')
parser.add_argument('--cats', type=str, nargs='+',
                    default=['baseball', 'beer', 'camera_box', 'golf_ball', 'orange',
                             'pack_of_cards', 'rubix_cube', 'rugby_ball', 'soda_can', 'shampoo', 'spam', 'tape'],
                    help='list of object classes to use.')
parser.add_argument('--num-points', type=int, default=1024,
                    help='Number of points to sample from meshes.')
parser.add_argument('--epochs', type=int, default=45,
                    help='Number of train epochs.')
parser.add_argument('-lr', '--learning-rate', type=float,
                    default=1e-2, help='Learning rate.')
parser.add_argument('--split', type=float, default=0.8,
                    help='Cross validation partition, 0.8 gives 80% train 20% val')
parser.add_argument('--batch-size', type=int, default=1, help='Batch size.')
parser.add_argument('--device', type=str, default='cuda',
                    help='Device to use.')
parser.add_argument('--tensorboard-name', type=str, default='test_run',
                    help='The tensorboard filename')
parser.add_argument('--training-noise', type=float, default=0.05,
                    help='Standard dev of noise added to all training samples')
args = parser.parse_args()

""" Create Transforms for data - objects shared"""
tf_train = tfs.Compose([
    mytfs.SamplePoints(scheme='random', nb_points=args.num_points),
    mytfs.RandomRotatePointCloud(device=args.device),
    mytfs.WhiteNoise(sd=args.training_noise, device=args.device)
])

tf_val = tfs.Compose([
    mytfs.SamplePoints(scheme='random', nb_points=args.num_points),
    mytfs.RandomRotatePointCloud(device=args.device),
    mytfs.WhiteNoise(sd=0, device=args.device)
])

tf_test = tfs.Compose([
    mytfs.SamplePoints(scheme='random', nb_points=args.num_points),
    mytfs.RandomRotatePointCloud(device=args.device),
    mytfs.WhiteNoise(sd=0, device=args.device)
])

"""Load Vision and Tactile Datasets """
train_tactile = CloudDataset(args.tactile_root, categories=args.cats,
                             split='train', device=args.device, transform=tf_train)
train_vision = CloudDataset(args.vision_root, categories=args.cats, split='train', device=args.device,
                            transform=tf_train)
test_vision = CloudDataset(args.vision_root, categories=args.cats,
                           split='test', device=args.device, transform=tf_test)
test_tactile = CloudDataset(args.tactile_root, categories=args.cats, split='test', device=args.device,
                            transform=tf_test)
train_all = train_vision if args.train_type is 'vision' else train_tactile
if args.train_type is 'vision':
    print("vision training")
elif args.train_type is 'tactile':
    print("Tactile Training")
train_partition_lengths = ComputePartition(train_all, args.split)
train, val = torch.utils.data.random_split(train_all, train_partition_lengths)
val.dataset.transform = tf_val  # Validate with no noise

"""Create Dataloaders"""
train_loader = DataLoader(
    train, batch_size=args.batch_size, shuffle=True, drop_last=True)
val_loader = DataLoader(val, batch_size=args.batch_size,
                        shuffle=True, drop_last=True)
test_vision_loader = DataLoader(
    test_vision, batch_size=args.batch_size, shuffle=True, drop_last=True)
test_tactile_loader = DataLoader(
    test_tactile, batch_size=args.batch_size, shuffle=True, drop_last=True)

"""Prepare Engine"""
assert args.model in ['PointNet', 'PointNet++', 'DGCNN']
model = None
if args.model is 'PointNet':
    model = PointNetClassifier(num_classes=len(args.cats)).to(args.device)
elif args.model is 'PointNet++':
    model = PointNet2Classifier(num_classes=len(args.cats)).to(args.device)
elif args.model is 'DGCNN':
    model = DGCNN(output_channels=len(args.cats), k=20)
print(model)

optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
criterion = torch.nn.CrossEntropyLoss().to(args.device)
tb_params = {'name': args.tensorboard_name,
             'dir': os.getcwd() + '/recognition/runs/' + args.tensorboard_name,
             'cats': args.cats,
             'nb_cats': len(args.cats)}
engineparams = {'epochs': args.epochs, 'val_loss_threshold': 1e-3}
# curriculum_list = [(20, ('nb_points', 512)), (40, ('nb_points', 256))]
# curriculum_list = [(30, ('noise', 0.10)), (35, ('noise', 0.15)),
#                    (40, ('noise', 0.2))]
curriculum_list = [(0, ('noise', 0.05)), (20, ('noise', 0.10)), (35, ('noise', 0.15)),
                   (40, ('noise', 0.2))]
curriculum = Curriculum(tf_train, curriculum_list, mode='uniform')
engine = PCEngine(model=model, train_loader=train_loader, val_loader=val_loader,
                  tensorboard=tb_params,
                  optimizer=optimizer, device=args.device, engineparams=engineparams,
                  curriculum=curriculum)

""" Test real tactile data"""

engine.load_model('1_PointNet_V2T_e40_noise_0.05_run3')
dataset_root = 'datasets/filtered_tactile_objs/'
real_tactile = CloudDataset(dataset_root, categories=['baseball', 'golf_ball', 'pack_of_cards','tape'], split='test', device=args.device,
                            transform=tf_test)
real_tactile_loader = DataLoader(real_tactile, batch_size=args.batch_size, shuffle=True, drop_last=True)
# engine.visualise_batch()
batch = next(iter(real_tactile_loader))
# enginer.batc
# engine.model
# batch
print(batch)
engine.predict(real_tactile_loader, 'real_tactile')
# pred = enginer.model(data)

# engine.predict(r()

""" Regular Evaluation"""
# engine.visualise_batch()
# engine.fit()
# engine.save_model()
# engine.predict(test_vision_loader, 'vision')
# engine.predict(test_tactile_loader, 'tactile')

"""Experiment 2"""
""" part a (i) """
# For testing with different numbers of points:
# engine.load_model('1_PointNet_V2T_e40_noise_0.05_run3')
# accuracies = []
# for nb_points in [32, 64, 128, 256, 512, 1024, 2048, 4096]:
#     tf_test.transforms[0].nb_points = nb_points
#     name = 'tactile ' + str(nb_points)
#     accuracy = engine.predict(test_tactile_loader, name)
#     name = 'vision ' + str(nb_points)
#     accuracy = engine.predict(test_vision_loader, name)
#     accuracies.append(accuracy)
# print(accuracies)

"""part a (ii)"""
# tf2 = tfs.Compose([
#     mytfs.SamplePoints(scheme='proportion', proportion=100),
#     mytfs.RandomRotatePointCloud(device=args.device),
#     mytfs.WhiteNoise(sd=0.)
# ])
# accuracies = []
# test_tactile2aii = CloudDataset(args.tactile_root, categories=args.cats, split='test', device=args.device,
#                                 transform=tf2)
# test_tactile_loader2ii = DataLoader(test_tactile2aii, batch_size=args.batch_size, shuffle=True, drop_last=True)
# for nb_points in [10, 20, 30, 40, 50, 60, 70, 80, 90, 1000]:
#     tf.tforms[0].nb_points = nb_points
#     name = 'tactile ' + str(nb_points)
#     accuracy = engine.predict(test_tactile2aii, name)
#     accuracies.append(accuracy)
# print(accuracies)

"""2: Curriculum"""
# taccuracies, vaccuracies = [], []
# for nb_points in [32, 64, 128, 256, 512, 1024, 2048, 4096]:
#     tf_test.transforms[0].nb_points = nb_points
#     name = 'tactile ' + str(nb_points)
#     taccuracy = engine.predict(test_tactile_loader, name)
#     name = 'vision ' + str(nb_points)
#     vaccuracy = engine.predict(test_vision_loader, name)
#     taccuracies.append(taccuracy)
#     vaccuracies.append(vaccuracy)
# fname = os.getcwd() + '/runs/' + args.tensorboard_name + "/"
# np.savetxt(fname + 'points_acc_tac.txt', np.array(taccuracies), delimiter=',')
# np.savetxt(fname + 'points_acc_vis.txt', np.array(vaccuracies), delimiter=',')


"""Experiment 3 part a"""
"""For testing robustness to noise """
# # engine.load_model('1_PointNet_V2T_e40_noise_0.05_run3')
# # noise_sds = np.linspace(start=0.0, stop=0.5, num=11)
# # print(noise_sds)
# # accuracies = []
# # for noise_sd in noise_sds:
#     tf_test.transforms[2].update_sd(noise_sd)
#     # name = 'tactile noise_sd: ' + str(noise_sd)
#     # accuracy = engine.predict(test_tactile_loader, name)
#     name = 'vision noise_sd: ' + str(noise_sd)
#     accuracy = engine.predict(test_vision_loader, name)
#     accuracies.append(float(accuracy))
#
# print(accuracies)

"""3b: Curriculum"""
# taccuracies, vaccuracies = [], []
# noise_sds = np.linspace(start=0.0, stop=0.5, num=11)
# print(noise_sds)
# accuracies = []
# for noise_sd in noise_sds:
#     tf_test.transforms[2].update_sd(noise_sd)
#     name = 'tactile noise_sd: ' + str(noise_sd)
#     taccuracy = engine.predict(test_tactile_loader, name)
#     name = 'vision noise_sd: ' + str(noise_sd)
#     vaccuracy = engine.predict(test_vision_loader, name)
#     taccuracies.append(taccuracy)
#     vaccuracies.append(vaccuracy)
# fname = os.getcwd() + '/runs/' + args.tensorboard_name + "/"
# np.savetxt(fname + 'noise_acc_tac.txt', np.array(taccuracies), delimiter=',')
# np.savetxt(fname + 'noise_acc_vis.txt', np.array(vaccuracies), delimiter=',')
