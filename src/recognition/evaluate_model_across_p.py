from pandas.core import base
from torch.utils import tensorboard
from PointNetMedium import PointNet
from CloudDataloading import CloudDataset, SingleModelDataset
import kaolin.transforms as tfs, CloudTransforms as mytfs
from torch.utils.data import DataLoader
from PointClassificationEngine import ClassificationEngine as Engine
from tqdm import tqdm
import os, numpy as np, torch, pandas as pd, time

def run(s, evaluate:bool=False):
    
    model = PointNet(classes=len(s["cats"])).to(s["device"])
    tf_train = tfs.Compose([
        mytfs.SamplePoints(scheme='random', nb_points=s["n_points"], device=s["device"]),
        mytfs.RandomRotatePointCloud(device=s["device"]),
        mytfs.WhiteNoise(sd=s["training_noise"], device=s["device"]),
        mytfs.RandomScalePointCloud(scf=s["scf"], device=s["device"])
    ])
    tf_test = tfs.Compose([
        mytfs.SamplePoints(scheme='random', nb_points=s["n_points"], device=s["device"]),
        # mytfs.RandomRotatePointCloud(device=s["device"])
    ])

    dataset_train = SingleModelDataset(
        s["training_root"], s["cats"], transform=tf_train, device=s["device"], n_repeats=s["n_repeats_train"])
    dataset_val = SingleModelDataset(
        s["val_root"], s["cats"], transform=tf_test, device=s["device"], n_repeats=s["n_repeats_val"])

    train_loader = DataLoader(
        dataset_train, batch_size=s["batch_size_train"], shuffle=True, drop_last=True)
    val_loader = DataLoader(
        dataset_val, batch_size=s["batch_size_train"], shuffle=True, drop_last=True)
    optimiser = torch.optim.Adam(model.parameters(), lr=s["lr"])
    criterion = torch.nn.CrossEntropyLoss().to(s["device"])
    tensorboard_params = {
        'name': s["tensorboard_name"],
        'dir': "runs/"+ s["tensorboard_name"],
        'cats': s["cats"],
        'nb_cats': len(s["cats"])
    }
    engineparams = {
        'epochs': s["epochs"]
    }

    engine = Engine(model=model, train_loader=train_loader, val_loader=val_loader,
                    tensorboard=tensorboard_params,
                    optimizer=optimiser, device=s["device"], engineparams=engineparams, criterion=criterion
                    )

    engine.load_model(s["model_to_load"])
    
    # Column names for dataframe which will store all the 
    df = pd.DataFrame(columns=s["col_names"])
    # Across a range of K
    print("####### COMPUTING PREDICTIONS #######")
    for p in s["p_range"]:
        print("##### p: ", p, "#####")
        accuracy_tac_nof,accuracy_tac_filt,accuracy_vis_nof,accuracy_vis_filt = 0,0,0,0
        """ Predictions Tactile no filters """
        tactile_raw_dataset = CloudDataset(basedir=s["tactile_raw_root"]+str(p)+"/", split='test', transform=tf_test,
        categories=s["cats"], device=s["device"])
        tactile_taw_dataloader = DataLoader(
            tactile_raw_dataset, batch_size=s["batch_size_test"], shuffle=True)
        accuracy_tac_nof = engine.predict(tactile_taw_dataloader, 'tactile_raw_test_' + str(p))
        
        """ Predictions Tactile with filters """
        tactile_filter_dataset = CloudDataset(basedir=s["tactile_filtered_root"]+str(p) + "/", split='test', transform=tf_test,
        categories=s["cats"], device=s["device"])
        tactile_filter_dataloader = DataLoader(tactile_filter_dataset, batch_size=s["batch_size_test"],
                                            shuffle=True)
        accuracy_tac_filt = engine.predict(tactile_filter_dataloader, 'tactile_filtered_' + str(p))

        """ Predictions with Vision (filt+unfilt) - for completeness """
        # vision_no_filter_dataset = CloudDataset(basedir=s["vision_root"]+str(p) + "/", split='test', transform=tf_no_filter,
        # categories=s["cats"], device=s["device"])
        # vision_no_filter_dataloader = DataLoader(
        #     vision_no_filter_dataset, batch_size=s["batch_size_test"], shuffle=True, drop_last=True)
        # accuracy_vis_nof = engine.predict(vision_no_filter_dataloader, "vision_raw_no_filter")

        # vision_filter_dataset = CloudDataset(basedir=s["vision_root"]+str(p) +"/", split='test', transform=tf_test,
        # categories=s["cats"], device=s["device"])
        # vision_filter_dataloader = DataLoader(
        # vision_filter_dataset, batch_size=s["batch_size_test"], shuffle=True, drop_last=True)
        # accuracy_vis_filt = engine.predict(vision_filter_dataloader, "vision_filter_" + str(p))


        df_accuracies = pd.DataFrame([[s["tensorboard_name"], p, accuracy_tac_nof, accuracy_tac_filt, accuracy_vis_nof, accuracy_vis_filt, 0]], columns=s["col_names"])
        df = pd.concat([df, df_accuracies])
        print(df_accuracies)
        print(df)
    return df 

if __name__ == "__main__":
    
    col_names = ["tensorboard_name", "p_kmeans", "tac_nof", "tac_filt", "vis_nof", "vis_filt", 'final_val']

    settings ={
        "n_runs": 1,
        "p_range": np.arange(start=0.05,stop=1.05,step=0.05).round(decimals=2),

        ## TO BE USED
        # "tactile_filtered_root": "../datasets/kmeans_v3/tactile3/filtered/",
        # "tactile_raw_root": "../datasets/kmeans_v3/tactile3/filtered/",
        "tactile_filtered_root": "../datasets/kmeans_v3_noaug/filtered/",
        "tactile_raw_root": "../datasets/kmeans_v3_noaug/raw/",

        "vision_root": "../datasets/kmeans_v3/vision/filtered/",
        
        # "tensorboard_name": "Feb24_feb21_viskmeans_curric_overnight50_prangex_noaug_noagg_v2",
        # "tensorboard_name": "Feb24_feb21_viskmeans_vanilla_00_prange_noaug_noagg_v2",
        "tensorboard_name" : "confusion_matrix_test",

        # "model_to_load": "viskmeans_trial_19thfeb_10", #Best Curriculum puzzle trained 
        # "model_to_load": "viskmeans_control_20thfeb_00", #Best non - curriculum trained.
        # "model_to_load": "experiment2_b" # Best n_points using curric
        # "model_to_load": "experiment1_b" # Best n_points non-curric
        # "model_to_load": "Feb21st_viskmeans_curric_overnight50", #Best curric
        "model_to_load": "Feb21st_viskmeans_vanilla_00", #Best non curric


        ### TO BE IGNORED
        "epochs": 80,
        "training_root": "../datasets/single/vision/real/filtered_chopped/train/",
        "val_root": "../datasets/single/vision/real/filtered_chopped/test/",
    
        # "vision_root": "../datasets/single/vision/real/filtered_chopped/", 
        "cats": ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'spam', 'shampoo', 'tape'],
        "n_points": 1024,
        "lr": 1e-2,
        "batch_size_train": 12,
        "batch_size_test": 60,
        "device": 'cuda',
        "scf": 1.03,
        "training_noise": 0.05,
        "MLS_radius": 0.5,
        "leafsize": 0.35,
        "n_repeats_train": 200,
        "n_repeats_val": 84,
        "n_repeats_test": 200, #unused

        # "p_range": np.flip(np.arange(0.1,1,1.1).round(decimals=1)),
        "col_names": col_names
    }

    df = pd.DataFrame(columns=col_names)
    tstart= time.time()
    tensorboard_base = settings["tensorboard_name"] 
    for run_idx in tqdm(range(settings["n_runs"])):
        settings["tensorboard_name"] = tensorboard_base + str(run_idx) 
        df_single_run = run(settings, evaluate=True)
        print("df_single_run: ", df_single_run)
        df = pd.concat([df, df_single_run])
        df_single_run.to_csv(settings["tensorboard_name"] + '.csv')
        print("Dataframe: ", df)
        df.to_csv(tensorboard_base + '.csv')
        print("saved as: ", tensorboard_base + '.csv')
    print("total duration: {} minutes".format((time.time() - tstart)/60))
