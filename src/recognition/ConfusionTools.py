import os
from typing import List

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn
import torch
from matplotlib import rc

def CalculateConfMat(p_labels: torch.Tensor, gt_labels: torch.Tensor, nb_classes: int):
    """
    Create a confusion matrix from given predicted and ground truth labels

    Args:
        p_labels (torch.Tensor):   Predicted labels
        gt_labels (torch.Tensor):  Ground truth labels
        nb_classes (int):         The number of classes

    Returns:
        cm (torch.Tensor):      A filled confusion matrix
    """
    cm = torch.zeros(nb_classes, nb_classes)
    for gt, pred_label in zip(gt_labels.view(-1), p_labels.view(-1)):
        cm[int(gt.item()), int(pred_label.item())] += 1
    return cm


def AddToConfusionMat(cmatrix: torch.Tensor, p_labels, gt_labels):
    """
    Add values to an existing confusion matrix

    Args:
        cmatrix (torch.Tensor):  Confusion matrix
        p_labels (torch.Tensor):    Predicted labels
        gt_labels (torch.Tensor): Ground truth labels

    Returns:

    """
    for gt, pred_label in zip(gt_labels.view(-1), p_labels.view(-1)):
        cmatrix[int(gt.item()), int(pred_label.item())] += 1
    return cmatrix

def NormaliseConfusion(cmatrix: torch.tensor):
    """Normalise the Confusion matrix which is currently a count, rather than a probability.

    Args:
        cmatrix (torch.tensor): [description]

    Returns:
        normalised_cmatrix (torch.tensor): normalised confusion matrix
        per_class_accuracy (list): A list of accuracies of predicting class X as X 
    """
    per_class_accuracy = cmatrix.diag() / cmatrix.sum(1)
    # Turn into probabilities per GT class
    normalised_confusion_matrix = torch.nn.functional.normalize(cmatrix, p=1, dim=1)
    return normalised_confusion_matrix, per_class_accuracy

def PlotConfusion(categories: list,
                  cmatrix: torch.Tensor,
                  ax: plt.Axes = None,
                  saveplot: bool = False,
                  path: str = '',
                  filetype: str = 'eps'
                  ):
    """
    Plot a confusion matrix, with the option to save it the resulting figure.
    Args:
        categories (List): List of categories (in words), where each index corresponds to category indices.
        cmatrix (torch.Tensor): Completed confusion matrix
        ax (plt.Axes): Axes on which to plot.
            This is required so that the figure can also be saved by the tensorboard writer.
        saveplot (bool): To save the file
        path: Save path
        filetype: Format in which to save the figure. Choices of 'png', 'eps' - see matplotlib doc.

    Returns:
        ax (plt.Axes): Axes with the completed plot.
    """
    # rc('text', usetex=True) # REQUIRES LaTeX installation
    # rc('font', **{'family': 'DejaVu Sans', 'serif': ['Times']})
    # rc('font', **{'family': 'fantasy'})
    # rc('font', **{'family': 'serif', 'serif':'Times'})

    mpl.rcParams['figure.dpi'] = 1000
    # sn.set_style({'font.family': 'serif', 'serif':['Times']})

    category_names = [" ".join(name.title() for name in word.split('_')) for word in categories]

    df_cm = pd.DataFrame(cmatrix.numpy(), category_names, category_names)
    df_cm.to_csv(path + '.csv')
    if ax:
        sn_ax = sn.heatmap(df_cm, annot=False, cmap='Blues', cbar=True, square=True, ax=ax, vmax=1.0, vmin=0)
    else:
        sn_ax = sn.heatmap(df_cm, annot=False, cmap='Blues', cbar=True, square=True, vmax=1.0, vmin=0)
        # annot dictates whether values appear in the each tile
    plt.tick_params(axis='both', which='major', labelbottom=False, bottom=False, top=True, labeltop=True)
    # sn_ax.set_xticklabels(sn_ax.get_xticklabels(), rotation=90)
    sn_ax.set_xticklabels(sn_ax.get_xticklabels(), fontdict={'fontsize': 14}, rotation=90, ha='center')
    sn_ax.set_yticklabels(sn_ax.get_yticklabels(), fontdict={'fontsize': 14}, rotation=0, va='center')

    sn_ax.xaxis.set_label_position('top')
    # sn_ax.set_xlabel('Predicted Label', labelpad=15, )
    # sn_ax.set_ylabel('Ground Truth Label', labelpad=15)
    sn_ax.set_xlabel('Predicted Label', fontsize=14, labelpad=15)
    sn_ax.set_ylabel('Ground Truth Label', fontsize=14, labelpad=15)

    plt.tight_layout()
    
    if saveplot is True:
        fname = path + "." + filetype
        plt.savefig(fname, format=filetype)
    return sn_ax


if __name__ == '__main__':
    classes = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange',
               'pack_of_cards', 'rubix_cube', 'rugby_ball', 'soda_can', 'shampoo', 'spam', 'tape']
    confusion_matrix = torch.rand((12, 12))
    filepath = os.getcwd() + 'test_confusion_matrix'
    # rc('font', **{'family': 'DejaVu Sans', 'serif': ['Computer Modern']})
    # rc('font', **{'family': 'DejaVu Sans', 'serif': ['Times']})

    # rc('text', usetex=True) # REQUIRES LaTeX installation
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    PlotConfusion(categories=classes, cmatrix=confusion_matrix, saveplot=True, path=filepath, filetype='png', ax=ax)
