import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import torch
import pandas as pd
"""Precision recall tools"""


def TpFpFnCalculator(cm: torch.Tensor):
    """
    True Positive, False Positive, False Negative Calculator from confusion matrix
    Args:
        cm (torch.Tensor): confusion matrix (mxm for m categories)
            Ground truth categories in rows, Predicted in columns. All true Positives are in the diagonals

    Returns:
        (tuple):
            tp (torch.Tensor): true positives (dimension 1xm)
            fp (torch.Tensor): false positives (dimension 1xm)
            fn (torch.Tensor): false negatives (dimension 1xm)
    """
    # True positive in the diagonal element.
    tp = cm.diag()
    # False positives are the rows excl diag element,
    fp = torch.sum(cm, dim=1) - tp  # dim 0 is columns, dim 1 is rows
    # False negatives are the columns excl diag element,
    fn = torch.sum(cm, dim=0) - tp
    return tp, fp, fn


def PRCalculator(cm: torch.Tensor):
    """
    Calculate the precision and recall values for the given confusion matrix
    Args:
        cm:

    Returns:

    """
    tp, fp, fn = TpFpFnCalculator(cm)
    precision = torch.div(tp, tp + fp)
    recall = torch.div(tp, tp + fn)
    return precision, recall


def PlotPRBarchart(precision: torch.Tensor, recall: torch.Tensor, categories: list, saveplot=False, path='',
                   filetype='eps'):
    categories = [" ".join(name.title() for name in word.split('_')) for word in categories]
    mpl.rcParams['figure.dpi'] = 400
    width = 0.35  # the width of the bars
    fig, ax = plt.subplots()
    x = np.arange(len(categories))
    rects1 = ax.bar(x - width / 2, precision, width, label='Precision')
    rects2 = ax.bar(x + width / 2, recall, width, label='Recall')
    ax.set_ylabel('Probability', fontsize=14, labelpad=10)
    ax.set_xlabel('Class Label', fontsize=14, labelpad=10)
    # ax.set_title('Precision Recall Scores per class', fontsize=14)
    ax.set_xticks(x)
    ax.set_xticklabels(categories, fontdict={'fontsize': 14}, rotation=90)
    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend()
    fig.tight_layout()

    if saveplot:
        fname = path + "." + filetype
        plt.savefig(fname, format=filetype)
    
    # save values
    df_pr = pd.DataFrame([precision.numpy(), recall.numpy()], ['precision', 'recall'], categories)
    df_pr.to_csv(path+'.csv')
    return fig, ax


if __name__ == '__main__':
    cm = torch.rand((12, 12))
    my_categories = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange',
                     'pack_of_cards', 'rubix_cube', 'rugby_ball', 'soda_can', 'shampoo', 'spam', 'tape']
    p, r = PRCalculator(cm)
    filepath = '//Classification/practice_PR'
    fig, ax = PlotPRBarchart(precision=p, recall=r, categories=my_categories, saveplot=True, path=filepath,
                             filetype='eps')
    plt.show()
