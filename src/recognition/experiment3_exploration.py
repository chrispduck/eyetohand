
import matplotlib
matplotlib.use('Agg')  #required for plotting on a remote server
from PointNetMedium import PointNet
import numpy as np, torch
import kaolin.transforms as tfs, CloudTransforms as mytfs
from CloudDataloading import CloudDataset, SingleModelDataset
from torch.utils.data import DataLoader
from PointClassificationEngine import ClassificationEngine as Engine
from tqdm import tqdm
s = {
    # "tensorboard_name": "Feb22nd_sequential_exploration_w_viskmeans_vanilla_00",
    # "tensorboard_name": "Feb22nd_sequential_raw_exploration_w_viskmeans_vanilla_00",
    # "tensorboard_name": "Feb22nd_sequential_raw_exploration_w_viskmeans_curric_overnight50",
    # "tensorboard_name": "Feb24nd_sequential_raw_exploration_w_Feb21st_viskmeans_curric_overnight50_noaug_noagg_raw",
    # "tensorboard_name": "Feb24nd_sequential_raw_exploration_w_Feb21st_viskmeans_curric_overnight50_noaug_noagg_filtered",
    # "tensorboard_name": "Feb24nd_sequential_raw_exploration_w_Feb21st_viskmeans_vanilla_00_noaug_noagg_filtered",
    "tensorboard_name": "Feb24nd_sequential_raw_exploration_w_Feb21st_viskmeans_vanilla_00_noaug_noagg_raw",

    "cats": ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'spam', 'shampoo', 'tape'],
    # "tensorboard_name": "experiment3_cards_only",
    # "cats": ["pack_of_cards"]*10,
    # "dataset_base": "../datasets/tactile_exploration_multi/",
    # "dataset_base": "../datasets/tactile_exploration_multi_raw/",
    # "dataset_base": "../datasets/tactile_exploration_noaug_raw/",
    "dataset_base": "../datasets/tactile_exploration_noaug_raw/",
    # "dataset_base": "../datasets/tactile_exploration_noaug_filtered/",
    "device": "cuda",
    "n_points": 1024,
    "n_repeats": 400,
    "trained_model_name": "Feb21st_viskmeans_vanilla_00",
    # "trained_model_name": "Feb21st_viskmeans_curric_overnight50",
    "batch_size": 12,
    # excess info
    "lr": 0.01,
    "epochs": 0
}

# Load a trained version of PointNet
model = PointNet(classes=len(s["cats"])).to('cuda')
tf_test = tfs.Compose([
    mytfs.SamplePoints(scheme='random', nb_points=s["n_points"], device=s["device"]),
    mytfs.RandomRotatePointCloud(device=s["device"])
])

# PCEngine required objects - WILL BE UNUSED - USE of engine to make plotting and prediction tracking easy only
optimiser = torch.optim.Adam(model.parameters(), lr=s["lr"])
criterion = torch.nn.CrossEntropyLoss().to(s["device"])
tensorboard_params = {
    'name': s["tensorboard_name"],
    'dir': "runs/"+ s["tensorboard_name"],
    'cats': s["cats"],
    'nb_cats': len(s["cats"])
}
engineparams = {
    'epochs': s["epochs"]
    }
_dataset = SingleModelDataset(
    s["dataset_base"] + '0.05/', s["cats"], transform=tf_test, device=s["device"], n_repeats=1000)
_loader =  DataLoader(
    _dataset, batch_size=s["batch_size"], shuffle=True, drop_last=True)

engine = Engine(model=model, train_loader=_loader, val_loader=_loader,
                tensorboard=tensorboard_params,
                optimizer=optimiser, device=s["device"], engineparams=engineparams, criterion=criterion)

engine.load_model(s["trained_model_name"])
# Be able to load the data sequentially
prange = np.arange(start=0.05,stop=1.05,step=0.05).round(decimals=2)

# first samples contain first 5% of points, 10% etc.
accuracies = []
dataset = None
for p in prange:
    dataset_dir = s["dataset_base"] +  str(p) + '/'
    print(dataset_dir)
    # dataset = SingleModelDataset(
    #     dataset_dir, s["cats"], transform=tf_test, device=s["device"], n_repeats=s["n_repeats"])
    dataset = CloudDataset(basedir=dataset_dir, split='test', categories=s["cats"], transform=tf_test, device=s["device"])
    loader =  DataLoader(
        dataset, batch_size=12, shuffle=True, drop_last=True)
    accuracy = engine.predict(loader, "p = {}".format(str(p)))
    accuracies.append(accuracy)
    print(accuracies)
    arr = np.array(accuracies)
    np.savetxt("runs/"+ s["tensorboard_name"] + "/results.csv", arr, delimiter=',')
    del dataset
arr = np.transpose(np.array([accuracies, 100*prange]))
np.savetxt("runs/"+ s["tensorboard_name"] + "/results.csv", arr, delimiter=',', header="accuracies, percentage_exploration")



