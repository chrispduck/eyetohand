from PointNetMedium import PointNet
from kaolin import transforms as tfs
import CloudTransforms as mytfs
from torch.utils.data import DataLoader
from CloudDataloading import SingleModelDataset
from tqdm import tqdm

s = {
    "tactile_root": "../datasets/single/tactile/real/filtered/",
    "n_points": 1024,
    "batch_size": 60,
    "device": 'cuda',
    "MLS_radius": 0.5,
    "leafsize": 0.35,
    "noise": 0.05,
    "p_kmeans": 0.7,
    "cats": ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'soda_can', 'shampoo', 'tape'],
    "n_repeats": 50,
}


model = PointNet(classes=len(s["cats"])).to(s["device"])

tf_filter = tfs.Compose([
    mytfs.SampleKMeans(p=s["p_kmeans"], device=s["device"]),
    mytfs.VoxelMLSFilter(
        MLS_radius=s["MLS_radius"], leafsize=s["leafsize"], device=s['device']),
    mytfs.SamplePoints(scheme='random', nb_points=s["n_points"]),
    mytfs.RandomRotatePointCloud(device=s["device"]),
    mytfs.WhiteNoise(sd=s["noise"], device=s["device"]),
    mytfs.RandomScalePointCloud(scf=1.05)
])

dataset = SingleModelDataset(
    s["tactile_root"], obj_names=s["cats"], transform=tf_filter, device=s["device"], n_repeats=s["n_repeats"])
dataloader = DataLoader(
    dataset, batch_size=s["batch_size"], shuffle=True, num_workers=0)

for idx, batch in enumerate(tqdm(dataloader)):
    pass
