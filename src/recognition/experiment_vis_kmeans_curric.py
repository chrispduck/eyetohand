from pandas.core import base
from torch.utils import tensorboard
from PointNetMedium import PointNet
from CloudDataloading import CloudDataset, SingleModelDataset
import kaolin.transforms as tfs, CloudTransforms as mytfs
from torch.utils.data import DataLoader
from Curriculum import Curriculum
from PointClassificationEngine import ClassificationEngine as Engine
from tqdm import tqdm
import os, numpy as np, torch, pandas as pd, time

def run(s:dict, scheme:tuple, evaluate:bool=False):
    
    model = PointNet(classes=len(s["cats"])).to(s["device"])
    tf_train = tfs.Compose([
        mytfs.SamplePoints(scheme='random', nb_points=s["n_points"], device=s["device"]),
        mytfs.RandomRotatePointCloud(device=s["device"]),
        mytfs.WhiteNoise(sd=s["training_noise"], device=s["device"]),
        mytfs.RandomScalePointCloud(scf=s["scf"], device=s["device"])
    ])
    tf_test = tfs.Compose([
        mytfs.SamplePoints(scheme='random', nb_points=s["n_points"], device=s["device"]),
        mytfs.RandomRotatePointCloud(device=s["device"])
    ])

    # dataset_train = SingleModelDataset(
    #     s["training_root"], s["cats"], transform=tf_train, device=s["device"], n_repeats=s["n_repeats_train"])
    # dataset_val = SingleModelDataset(
    #     s["val_root"], s["cats"], transform=tf_test, device=s["device"], n_repeats=s["n_repeats_val"])

    dataset_train = CloudDataset(
        s["training_root"], split='train', categories=s["cats"], transform=tf_train, device=s["device"])
    dataset_val = CloudDataset(
        s["val_root"], split='test', categories=s["cats"], transform=tf_test, device=s["device"])

    train_loader = DataLoader(
        dataset_train, batch_size=s["batch_size_train"], shuffle=True, drop_last=True)
    val_loader = DataLoader(
        dataset_val, batch_size=s["batch_size_train"], shuffle=True, drop_last=True)
    optimiser = torch.optim.Adam(model.parameters(), lr=s["lr"])
    criterion = torch.nn.CrossEntropyLoss().to(s["device"])
    tensorboard_params = {
        'name': s["tensorboard_name"],
        'dir': "runs/"+ s["tensorboard_name"],
        'cats': s["cats"],
        'nb_cats': len(s["cats"])
    }
    engineparams = {
        'epochs': s["epochs"]
    }
    

    engine = Engine(model=model, train_loader=train_loader, val_loader=val_loader,
                    tensorboard=tensorboard_params,
                    optimizer=optimiser, device=s["device"], engineparams=engineparams, criterion=criterion,
                    )

    
    ###### CURRICULUM TRAINING
    epochs, ps = scheme
    for n_epochs, p in zip(epochs, ps):
        dataset_train = CloudDataset(basedir="../datasets/kmeans_v3/vision/filtered/"+str(p)+'/', split='train', categories=s["cats"], transform=tf_train, device=s["device"])
        dataset_val = CloudDataset(basedir="../datasets/kmeans_v3/vision/filtered/"+str(p) + '/', split='test', categories=s["cats"], transform=tf_test, device=s["device"])
        train_loader = DataLoader(
                    dataset_train, batch_size=s["batch_size_train"], shuffle=True, drop_last=True)
        val_loader = DataLoader(
                    dataset_val, batch_size=s["batch_size_test"], shuffle=True, drop_last=True)
        engine.train_loader = train_loader
        engine.val_loader = val_loader
        engine.engineparams['epochs'] = n_epochs
        engine.fit()
    engine.save_model()

    # dataset_eval = CloudDataset(basedir="kmeans_v2/vision/filtered/1.0/", split='train', categories=s["cats"], transform=tf_train, device=s["device"])
    # eval_loader = DataLoader(dataset_eval, batch_size=s["batch_size_train"], shuffle=True, drop_last=True)
    # engine.predict(eval_loader, "p=1 eval")
    if evaluate:
    # Column names for dataframe which will store all the 
        df = pd.DataFrame(columns=s["col_names"])
        # Across a range of K
        for p in s["p_range"]:
            
            print("####### COMPUTING PREDICTIONS #######")
            accuracy_tac_nof,accuracy_tac_filt,accuracy_vis_nof,accuracy_vis_filt = 0,0,0,0
            """ Predictions Tactile no filters """
            tactile_raw_dataset = CloudDataset(basedir=s["tactile_raw_root"]+str(p)+"/", split='test', transform=tf_test,
            categories=s["cats"], device=s["device"])
            tactile_taw_dataloader = DataLoader(
                tactile_raw_dataset, batch_size=s["batch_size_test"], shuffle=True)
            accuracy_tac_nof = engine.predict(tactile_taw_dataloader, 'tactile_raw_nofilters_' + str(p))
            
            """ Predictions Tactile with filters """
            tactile_filter_dataset = CloudDataset(basedir=s["tactile_filtered_root"]+str(p) + "/", split='test', transform=tf_test,
            categories=s["cats"], device=s["device"])
            tactile_filter_dataloader = DataLoader(tactile_filter_dataset, batch_size=s["batch_size_test"],
                                                shuffle=True)
            accuracy_tac_filt = engine.predict(tactile_filter_dataloader, 'tactile_raw_filtered_' + str(p))

            """ Predictions with Vision (filt+unfilt) - for completeness """
            # vision_no_filter_dataset = CloudDataset(basedir=s["vision_root"]+"p-"+str(p) + "/", split='test', transform=tf_no_filter,
            # categories=s["cats"], device=s["device"])
            # vision_no_filter_dataloader = DataLoader(
            #     vision_no_filter_dataset, batch_size=s["batch_size_test"], shuffle=True, drop_last=True)
            # accuracy_vis_nof = engine.predict(vision_no_filter_dataloader, "vision_raw_no_filter")

            vision_filter_dataset = CloudDataset(basedir=s["vision_root"]+str(p) +"/", split='test', transform=tf_test,
            categories=s["cats"], device=s["device"])
            vision_filter_dataloader = DataLoader(
            vision_filter_dataset, batch_size=s["batch_size_test"], shuffle=True, drop_last=True)
            accuracy_vis_filt = engine.predict(vision_filter_dataloader, "vision_raw_filter" + str(p))


            df_accuracies = pd.DataFrame([[s["tensorboard_name"], p, accuracy_tac_nof, accuracy_tac_filt, accuracy_vis_nof, accuracy_vis_filt, engine.train_accuracy[-1]]], columns=s["col_names"])
            df = pd.concat([df, df_accuracies])
            print(df_accuracies)
            print(df)
    else:
        return pd.DataFrame([])
    return df 

if __name__ == "__main__":
    
    col_names = ["tensorboard_name", "p_kmeans", "tac_nof", "tac_filt", "vis_nof", "vis_filt", 'final_val']

    settings ={ 
        "schemes_to_try": [ 
                            # ((25,50,75), (1.0, 0.7,0.4)), #93.8%
                            ((40,55,70), (1.0, 0.7 ,0.4)), #97%
                            ((40,55,70), (1.0, 0.7 ,0.4)), #97%
                            ((40,55,70), (1.0, 0.7 ,0.4)), #97%
                            ((40,55,70), (1.0, 0.7 ,0.4)), #97%
                            ((40,55,70), (1.0, 0.7 ,0.4)), #97%
                            ((40,55,70,75), (1.0, 0.7, 0.4, 0.2)), #?
                            ((40,55,70,75,80), (1.0, 0.7, 0.4, 0.2, 0.1)), #?
                            # ((30,45,65,70), (1.0, 0.7, 0.5, 0.3)), #96.2
                            # ((30,50,70), (1.0, 0.7, 0.5)), #76
                            # ((35,60,80), (1.0, 0.7, 0.5)) #96
                            ],
        # "schemes_to_try": [
        #                     # ((40,55,70), (1.0, 0.7 ,0.4)), #97%
        #                     # ((40,55,70), (1.0, 0.7 ,0.4)), #97%
        #                     # ((40,55,70), (1.0, 0.7 ,0.4)), #97%
        #                     # ((40,55,70), (1.0, 0.7 ,0.4)), #97%
        #                     # ((40,55,70), (1.0, 0.7 ,0.4)), #97%
        #                     ((80,), (1.0,)), # Control runs
        #                     ((80,), (1.0,)),
        #                     ((80,), (1.0,)),
        #                     ((80,), (1.0,)),
        #                     ((80,), (1.0,)),

        # ],
        "save_names": ["Feb21st_viskmeans_curric_overnight" + str(i) for i in range(7)],
        "n_runs": 1,
        "epochs": 75,
        "p_range": [1.,0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1],
        # "p_range": [1.],

        # "p_range": [1.0, 0.1],
        # "scheme": ((25,25,25), (1.0, 0.7,0.4)),
        # "tensorboard_name": "20thFeb_curric_kmeans",
        "training_root": "../datasets/kmeans_v3/vision/filtered/1.0/",
        "val_root": "../datasets/kmeans_v3/vision/filtered/1.0",

        "tactile_filtered_root": "../datasets/kmeans_v3/tactile3/filtered/",
        "tactile_raw_root": "../datasets/kmeans_v3/tactile3/raw/",
        "vision_root": "../datasets/kmeans_v3/vision/filtered/",

        # "vision_root": "../datasets/single/vision/real/filtered_chopped/", 
        "cats": ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'spam', 'shampoo', 'tape'],
        "n_points": 1024,
        "lr": 1e-2,
        "batch_size_train": 12,
        "batch_size_test": 60,
        "device": 'cuda',
        "scf": 1.03,
        "training_noise": 0.05,
        # "MLS_radius": 0.5,
        # "leafsize": 0.35,
        "n_repeats_train": 200,
        "n_repeats_val": 84,
        "n_repeats_test": 200, #unused

        # "p_range": np.flip(np.arange(0.1,1,1.1).round(decimals=1)),
        "col_names": col_names
    }

    df = pd.DataFrame(columns=col_names)
    tstart= time.time()
    for save_name, scheme in zip(settings["save_names"], settings["schemes_to_try"]):
        print(scheme)
        a, b = scheme
        # tensorboard_base = name
        for run_idx in tqdm(range(settings["n_runs"])):
            settings["tensorboard_name"] = save_name +str(run_idx)
            df_single_run = run(settings, scheme=scheme, evaluate=True)
            print("df_single_run: ", df_single_run)
            df = pd.concat([df, df_single_run])
            df_single_run.to_csv("runs/" + settings["tensorboard_name"] +'/results' +str(run_idx) + '.csv')
            print("Dataframe: ", df)
            df.to_csv("runs/" + settings["tensorboard_name"] +'/results' +str(run_idx) + '.csv')
        print("total duration: {} minutes".format((time.time() - tstart)/60))
