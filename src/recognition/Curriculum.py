from PointNetMedium import Transform
from collections import defaultdict
import torch
import numpy as np
from kaolin.transforms import Compose
import CloudTransforms as mytfs

class Curriculum:
    def __init__(self, transforms: Compose, settings: list, mode='uniform'):
        """
        Warning - relies on the order of the transforms.  #TODO Remove this dependancy
        Args:
            transforms:
            total_epochs:
            settings: a list of key-value epoch_no-(type, number)
                        eg. 20-(noise, 0.05) adjusts the noise to 0.05 on the 20th epoch
            mode: (str) options of 'normal' or 'random'. Normal will parse the commands and update to the value
                        'random' is an option for the noise curriculum training. It will select a noise sd uniformly
                        between 0 and the given sd.
        """
        assert isinstance(transforms, Compose)
        assert mode in ['normal', 'uniform']
        self.tfs = transforms
        self.mode = mode
        self.epoch = 0
        self.noise_limit = None
        self.curr_dict = defaultdict(lambda: None) #curriculum_dictionary
        for setting in settings:
            epoch, command = setting
            self.curr_dict[str(epoch)] = command

    def increment_epoch(self):
        print("epoch incremented")
        instruction = self.curr_dict[str(self.epoch)]
        if self.mode is 'normal':
            if instruction:
                self._parse_instruction(instruction)
        elif self.mode is 'uniform': 
            if instruction:
                scheme, noise = instruction # unpack
                self.noise_limit = noise
                # sample noise uniformly
            sd = np.random.uniform(0, self.noise_limit)
            self._adjust_noise(sd)
        self.epoch += 1

    def _parse_instruction(self, instruction):
        scheme, number = instruction
        if scheme is 'noise':
            self._adjust_noise(number)
        elif scheme is 'nb_points':
            self._adjust_nb_points(number)
        else:
            print("Instruction not recognised")

    def _adjust_noise(self, sd):
        assert any(isinstance(t, mytfs.WhiteNoise) for t in self.tfs.transforms)
        idx = [isinstance(t, mytfs.WhiteNoise) for t in self.tfs.transforms].index(True)
        self.tfs.transforms[idx].update_sd(sd)
        print("Adjusted noise to sd {0}".format(sd))

    def _adjust_nb_points(self, nb_points):
        assert any(isinstance(t, mytfs.SamplePoints) for t in self.tfs.transforms)
        idx = [isinstance(t, mytfs.SamplePoints) for t in self.tfs.transforms].index(True)
        self.tfs.transforms[idx].nb_points = nb_points
        print("Adjusted nb points to {0}".format(nb_points))

    def __str__(self):
        repr = {
            "mode": self.mode,
            "noise_limit": self.noise_limit,
            "settings": self.curr_dict
        }
        return "\n".join(str(key)+ ': ' + str(value) for key, value in repr.items())
