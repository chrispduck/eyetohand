from kmeans_sampling_single import select_clusters
import numpy as np
from tqdm import tqdm
import pandas as pd
import pcl, os,shutil
from sklearn.cluster import KMeans
from collections import Counter

class Generate_KMeans_Samples:

    def __init__(self, cluster_size):
        self.cats =  ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'spam', 'shampoo', 'tape']
        # self.cats =  ['baseball']
        self.filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)
        self.cluster_size = cluster_size
    
    def generate(self, n_repeats):
        # filter = [True, False, True, False]
        filter = [False]
        infolders = [
            # '../datasets/single/tactile/real/raw/',
                # '../datasets/single/tactile/real/raw/',
                '../datasets/single/vision/real/filtered_chopped/'
                ]
        outfolders = [
            # '../datasets/kmeans_v2/tactile/filtered/',
                # '../datasets/kmeans_v2/tactile/raw/',
                '../datasets/kmeans_v3/vision/filtered/',
                ]

        for infolder, outfolder, filter in zip(infolders,outfolders, filter):
            if os.path.exists(outfolder):
                shutil.rmtree(outfolder)
            for p in tqdm(reversed(np.arange(0.05,1.05,0.05).round(decimals=2))):
                # print("p:", p)
                os.makedirs(outfolder + 'test/' + str(p)+'/')
                for cat in self.cats:
                    fname_in = infolder + cat + '.csv'
                    X = np.loadtxt(fname_in, delimiter=',')
                    print("Loaded {} with {} points".format(fname_in, X.shape[0]))
                    
                    # Calculate k-means clusters
                    labels = self.compute_k_means(X, cluster_size=self.cluster_size)
                    # print("Number of labels computed: ",set(labels.shape))
                    X_labels = np.concatenate([X, np.expand_dims(labels, axis=1)], axis=1)
                    # c = Counter(labels)

                    for split in ["/test/", "/train/"]:
                        pdir = outfolder + str(p) + '/' + cat + '/' + split 
                        os.makedirs(pdir)
                        for idx in range(n_repeats):
                            sample, _ = self.select_clusters(X, labels, p=p)
                            # print("Sample points: ", sample.shape[0])
                            if filter:
                                sample = self.filter(sample)
                                # print("Filtered to: ", sample.shape[0])
                            # Finally save the sample
                            # print("p: ", p, "  Sample points: ", sample.shape[0])
                            fname_out = pdir +  cat + '_' + str(idx).zfill(4) + '.csv'
                            np.savetxt(fname_out, sample, delimiter=',')

    def select_clusters(self, X: np.array, labels: np.array, p: float):
        """Select clusters from a partitioned point cloud based on a percentage of overall cloud

        Args:
            points (np.array): pointcloud (nx3)
            labels (np.array): labels (nx1)
            p (float): percentage of clusters to keep 

        Returns:
            np.array: pointcloud
        """
        # number of clusters
        nb_clusters = len(set(labels))
        # number of clusters desired
        nb_desired = int(np.ceil(nb_clusters*p))
        # randomly choose the clusters - use discrete unifrom dist. without replacement
        cluster_ids = np.random.choice(np.array(list(set(labels))), size=nb_desired, replace=False)
        # indices of the pointcloud corresponding to those clusters
        indices = np.isin(labels, cluster_ids)
        # return resulting cloud
        return X[indices], labels[indices]

    def generate_k_means_samples(self, indir: str, outdir:str , cluster_size:int, categories: list, header=None) -> None:
        fnames_in = [indir + category + '.csv' for category in categories]
        fnames_out = [outdir + category + '.csv' for category in categories]
        for f_in, f_out in tqdm(zip(fnames_in, fnames_out)):
            if header:
                df = pd.read_csv(f_in)
                X = df.loc[:, ['x', 'y', 'z']]
            else:
                X = pd.read_csv(f_in, header=None)
            # Compute kmeans_labels
            labels = self.compute_k_means(X.to_numpy(), cluster_size=cluster_size)
            X_ = np.concatenate([X, np.expand_dims(labels, axis=1)], axis=1)
            np.savetxt(f_out, X_, delimiter=',')
            print(f_out)
        

    def compute_k_means(self, X: np.array, cluster_size): 
        # Compute number of clusters for KMeans computation
        nb_clusters = int(np.floor(X.shape[0]/cluster_size))
        # Perform Kmeans
        kmeans = KMeans(n_clusters=nb_clusters, random_state=0).fit(X)
        # Extract Labels
        labels=kmeans.labels_
        return labels


class MLS_Voxel_Filter:
    """ A class to perform moving least squares and a voxel filtering to an input cloud"""

    def __init__(self, MLS_radius: float, leafsize: float):
        """
        Args:
            MLS_radius: Moving least squares k-nn radius
            leafsize: length of the voxel cube
        """
        self.MLS_radis = MLS_radius
        self.leafsize = leafsize

    def __call__(self, points: np.ndarray):
        # assert points is np.ndarray
        cloud = pcl.PointCloud(points.astype('float32'))
        # Moving least Squares and voxel filtering
        MLS_filtered_cloud = self.apply_MLS(cloud)
        MLS_and_voxel_cloud = self.apply_voxel_filter(MLS_filtered_cloud)
        return MLS_and_voxel_cloud.to_array()

    def apply_MLS(self, cloud: pcl.PointCloud) -> pcl.PointCloud:
        MLS = cloud.make_moving_least_squares()  # type: pcl.MovingLeastSquares
        MLS.set_search_radius(self.MLS_radis)
        return MLS.process()

    def apply_voxel_filter(self, cloud: pcl.PointCloud) -> pcl.PointCloud:
        voxel_filter = cloud.make_voxel_grid_filter()  # type pcl.VoxelGridFilter
        voxel_filter.set_leaf_size(self.leafsize, self.leafsize, self.leafsize)
        return voxel_filter.filter()


if __name__=='__main__': 
    dataset_generator = Generate_KMeans_Samples(cluster_size=10)
    dataset_generator.generate(n_repeats=200)
