import os, shutil, pcl, numpy as np

class SplitTactileClouds:
    def __init__(self, tactile_root, cats):
        # Assumes tactile clouds are in the file paths tactile_root/object.csv
        self.tactile_root = tactile_root
        self.cats = cats
        self.filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)

    def generate(self, outfolder, n_samples, total_samples):
        if os.path.exists(outfolder):
            shutil.rmtree(outfolder)
        for filter_path, filter in zip(["filtered/", "raw/"],[True, False]):
            for cat in self.cats:
                fname_in = self.tactile_root + cat + '.csv'
                cloud_in = np.loadtxt(fname_in, delimiter=',')
                for split in ["train/", "test/"]: 
                    print("Split: ", split)
                    split_dir = outfolder + filter_path + cat + '/' + split 
                    os.makedirs(split_dir)
                    total_points = cloud_in.shape[0]
                    print("total_points", total_points)
                    available_row_indices = np.arange(total_points)

                    # for 200 samples, create 66 twice and 68 once
                    n_repeats_regular = total_samples // n_samples 
                    n_repeats_final = n_repeats_regular + total_samples %n_samples 
                    idx=0 # count the number of samples
                    sample_number=0
                    # Generate 2 lots of 66
                    for _ in range(n_samples-1):
                        # select first set of row indices
                        selected_row_indices = np.random.choice(available_row_indices, size=total_points//n_samples, replace=False)
                        to_delete = np.isin(available_row_indices, selected_row_indices)
                        print(to_delete.shape)
                        print(available_row_indices.shape)
                        available_row_indices = available_row_indices[~to_delete]
                        print("available row indices after deselection", available_row_indices.shape[0])
                        sample = cloud_in[selected_row_indices, :]
                        if filter:
                            n_before = sample.shape[0]
                            sample=self.filter(sample)
                            print("Performing filtering: n before, n_after", n_before, sample.shape[0])

                        # Save one of each seperately
                        fname_unique_sample = outfolder + filter_path + cat+str(sample_number) +'.csv'
                        sample_number+=1
                        np.savetxt(fname_unique_sample, sample, delimiter=',')
                        # Save many in object_XXXX.csv format
                        for _ in range(n_repeats_regular):
                            fname_out = split_dir +  cat + '_' + str(idx).zfill(4) + '.csv'
                            np.savetxt(fname_out, sample, delimiter=',')
                            idx+=1
                    sample = cloud_in[available_row_indices, :]
                    if filter:
                        n_before = sample.shape[0]
                        sample=self.filter(sample)
                        print("Performing filtering: n before, n_after", n_before, sample.shape[0])

                    print("final sample shape: ", sample.shape[0])
                    fname_unique_sample = outfolder + filter_path +cat+str(sample_number)+'.csv'
                    sample_number+=1
                    np.savetxt(fname_unique_sample, sample, delimiter=',')
                    # Generate 1 lot of 68
                    for _ in range(n_repeats_final):
                        # Save many in object_XXXX.csv format
                        fname_out = split_dir +  cat + '_' + str(idx).zfill(4) + '.csv'
                        np.savetxt(fname_out, sample, delimiter=',')
                        idx+=1
                    





class MLS_Voxel_Filter:
    """ A class to perform moving least squares and a voxel filtering to an input cloud"""

    def __init__(self, MLS_radius: float, leafsize: float):
        """
        Args:
            MLS_radius: Moving least squares k-nn radius
            leafsize: length of the voxel cube
        """
        self.MLS_radis = MLS_radius
        self.leafsize = leafsize

    def __call__(self, points: np.ndarray):
        # assert points is np.ndarray
        cloud = pcl.PointCloud(points.astype('float32'))
        # Moving least Squares and voxel filtering
        MLS_filtered_cloud = self.apply_MLS(cloud)
        MLS_and_voxel_cloud = self.apply_voxel_filter(MLS_filtered_cloud)
        return MLS_and_voxel_cloud.to_array()

    def apply_MLS(self, cloud: pcl.PointCloud) -> pcl.PointCloud:
        MLS = cloud.make_moving_least_squares()  # type: pcl.MovingLeastSquares
        MLS.set_search_radius(self.MLS_radis)
        return MLS.process()

    def apply_voxel_filter(self, cloud: pcl.PointCloud) -> pcl.PointCloud:
        voxel_filter = cloud.make_voxel_grid_filter()  # type pcl.VoxelGridFilter
        voxel_filter.set_leaf_size(self.leafsize, self.leafsize, self.leafsize)
        return voxel_filter.filter()



if __name__ == '__main__':
    cats =  ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'spam', 'shampoo', 'tape']
    splitter = SplitTactileClouds("../datasets/single/tactile/real/raw/", cats=cats)
    splitter.generate(outfolder="../datasets/test_tactile_split3/", n_samples=3, total_samples=200)
    splitter.generate(outfolder="../datasets/test_tactile_split5/", n_samples=5, total_samples=200)