import CloudTransforms as mytfs
import kaolin.transforms as tfs
from CloudDataloading import SingleModelDataset
from torch.utils.data import DataLoader
from VisualiseCloud import visualize_batch
from tqdm import tqdm 
import numpy as np

if __name__ == '__main__':
    device = 'cuda'
    n_points =1024
    # scf = 1.05
    # n_sd = 0.05
    batchsize =10
    categories = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'soda_can', 'shampoo', 'tape']
    
    ps = [0.1, 0.4, 0.7]
    modes = ['tactile', 'vision']
    styles = ['raw', 'filtered']
    for mode in modes: 
        for style in styles:
            for p in ps:
                dataset_root = "../datasets/kmeans/" + mode + "/real/" + style + '/'
                tf_kmeans = tfs.Compose([
                    mytfs.SampleKMeans(p=p, device='cuda'),
                    mytfs.SamplePoints(scheme='random', nb_points=n_points)
                    # mytfs.RandomRotatePointCloud(device=device)
                ])

                dataset = SingleModelDataset(
                    dataset_root, categories, transform=tf_kmeans, device=device, n_repeats=50)
                dataloader = DataLoader(
                    dataset, batch_size=batchsize, shuffle=False, drop_last=True)

                batch, labels = next(iter(dataloader))
                visualize_batch(batch, labels, categories, savefig=True, fname='sampling_images/kmeans-implementation-' + mode + '-' + style + '-' + str(p))


