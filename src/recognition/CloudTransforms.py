from os import sched_get_priority_max
from typing import Optional, Union
import matplotlib
matplotlib.use('Agg')  #required for plotting on a remote server
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import numpy as np
import scipy.stats as stat
import torch
import torch.distributions
from kaolin.rep.PointCloud import PointCloud
from kaolin.transforms import pointcloudfunc as pcfunc
import pcl

class RandomRotatePointCloud(object):
    r"""Rotate a pointcloud with a random rotation matrix.
    This transform will randomly rotate each
    point in the cloud by the rotation matrix specified.

    Args:
.       device (string, optional): String to select gpu or cuda
        inplace (bool, optional): Bool to make this operation in-place.

    """

    def __init__(self, device: Optional[str] = 'cpu', inplace: Optional[bool] = True):
        self.inplace = inplace
        self.device = torch.device(device)

    def __call__(self, cloud: Union[torch.Tensor, PointCloud]):
        """
        Args:
            cloud (torch.Tensor or PointCloud): Input pointcloud to be rotated.

        Returns:
            (torch.Tensor or PointCloud): Rotated pointcloud.
        """
        assert cloud.shape[1] == 3
        rotmat = torch.from_numpy(stat.special_ortho_group.rvs(3)).float().to(self.device)
        cloud = cloud.to(self.device)
        return pcfunc.rotate(cloud, rotmat=rotmat, inplace=self.inplace).to(self.device)


class Curriculum:
    """
    A curriculum learning class. This is used to change the learning parameters, such as number of points or white noise
     as the model trains.
    """

    def __init__(self, active: bool, epochs: int, lengths: list, fracs: list):
        self.active = active
        self.epoch = 0
        self.cumsteps = np.cumsum(lengths)
        self.epochs = epochs
        self.fracs = fracs
        if active:
            assert epochs == self.cumsteps[-1]

    def drop(self, cloud):
        if not self.active:
            return cloud
        no_unique_points = self._compute_no_unique(cloud)
        no_repeats = int(cloud.shape[0] - no_unique_points)
        if no_repeats > 0:
            # print('no repeats: ', no_repeats, '    points before:', len(torch.unique(cloud[:, 0])))
            cloud[-no_repeats:, :] = cloud[:no_repeats, :]
            # print('after: ', len(torch.unique(cloud[:, 0])))
        return cloud

    def _compute_no_unique(self, cloud):
        # print("cumulative steps: ", self.cumsteps, "step", self.epoch+ 1)
        index = np.searchsorted(self.cumsteps, self.epoch + 1)
        # print('index', index)
        frac = self.fracs[index]
        # print('frac', frac)
        no_unique_points = np.floor(cloud.shape[0] * frac)
        return no_unique_points

    def set_epoch(self, e):
        self.epoch = e
        if self.active:
            index = np.searchsorted(self.cumsteps, self.epoch + 1)
            frac = self.fracs[index]
            print("Cumulative steps:", self.cumsteps, "  Epoch:",
                  self.epoch, "  Index:", index, "  Frac:", frac)


class SamplePoints(object):
    """
    A class used to sample points from a pointcloud. For use with tf
    Two modes are avaliable:
        1) 'random' : points are selected randomly
        2) 'proportion': a proportion of points are taken from each cloud

    """

    def __init__(self, scheme: str = 'random', nb_points: int = None, proportion: float = None, device='cpu'):
        """
        Args:
            scheme (str): either 'random' or 'proportion'
            nb_points (int): number of points to sample. Fill only for 'random' scheme
            proportion:  proportion of points to sample. Fill only for 'proportion' scheme
        """
        self.nb_points = nb_points
        self.scheme = scheme
        self.device = device
        self.proportion = proportion

    def __call__(self, cloud: Union[torch.Tensor, PointCloud]):
        """

        Args:
            cloud (torch.Tensor): full point cloud to sampling (shape nx3)

        Returns:
            sampled (torch.Tensor): Sampled pointcloud
        """
        assert cloud.shape[1] == 3
        nb_cloud_points = cloud.shape[0]
        if self.scheme is 'random':
            return self.__sample(self.nb_points, cloud)
        elif self.scheme is 'proportion':
            nb_points_prop = int(np.floor(nb_cloud_points * self.proportion))
            cloud = self.__sample(nb_points_prop, cloud)
            return self.__sample(self.nb_points, cloud)
        else:
            return -1

    def __sample(self, nb_points:int , cloud: torch.Tensor):
        # print(nb_points, cloud.shape )
        nb_cloud_points = cloud.shape[0]
        row_ids = np.array([])
        if nb_cloud_points >= nb_points:
            row_ids = np.random.choice(nb_cloud_points, nb_points, replace=False)
        else:
            nb_repeats = np.floor(nb_points / nb_cloud_points)
            nb_spare = int(nb_points - nb_repeats * nb_cloud_points)
            duplicate_ids = np.arange(nb_cloud_points).repeat(nb_repeats)
            padding_ids = np.random.choice(nb_cloud_points, nb_spare, replace=False)
            row_ids = np.append(duplicate_ids, padding_ids)
        return (cloud[row_ids, :] - torch.mean(cloud[row_ids, :], dim=0)).to(self.device)

    def __str__(self):
        s = "scheme: " + str(self.scheme) + '\n' + "nb_points: " + str(self.nb_points) + '\n' + "device = " + str(self.device) +'\n' + "proportion: " + str(self.proportion)
        return s

    def update_params(self, scheme, proportion: float = None, nb_points: int = None):
        """

        Args:
            scheme:
            proportion (Optional[]:
            nb_points:

        Returns:

        """
        if self.scheme is 'proportion' and proportion is not None:
            self.proportion = proportion
        elif self.scheme is 'random' and nb_points is not None:
            self.nb_points = nb_points


class WhiteNoise(object):
    """
    Class to add white noise to samples. Samples are generated using __call__
    """

    def __init__(self, sd: float = 0, device='cuda'):
        """
        Args:
            sd: standard deviation of white noise
        """
        self.sd = sd
        self.device = device
        self.normal_dist = torch.distributions.normal.Normal(loc=0, scale=self.sd)

    def __call__(self, cloud: torch.Tensor):
        assert cloud.shape[1] == 3
        return cloud + self.normal_dist.sample(cloud.shape).to(self.device)

    def update_sd(self, sd):
        """
        Function to update the standard deviation of the noise. This method must be used to recreate the normal
         distribution object
        Args:
            sd: standard deviation
        """
        self.sd = sd
        self.normal_dist = torch.distributions.normal.Normal(loc=0, scale=self.sd)


class RandomScalePointCloud(object):
    """Scale Point cloud with option to use a random scaling in the interval [2-scf, scf] 
        If scf = 1.2, the scaling is selected uniformly from the interval [0.8,1.2]
        
        Args:
            scf (Union[float, int, torch.Tensor]): scale factor
            stochastic (bool): True to select a scale factor randomly from a distribution
            inplace (bool): True to apply scaling inplace - avoids memory copying operation 
    """

    def __init__(self, scf: Union[float, int, torch.Tensor], stochastic=False, inplace = True, device='cpu'):
        self.scf = scf
        self.stochastic = stochastic
        self.inplace = inplace
        self.device = device
    
    def __call__(self, cloud: Union[torch.Tensor, PointCloud]):
        assert cloud.shape[1] == 3
        if self.stochastic:
            return pcfunc.scale(cloud, scf=torch.distributions.uniform.Uniform(2-self.scf, self.scf), inplace=self.inplace).to(self.device)
        else:
            return pcfunc.scale(cloud, self.scf, inplace=self.inplace).to(self.device)

class SamplePartialVolume(object):
    """ Filters points outside a randomly generated cubic volume
    If the original cubic volume is 1, the filtered clouds fit in a cubic volume of p
    

    Args:
        p (float): volumetric proportion [0,1]
        eps (float): the threshold to require the cloud to be recentred at origin.
    """
    def __init__(self, p, eps=1e-5):
        assert 0<=p<=1
        self.p = p
        self.l = torch.pow(torch.tensor(p), 1/3)
        self.eps = eps

    def __call__(self, cloud: torch.Tensor):
        assert cloud.shape[1] == 3
        # Ensure centred 
        if torch.max(torch.mean(cloud, dim=0)) > self.eps:
            cloud = cloud - torch.mean(cloud, dim=0)
        xyz_max = torch.max(cloud, dim=0).values
        xyz_min = torch.min(cloud, dim=0).values
        box_lengths = xyz_max - xyz_min 
        u_dist = torch.distributions.uniform.Uniform(xyz_min, xyz_max-self.l* box_lengths)
        lower = u_dist.sample()
        upper = lower + self.l*box_lengths
        
        # Select volume 
        print(lower, upper)
        condition1 = cloud>=lower
        row_condition1 = condition1.all(1)
        cloud = cloud[row_condition1,:]
        
        condition2 = cloud<=upper
        row_condition2 = condition2.all(1)
        cloud = cloud[row_condition2,:]
        # condition= torch.logical_and(row_condition1, row_condition2) #not in this torch 
        return cloud

class SampleKMeans(object):
    def __init__(self, p, device='cpu') -> None:
        self.p = p
        self.device = device
        
    def __call__(self, cloud: torch.Tensor):
        assert cloud.shape[1] == 4
        n_clusters = torch.unique(cloud[:, 3]).shape[0]
        n_desired_clusters = np.ceil(self.p*n_clusters)
        if n_clusters == n_desired_clusters:
            return cloud[:,:3]
        cluster_idxs = list(np.random.choice(n_clusters, int(n_desired_clusters), replace=False))
        
        unique_selected_idxs = set(cluster_idxs)
        df = pd.DataFrame(cloud, columns=['x', 'y', 'z', 'cid'], dtype=np.float)
        mask = df['cid'].isin(unique_selected_idxs)
        df_subset = df[mask]
        points = df_subset[['x', 'y', 'z']].to_numpy()

        return torch.Tensor(points).to(self.device)

    def __str__(self):
        repr = {
            "transform" : "SampleKMeans",
            "p": self.p
        }
        return repr

class VoxelMLSFilter(object):
    """ A class to perform moving least squares and a voxel filtering to an input cloud"""
    def __init__(self, MLS_radius: float, leafsize: float, device = 'cpu'):
        """
        Args:
            MLS_radius: Moving least squares k-nn radius
            leafsize: length of the voxel cube
        """
        self.MLS_radius = MLS_radius
        self.leafsize = leafsize
        self.device = device

    def __call__(self, x: torch.Tensor):
        assert x.shape[1] == 3
        # assert points is np.ndarray
        arr = x.cpu().numpy()
        cloud = pcl.PointCloud(x.cpu().numpy())
        # Moving least Squares and voxel filtering
        MLS = cloud.make_moving_least_squares()  # type: pcl.MovingLeastSquares
        MLS.set_search_radius(self.MLS_radius)
        cloud = MLS.process()
        voxel = cloud.make_voxel_grid_filter() #     type pcl.VoxelGridFilter
        voxel.set_leaf_size(self.leafsize, self.leafsize, self.leafsize)
        cloud = voxel.filter()
        return torch.Tensor(cloud.to_array()).to(self.device)


if __name__ == '__main__':
    A = torch.tensor([[1, 2],[3,4]])
    condition = A >= torch.tensor([2,1])
    print(condition)
    print(condition.all(dim=1))
    print(A[condition.all(dim=1)])
    filename = '/EyeToHand/src/datasets/filtered_tactile_objs/shampoo.csv'
    cloud = pd.read_csv(filename, header=None)
    cloud = torch.tensor(cloud.to_numpy())
    print(cloud.shape)
    sampler= SamplePartialVolume(p=0.5)
    fcloud, low, high  = sampler(cloud)
    print(fcloud.shape)
    fig = plt.figure()
    ax = fig.add_subplot(1,2,1, projection='3d')
    ax.scatter(cloud[:,0], cloud[:,1], cloud[:,2], c=cloud[:,2], cmap='Blues')
    ax.scatter(low[0], low[1], low[2], c='r')
    ax.scatter(high[0], high[1], high[2], c='g')

    ax = fig.add_subplot(1,2,2, projection='3d')
    ax.scatter(low[0], low[1], low[2], c='r')
    ax.scatter(high[0], high[1], high[2], c='g')

    ax.scatter(fcloud[:,0], fcloud[:,1], fcloud[:,2], c=fcloud[:,2], cmap='Blues')
    
    plt.savefig(fname='sample_partial_volume_shampoo.png')