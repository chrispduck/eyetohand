import CloudTransforms as mytfs
import kaolin.transforms as tfs
from CloudDataloading import SingleModelDataset
from torch.utils.data import DataLoader
from VisualiseCloud import visualize_batch
from tqdm import tqdm 
import numpy as np

if __name__ == '__main__':
    device = 'cuda'
    n_points =1024
    batchsize =10
    categories = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'soda_can', 'shampoo', 'tape']
    MLS_radius =0.5
    leafsize=0.35
    ps= [0.1, 0.4, 0.7]
    dataset_root = "../datasets/kmeans/tactile/real/raw/"
    for p in ps:
        tf_kmeans = tfs.Compose([
            mytfs.SampleKMeans(p=p, device='cuda'),
            mytfs.VoxelMLSFilter(MLS_radius=0.5, leafsize=0.35, device='cuda'),
            mytfs.SamplePoints(scheme='random', nb_points=n_points)
            # mytfs.RandomRotatePointCloud(device=device)
        ])

        dataset = SingleModelDataset(
            dataset_root, categories, transform=tf_kmeans, device=device, n_repeats=50)
        dataloader = DataLoader(
            dataset, batch_size=batchsize, shuffle=False, drop_last=True)

        batch, labels = next(iter(dataloader))
        visualize_batch(batch, labels, categories, savefig=True, fname='sampling_images/onlinefilter-MLS' + str(MLS_radius) + '-Leaf' + str(leafsize) + '-p' +str(p))


