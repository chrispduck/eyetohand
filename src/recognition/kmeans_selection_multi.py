from torch.utils import data
from kmeans_sampling_single import select_clusters
import pandas as pd
import numpy as np
import os
import torch
from tqdm import tqdm
from VisualiseCloud import visualize_batch
from CloudDataloading import CloudDataset
from torch.utils.data import DataLoader, dataloader
import CloudTransforms as mytfs
import kaolin.transforms as tfs
import matplotlib.pyplot as plt

def generate_k_means_selections(indir: str, outdir: str, p: float, n_repeats: int, categories: list, split='train'):
    # create out directory
    outdir = outdir + 'p-' + str(p) + '/'
    if not os.path.isdir(outdir):
        os.mkdir(outdir)

    for cat in categories:
        print(cat)
        fname_in = indir + cat + '.csv'
        object_folder = outdir + cat + '/'
        if not os.path.isdir(object_folder):
            os.mkdir(object_folder)
        subfolder = object_folder + split + '/'
        if not os.path.isdir(subfolder):
            os.mkdir(subfolder)
        # load kmeans

        x = np.loadtxt(fname_in, delimiter=',')
        print(x.shape)
        for idx in range(n_repeats):
            sample, _labels = select_clusters(x[:, :3], x[:, 3], p=p)
            fname_out = subfolder + cat + '_' + str(idx).zfill(4) + '.csv'
            np.savetxt(fname_out, sample, delimiter=',')


if __name__ == '__main__':
    n_repeats = 400
    nb_points=1024 #only for plotting
    # split="train"
    folders = [
                # "../datasets/kmeans/tactile/real/raw/",
            #    "../datasets/kmeans/tactile/real/filtered/",
            #    "../datasets/kmeans/vision/real/raw/",
            #    "../datasets/kmeans/vision/real/filtered/"
                "../datasets/kmeans/vision/real/filtered_chopped/"

               ]

    graph_names_bases = [
                        # "sampling_images/p-selection/tactile_raw_",
                        # "sampling_images/p-selection/tactile_filtered_",
                        # "sampling_images/p-selection/vision_raw_",
                        # "sampling_images/p-selection/vision_filtered_",
                        "sampling_images/p-selection/vision_filtered_chopped_"
                        ]

    categories = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange',
                  'pack_of_cards', 'rubix_cube', 'spam', 'shampoo', 'tape']
    # categories = ['spam']

    for folder, graph_name_base in zip(folders, graph_names_bases):
        for split in ['train', 'test']:
            for p in [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]:
                print("  p =  {} ".format(p).center(30, '#'))
                # Create and save selection samples
                generate_k_means_selections(
                    indir=folder, outdir=folder, p=p, n_repeats=n_repeats, categories=categories, split=split)
                
                # Visualise
                tf = tfs.Compose(
                    [mytfs.SamplePoints(nb_points=nb_points, device='cuda')])
                dataset = CloudDataset(basedir=folder + 'p-' + str(p) + '/', split=split, categories=categories, transform=tf)
                dataloader = DataLoader(
                    dataset, batch_size=len(categories), shuffle=True, drop_last=True)
                batch, labels = next(iter(dataloader))
                fig = visualize_batch(pointclouds=batch, savefig=True,
                                fname=graph_name_base+str(p), labels=labels, categories=categories, hide_axis=False)
                plt.close()