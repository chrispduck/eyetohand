from typing import Union

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import torch


def visualize_batch(pointclouds: list, labels: torch.tensor, categories, pred_labels=None, savefig=False, fname='', hide_axis=True):
    """[summary]

    Args:
        pointclouds (list(np.array)): [list of pointclouds]
        labels (torch.tensor): numeric labels 
        categories (list(string)): string ground truth labels
        pred_labels (torch.tensor, optional): [description]. Defaults to None.
        savefig (bool, optional): [description]. Defaults to False.
        fname (str, optional): [description]. Defaults to 'visualisation'.
    """
    
    batch_size = len(pointclouds)
    fig = plt.figure()
    # fig = plt.figure(figsize=(8, batch_size / 2))

    ncols = 4 if (batch_size == 12) else 3
    nrows = max(1, np.ceil(batch_size / ncols))

    for idx, pc in enumerate(pointclouds):
        idx = int(idx)
        label = categories[int(labels[idx].item())]
        pc = pc.cpu().numpy()
        ax = fig.add_subplot(nrows, ncols, idx + 1, projection='3d')
        if pred_labels is not None:
            pred = categories[int(pred_labels[idx].item())]
            colour = 'g' if label == pred else 'r'
            ax.set_title('GT: {0}\n Pred: {1}'.format(label, pred), fontsize=12)
        else:
            colour = 'b'
            ax.set_title('GT: {0}'.format(label), fontsize=6)
        ax.scatter(pc[:, 0], pc[:, 1], pc[:, 2], c=pc[:, 2], s=1)
        fix_3D_aspect(ax=ax, cloud=pc)
        if hide_axis:
            ax.axis('off')
    if savefig:
        assert fname is not ''
        plt.savefig(fname + '.png', filetype='png')
    else:
        plt.show()
    return fig


def fix_3D_aspect(ax: plt.Axes, cloud: Union[torch.Tensor, np.ndarray]):
    X = cloud[:, 0]
    Y = cloud[:, 1]
    Z = cloud[:, 2]
    max_range = np.array([X.max() - X.min(), Y.max() - Y.min(), Z.max() - Z.min()]).max() / 2.0
    mid_x = (X.max() + X.min()) * 0.5
    mid_y = (Y.max() + Y.min()) * 0.5
    mid_z = (Z.max() + Z.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)


def plot_single_cloud(cloud: torch.Tensor = None, x=None, y=None, z=None, title=None, c=None, savefig=False, show=True, fname='', fix_aspect=True):
    assert cloud.shape
    if c is None:
        c = 'b'
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    assert (cloud is not None) or (i is not None for i in [x, y, z])
    if cloud is None:
        ax.scatter(x, y, z, s=2, c=c)
    else:
        ax.scatter(cloud[:, 0], cloud[:, 1], cloud[:, 2], s=2, c=c)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.set_title(title)
    if fix_aspect:
        fix_3D_aspect(ax, cloud)
    if savefig:
        assert fname is not ''
        plt.savefig(fname)
    if show:
        plt.show()
