import os
from glob import glob
from typing import Callable, Iterable, Optional, Union, List

import numpy as np
from numpy.testing._private.utils import decorate_methods
import torch
from torch import dtype
from torch.utils.data import dataset
from torch.utils.data.dataset import Dataset
from CloudTransforms import Curriculum


class CloudDataset(Dataset):
    r""" Dataset class for ETH' datasets, .

    Args:
        basedir (str): Path to the base directory of the Pointcloud dataset.
        split (str, optional): Split to load, 'train' or 'test',
            (default: 'train').
        categories (iterable, optional): List of categories to load
            (default: ['rubix_cube']).
        transform (callable, optional): A function/transform to apply on each
            loaded example.
        device (str or torch.device, optional): Device to use (cpu,
            cuda, cuda:1, etc.).  Default: 'cpu'
        category_offset (int): use if concatenating datasets with different classes
            (Default: '0')
        dropout (Curriculum): Used to replicate partial exploration
            (Default: None)

    Examples:
    # >>> dataset = CloudDataset(basedir='data/TactileData')
    # >>> train_loader = DataLoader(dataset, batch_size=10, shuffle=True, num_workers=8)
    # >>> obj, label = next(iter(train_loader))

    """

    def __init__(self, basedir: str,
                 split: Optional[str] = 'train',
                 categories: Optional[Iterable] = None,
                 transform: Optional[Callable] = None,
                 device: Optional[Union[torch.device, str]] = 'cpu',
                 category_offset: Optional[int] = 0,
                 dropout: Optional[Union[Curriculum, None]] = None):

        if categories is None:
            categories = ['rubix cube']
        assert split.lower() in ['train', 'test']
        self.basedir = basedir
        self.transform = transform
        self.device = device
        self.categories = categories
        self.names = []
        self.filepaths = []
        self.cat_idxs = []
        self.dropout = dropout
        if not os.path.exists(basedir):
            raise ValueError('Data was not found at "{0}".'.format(basedir))

        available_categories = [p for p in os.listdir(basedir) if os.path.isdir(os.path.join(basedir, p))]

        for cat_idx, category in enumerate(categories):
            assert category in available_categories, 'object class {0} not in list of available classes: {1}'.format(
                category, available_categories)

            cat_paths = glob(os.path.join(basedir, category, split.lower(), '*.csv'))

            self.cat_idxs += [cat_idx + category_offset] * len(cat_paths)
            self.names += [os.path.splitext(os.path.basename(cp))[0] for cp in cat_paths]
            self.filepaths += cat_paths

    def __len__(self):
        """
        Returns:
            length (int): number of data items stored in the Dataloader
        """
        return len(self.names)

    def __getitem__(self, index):
        """
        Returns the item at index idx

        Args:
            index(int):
        Returns:
            (tuple): tuple containing:
                data (int): data item
                category (int): category of item
        """
        label = torch.tensor(self.cat_idxs[index], dtype=torch.float, device=self.device)
        # print(label)
        np_data = np.loadtxt(self.filepaths[index], delimiter=',')
        data = torch.tensor(np_data, dtype=torch.float, device=self.device)
        if self.transform:
            data = self.transform(data)
        if self.dropout:
            data = self.dropout.drop(data)
        return data, label


class ConcatDataset(Dataset):
    """
    A class to concatenate several datasets into a single dataset.
    Returns a single item from index i for every dataset when __getitem__ is called.
    """

    def __init__(self, *datasets: Union[List, tuple]):
        """

        Args:
            *datasets (Union[List, tuple]):
        """

        self.datasets = datasets

    def __getitem__(self, i):
        return tuple(d[i] for d in self.datasets)

    def __len__(self):
        return min(len(d) for d in self.datasets)


class ConcatTwoDatasets(Dataset):
    """
    Concatenates two dataset and returns items as if it were one long dataset.
    """

    def __init__(self, *datasets):
        self.datasets = datasets

    def __getitem__(self, i):
        if i < self.datasets[0].__len__():
            return self.datasets[0].__getitem__(i)
        else:
            return self.datasets[1].__getitem__(i - self.datasets[0].__len__())

    def __len__(self):
        return self.datasets[0].__len__() + self.datasets[1].__len__()


def ComputePartition(dataset, frac: float):
    """Compute the partition lengths for two datasets"""
    len_all = dataset.__len__()
    len_1 = np.floor(frac * len_all)
    len_2 = len_all - len_1
    return [int(len_1), int(len_2)]


class SingleModelDataset(Dataset):
    def __init__(self, path: str, obj_names: list, labels=None, transform=None, device='cpu', n_repeats=100) -> None:
        self.obj_names = obj_names
        self.fnames = [path  + obj + '.csv' for obj in obj_names] * n_repeats
        self.device = device
        self.transform = transform
        self.labels = torch.cat(n_repeats*[torch.tensor(labels, dtype=torch.long, device=device)]) if labels else torch.cat(n_repeats*[torch.arange(0,len(obj_names), dtype=torch.long, device=self.device)])
        # print("fnames", self.fnames)
        # print("labels", self.labels)

    def __len__(self):
        return len(self.fnames)
    
    def __getitem__(self, index):
        label = self.labels[index].clone().float().to(self.device)
        # print(label)
        np_data = np.loadtxt(self.fnames[index], delimiter=',')
        data = torch.tensor(np_data, dtype=torch.float, device=self.device)
        if self.transform: 
            data = self.transform(data)
        return data, label

    def __str__(self):
        s_dict = {
            "root_dir" : self.fnames[0],
            "n_models": len(self.fnames),
            "device" : self.device,
            "transform": self.transform,
            "labels": self.obj_names
        }
        return  "\n".join(str(key)+ ': ' + str(value) for key, value in s_dict.items())

    