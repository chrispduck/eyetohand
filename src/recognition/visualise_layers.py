import os, keyword, pickle, Hooks, torch
import pandas as pd
from typing import Union
import pandas.plotting as pdplot
import matplotlib
matplotlib.use('Agg')  #required for plotting on a remote server
import matplotlib.pyplot as plt
import kaolin.transforms as tfs
from torch.utils.data import DataLoader
import CloudTransforms as mytfs
from PointNetMedium import PointNet
from CloudDataloading import SingleModelDataset
from VisualiseCloud import plot_single_cloud, visualize_batch
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm
from sklearn.decomposition import PCA, FastICA
from sklearn.manifold import TSNE 
import seaborn as sns
import numpy as np
from VisualiseCloud import fix_3D_aspect

# from sklearn.preprocessing import StandardScaler


class VisualisePointNetLayers:
    def __init__(self, trained_model_path, dataset_path, obj_names,batchsize=128, n_points=1024, device='cpu', categories=None) -> None:
        super().__init__()
        self.trained_model_path = trained_model_path
        self.dataset_path = dataset_path
        self.obj_names = obj_names
        self.batchsize= batchsize
        self.categories = categories if categories else ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange',
                'pack_of_cards', 'rubix_cube', 'rugby_ball', 'soda_can', 'shampoo', 'spam', 'tape']
        self.device = device
        self.num_points = n_points
        self.activations, self.snapshot = {}, {}
        self._prepare_model()
        self._prepare_data()
        self._register_hooks()

    def _prepare_model(self):
        self.model = PointNet(classes=len(self.categories)).to(self.device)
        self.model.load_state_dict(torch.load(self.trained_model_path))
        return
    
    def _prepare_data(self):
        self.transform = tfs.Compose([
        # mytfs.SamplePartialVolume(0.5),
        mytfs.SamplePoints(scheme='random', nb_points=self.num_points),
        mytfs.RandomRotatePointCloud(device=self.device),
        mytfs.RandomScalePointCloud(scf=1.05)
        ])
        self.labels = [self.categories.index(name) for name in self.obj_names]
        self.dataset = SingleModelDataset(self.dataset_path, self.obj_names, self.labels, self.transform, self.device)
        self.dataloader = DataLoader(self.dataset, batch_size=self.batchsize,
                        shuffle=False, num_workers=0)
        return

    def _register_hooks(self): 
        self.model.transform.conv1a.register_forward_hook(Hooks.get_activation('transformed_input', self.activations))
        self.model.transform.feature_transform.conv1.register_forward_hook(Hooks.get_activation('extracted_features', self.activations))
        self.model.transform.conv2.register_forward_hook(Hooks.get_activation('transformed_features', self.activations ))
        self.model.transform.bn3.register_forward_hook(Hooks.get_activation('embeddings', self.activations)) 
        self.model.fc1.register_forward_hook(Hooks.get_activation('global_features', self.activations))
    
    def take_snapshot(self):
        input, gt_labels = next(iter(self.dataloader))
        output, matrix3x3, matrix64x64 = self.model.forward(input, return_transforms=True)
        # print(self.activations['extracted_features'][0][0].shape)
        self.snapshot['input'] = input.detach().cpu()
        self.snapshot['matrix3x3'] = matrix3x3.detach().cpu()
        self.snapshot['transformed_input'] = self.activations['transformed_input'][0][0].detach().cpu()
        self.snapshot['extracted_features'] = self.activations['extracted_features'][0][0].detach().cpu()
        self.snapshot['matrix64x64'] = matrix64x64.detach().cpu()
        self.snapshot['transformed_features'] = self.activations['transformed_features'][0][0].detach().cpu() 
        self.snapshot['embeddings'] = self.activations['embeddings'][1].detach().cpu() #[1] indicates output fr
        self.snapshot['global_features'] = self.activations['global_features'][0][0].detach().cpu()
        self.snapshot['output'] = output.detach().cpu() #numeric (float)
        self.snapshot['pred_labels'] = output.argmax(dim=1).detach().cpu() #numeric (int)
        self.snapshot['gt_labels'] = gt_labels.detach().cpu() #numeric (int)
        d = {number: name for (number, name) in enumerate(self.obj_names)}  
        df_gt_labels = pd.DataFrame(self.snapshot['gt_labels'], columns=['gt_labels'])
        self.snapshot['str_labels'] = df_gt_labels['gt_labels'].map(d).rename('str_labels') # dataframe of strings
        print(self.snapshot['str_labels'])
        
    def save_snapshot(self, fname: str):
        with open(fname, 'wb') as fp:
            pickle.dump(self.snapshot, fp, protocol=pickle.HIGHEST_PROTOCOL)
    
    def load_snapshot(self, fname:str):
        with open(fname, 'rb') as fp:
            self.snapshot = pickle.load(fp)

    def plot_extracted_features(self, dirname):
        cloud = self.snapshot['extracted_features'][2].numpy().transpose()
        category = self.categories[int(self.snapshot['gt_labels'][2])]
        print(cloud.shape)
        
        n_cats = len(self.categories)
        n_models = len(self.obj_names)
        # print("ncats: ", n_cats)
        n_plots = 8 # plots per cat
        figures = []
        for i, cat in enumerate(self.obj_names):
            clouds = []
            for j in range(n_plots):
                index = self.categories.index(cat)
                # print(cat, index, index + n_models*j)
                clouds.append(self.snapshot['extracted_features'][i + n_models*j].transpose(0,1))
                # print(clouds[0].shape)
                # print(index, int(snapshot['gt_labels'][i+n_models*j]))        
                assert index == int(self.snapshot['gt_labels'][i+n_models*j])
            labels = torch.zeros(n_plots).int() + index
            fig = visualize_batch(clouds, labels, self.categories, savefig=True, fname=dirname + cat)
            figures.append(fig)
        return figures

    def create_tensorboard(self, logdir):
        assert len(self.snapshot) != None
        self.writer = SummaryWriter(log_dir=logdir + 'tensorboard/')
        # In order of features
        figs = self.plot_extracted_features(logdir)
        print("len figs: ", len(figs))
        for i, fig in enumerate(figs):
            self.writer.add_figure('extracted_features_'+str(i), fig)
        # print(self.snapshot['transformed_features'].shape)
        
        # add embedding
        # print(self.snapshot['global_features'].shape)
        self.writer.add_embedding(self.snapshot['transformed_features'], self.snapshot['gt_labels'], tag='transformed features')
        self.writer.add_embedding(self.snapshot['global_features'], self.snapshot['gt_labels'], tag='pooled features')

        meta = []
        while len(meta)<100:
            meta = meta+keyword.kwlist # get some strings
        meta = meta[:100]

        for i, v in enumerate(meta):
            meta[i] = v+str(i)

        label_img = torch.rand(100, 3, 10, 32)
        for i in range(100):
            label_img[i]*=i/100.0

        self.writer.add_embedding(torch.randn(100, 5), metadata=meta, label_img=label_img)
        self.writer.add_embedding(torch.randn(100, 5), label_img=label_img)
        self.writer.add_embedding(torch.randn(100, 5), metadata=meta)
        # self.writer.flush()
    
    def plot_andrews(self, fname, n_components=4, var=None, transform='pca', feature_name ='global_features'):
        """ plots an andrews curve of pooled 

        Args:
            fname (string): [filename]
            n (int, optional): number of components. Defaults to 4.
            var (float, optional): If set, the number of components is ignored. The n_components is minimised 
                                    whilst capturing at least var percentage of the variance. [0.0-1.0]
            transform (str, optional): Transform using PCA or ICA. Case Insensitive. Defaults to 'pca'.
            feature_name (str, optional): Featurename to plot. Defaults to 'global_features'

        Raises:
            Exception: [Invalid tranform option, or no snapshot loaded]
        """
        assert len(self.snapshot) != None

        df = pd.DataFrame(self.snapshot[feature_name].numpy()) 
        df_norm  = ((df - df.mean()) / (df.max() - df.min())) # Range of 1
        df_norm_01 = df_norm - df_norm.min() # now in interval [0,1]

        #  Fit transform
        if var:
            t = PCA() if transform.lower() == 'pca' else FastICA()
            all_components = t.fit_transform(df_norm)
            var_cumsum = np.cumsum(t.explained_variance_ratio_)
            n_components = np.argmax(var_cumsum > var) + 1 # note +1
            components = all_components[:, :n_components]  # 
        else:
            t= PCA(n_components) if transform.lower() == 'pca' else FastICA(n_components, random_state=0)
            components = t.fit_transform(df_norm)
        
        df_components = pd.DataFrame(components)
        df_final = pd.concat([df_components, self.snapshot['str_labels']], axis = 1)
        print(df_final.columns)
        fig = plt.figure()
        ax = pdplot.andrews_curves(frame=df_final, class_column='str_labels')
        if var: 
            ax.set_title(str(components.shape[1]) + ' components containing ' + str(var_cumsum[n_components]) + ' of variance' )
        
        plt.savefig(fname)
        plt.close()
        return 
    
    def plot_embedded_dist_3d(self, fname, i=0, j=0, p=2, feature_name='embeddings'):
        """ Plot a the distances of embeddings as a cmap on the input cloud. 

        Args:
            fname (string): foldername
            i (int, optional): Index of point to take distances from [0 to N-1]. Defaults to 0.
            j (int, optional): Index of Sample in batch of size B [0 to B-1]. Defaults to 0.
            p (int, optional): p-norm used to calculate distance. Defaults to 2.
        """
        assert len(self.snapshot) != None
        data = self.snapshot[feature_name] # (B)
        input = self.snapshot['input']     # (B,1024,3)
        # print(input.shape)
        x = data.transpose(dim0=1, dim1=2)
        distances = torch.cdist(x,x,p=p)     # (1024x1024)
        mask = torch.ones(input.shape[1], dtype=torch.bool)   #1024
        mask[i] = 0
        d = distances[j,i][mask]
        points = input[j][mask]
        # print("d -shape: ", d.shape)
        # print('ax.azim {}'.format(ax.azim))
        # print('ax.elev {}'.format(ax.elev))
        
        # plotting
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1, projection='3d')
        ax.scatter(input[j,i,0], input[j,i,1], input[j,i,2], c='r', s=30)
        fix_3D_aspect(ax, points)
        ax = ax.scatter(points[:,0], points[:,1], points[:,2], c=d, s=7, cmap='viridis')
        fig.colorbar(ax)
        
        f = fname + '/'+ feature_name + '_distance_cmap' + str(j)+'_'+str(i)+ '.png'
        plt.savefig(f)
        plt.close()
    

    def plot_tsne(self, fname, feature_name='global_features'):
        assert len(self.snapshot) != None
        tsne = TSNE(n_iter=10000)
        data = self.snapshot[feature_name]
        labels = self.snapshot['gt_labels']
        components = PCA(n_components=30).fit_transform(data)
        print(components.shape)
        tsne_results = tsne.fit_transform(components, )
        results = pd.DataFrame(tsne_results, columns=['t1', 't2'])
        results['str_labels'] = self.snapshot['str_labels']

        # plot figure
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        sns.scatterplot(
            x='t1', y='t2',
            hue='str_labels',
            palette=sns.color_palette("hls", 12),
            data=results,
            legend="full",
            alpha=0.9, 
            ax=ax
        )
        f = fname + '/tsne/tnse.png'
        plt.savefig(f)
    
    def plot_all_andrews(self, fname):
        self.plot_andrews(fname+'/andrews_curve_0.95.png', var=0.95)
        self.plot_andrews(fname+'/andrews_curve_0.90.png', var=0.90)
        self.plot_andrews(fname+'/andrews_curve_0.80.png', var=0.80)
        
    def plot_all(self, fname):
        # Andrews Curve
        self.plot_all_andrews(fname)

        # PCA as histograms  
        #TODO
        
        # T-SNE 
        
        # Embedded distances
        B, N = self.snapshot['input'].shape[0], self.snapshot['input'].shape[1] # Batch size, Nb points
        for n in tqdm(range(0, N-1, 100)):
            for b in range(B):
                path = fname + '/embedded_distances_l1/'
                self.plot_embedded_dist_3d(fname= path, i=n, j=b, p=1)
                path = fname + '/embedded_distances_l2/'
                self.plot_embedded_dist_3d(fname= path, i=n, j=b, p=2)
        

if __name__ == '__main__':
    # trained_model_path = '/EyeToHand/src/recognition/runs/PN_CJ_V2T_n0.05_e50/model.pth'
    # dataset_path = '/EyeToHand/src/datasets/filtered_real_tactile/'
    # obj_names = ['baseball', 'golf_ball', 'pack_of_cards', 'tape']
    torch.cuda.empty_cache()
    trained_model_path = '/EyeToHand/src/recognition/runs/vision_trained_1024p_50ep_1.05scaling_0.05noise/model.pth'
    dataset_path = '/EyeToHand/src/datasets/filtered_vision/'
    obj_names = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange',
                                 'pack_of_cards', 'rubix_cube', 'rugby_ball', 'soda_can', 'shampoo', 'spam', 'tape']

    v = VisualisePointNetLayers(trained_model_path, dataset_path, obj_names, batchsize=64)
    v.take_snapshot()
    # v.save_snapshot('visualisation/vision/vision_trained_1024p_50ep_1.05scaling_0.05noise.p')
    # v.plot_all_andrews('visualisation/vision')
    # v.load_snapshot('visualisation/vision/vision_trained_1024p_50ep_1.05scaling_0.05noise.p')
    v.plot_all('visualisation/vision')
    # v.plot_tsne('visualisation/vision')
    
    # v.plot_tsne(fname='visualisation/vision/tsne1.png')
    
    # v.save_snapshot('visualisation/vision/PN_CJ_V2T_n0.05_e50.p')
    # v.load_snapshot('visualisation/PN_CJ_V2T_n0.05_e50.p')
    # v.create_tensorboard('visualisation/')
    # matplotlib.rcParams['figure.dpi'] = 500
    
    # v.plot_andrews_pooled_embeddings(fname='visualisation/pooled_embedding_andrews_curve_4ica.png', transform='ica')
    # v.plot_andrews_pooled_embeddings(fname='visualisation/pooled_embedding_andrews_curve_10ica.png', transform='ica', n=10)
    # v.plot_andrews_pooled_embeddings(fname='visualisation/pooled_embedding_andrews_curve_20ica.png', transform='ica', n=20)

    # for i in tqdm(range(100)):
    #     for j in range(10):
    #         v.plot_embedded_dist_3d(fname='visualisation/embedded_distances_l1/distance_embedding_cmap'+str(j)+'_'+str(100*i)+'.png',i=10*i,j=j, p=1)
