python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_wo_none" --cats "baseball" "beer" "camera_box" "golf_ball" "orange" "pack_of_cards" "rubix_cube" "shampoo" "soda_can"  "tape"
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_wo_baseball" --cats "beer" "camera_box" "golf_ball" "orange" "pack_of_cards" "rubix_cube" "shampoo" "soda_can"  "tape"
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_wo_beer" --cats "baseball" "camera_box" "golf_ball" "orange" "pack_of_cards" "rubix_cube" "shampoo" "soda_can"  "tape"
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_wo_camerabox" --cats "baseball" "beer" "golf_ball" "orange" "pack_of_cards" "rubix_cube" "shampoo" "soda_can"  "tape"
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_wo_orange" --cats "baseball" "beer" "camera_box" "golf_ball" "pack_of_cards" "rubix_cube" "shampoo" "soda_can"  "tape"
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_wo_beer_orange" --cats "baseball" "camera_box" "golf_ball" "pack_of_cards" "rubix_cube" "shampoo" "soda_can"  "tape"
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_wo_beer_camerabox" --cats "baseball" "camera_box" "golf_ball" "orange" "pack_of_cards" "rubix_cube" "shampoo" "soda_can"  "tape"
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_wo_beer_baseball" --cats "camera_box" "golf_ball" "orange" "pack_of_cards" "rubix_cube" "shampoo" "soda_can"  "tape"

