import pandas as pd
from sklearn.cluster import KMeans
import numpy as np
import matplotlib
matplotlib.use('Agg')  #required for plotting on a remote server
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from tqdm import tqdm

def select_clusters(X: np.array, labels: np.array, p: float):
    """Select clusters from a partitioned point cloud based on a percentage of overall cloud

    Args:
        points (np.array): pointcloud (nx3)
        labels (np.array): labels (nx1)
        p (float): percentage of clusters to keep 

    Returns:
        np.array: pointcloud
    """
    # number of clusters
    nb_clusters = len(set(labels))
    # number of clusters desired
    nb_desired = int(np.ceil(nb_clusters*p))
    # randomly choose the clusters - use discrete unifrom dist. without replacement
    cluster_ids = np.random.choice(labels, size=nb_desired, replace=False)
    # indices of the pointcloud corresponding to those clusters
    indices = np.isin(labels, cluster_ids)
    # return resulting cloud
    return X[indices], labels[indices]

def generate_k_means_samples(indir: str, outdir:str , cluster_size:int, categories: list, header=None) -> None:
    fnames_in = [indir + category + '.csv' for category in categories]
    fnames_out = [outdir + category + '.csv' for category in categories]
    for f_in, f_out in tqdm(zip(fnames_in, fnames_out)):
        if header:
            df = pd.read_csv(f_in)
            X = df.loc[:, ['x', 'y', 'z']]
        else:
            X = pd.read_csv(f_in, header=None)
        # Compute kmeans_labels
        labels = compute_k_means(X.to_numpy(), cluster_size=cluster_size)
        X_ = np.concatenate([X, np.expand_dims(labels, axis=1)], axis=1)
        np.savetxt(f_out, X_, delimiter=',')
        print(f_out)
    

def compute_k_means(X: np.array, cluster_size=6): 
    # Compute number of clusters for KMeans computation
    nb_clusters = int(np.floor(X.shape[0]/cluster_size))
    # Perform Kmeans
    kmeans = KMeans(n_clusters=nb_clusters, random_state=0).fit(X)
    # Extract Labels
    labels=kmeans.labels_
    return labels



def example():
    # Read Pointcloud
    fname = '../datasets/filtered_tactile_objs/baseball.csv'
    X = pd.read_csv(fname, header=None).to_numpy() # pd.Dataframe
    nb_points = len(X)

    # Compute number of clusters for KMeans computation
    cluster_size = 6
    nb_clusters = int(np.floor(nb_points/cluster_size))
    # Perform Kmeans
    kmeans = KMeans(n_clusters=nb_clusters, random_state=0).fit(X)
    # Extract Labels
    labels=kmeans.labels_

    fig = plt.figure()
    figname = 'Kmeans-sampling.png'
    matplotlib.rcParams['figure.dpi']=4000
    fig.suptitle('K-Means Sampling')

    ax = fig.add_subplot(2,2,1, projection='3d') #plt.Axis
    ax.scatter(X[:,0], X[:,1], X[:,2], c=labels, cmap='viridis')
    plt.title('Clustered Points. 100%')

    ax = fig.add_subplot(2,2,2, projection='3d')
    X30, X30_labels = select_clusters(X, labels, p=0.3)
    plt.title('Clustered Points. 30%')
    ax.scatter(X30[:,0], X30[:,1], X30[:,2], c=X30_labels, cmap='viridis')

    ax = fig.add_subplot(2,2,3, projection='3d')
    X60, X60_labels = select_clusters(X, labels, p=0.6)
    plt.title('Clustered Points. 60%')
    ax.scatter(X60[:,0], X60[:,1], X60[:,2], c=X60_labels, cmap='viridis')

    ax = fig.add_subplot(2,2,4, projection='3d')
    X80, X80_labels = select_clusters(X, labels, p=0.8)
    plt.title('Clustered Points. 80%')
    ax.scatter(X80[:,0], X80[:,1], X80[:,2], c=X80_labels, cmap='viridis')

    plt.savefig(figname, dpi=1000 )


if __name__ == '__main__':

    indirs = [
            # '../datasets/single/tactile/real/raw/',
            # '../datasets/single/tactile/real/filtered/'
            #   '../datasets/single/vision/real/raw/',
            #   '../datasets/single/vision/real/filtered/'
              "../datasets/kmeans/vision/real/filtered_chopped/"

    ]

    outdirs = [ 
                # '../datasets/kmeans/tactile/real/raw/',
                # '../datasets/kmeans/tactile/real/filtered/'
                # '../datasets/kmeans/vision/real/raw/',
                # '../datasets/kmeans/vision/real/filtered/'
                "../datasets/kmeans/vision/real/filtered_chopped/"

            ]
    
    # headers = [True, False, False, False]
    headers = [False, False]
    
    cluster_size=6 
    categories = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'soda_can', 'shampoo', 'tape']
    # categories = ["spam"]
    for indir, outdir, header in zip(indirs, outdirs, headers):
        generate_k_means_samples(indir, outdir, cluster_size, categories, header=header)