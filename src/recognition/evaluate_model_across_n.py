from pandas.core import base
from torch.utils import tensorboard
from PointNetMedium import PointNet
from CloudDataloading import CloudDataset, SingleModelDataset
import kaolin.transforms as tfs, CloudTransforms as mytfs
from torch.utils.data import DataLoader
from PointClassificationEngine import ClassificationEngine as Engine
from tqdm import tqdm
import os, numpy as np, torch, pandas as pd, time

def run(s, evaluate:bool=False):
    
    model = PointNet(classes=len(s["cats"])).to(s["device"])
    tf_train = tfs.Compose([
        mytfs.SamplePoints(scheme='random', nb_points=s["n_points"], device=s["device"]),
        mytfs.RandomRotatePointCloud(device=s["device"]),
        mytfs.WhiteNoise(sd=s["training_noise"], device=s["device"]),
        mytfs.RandomScalePointCloud(scf=s["scf"], device=s["device"])
    ])
    tf_test = tfs.Compose([
        mytfs.SamplePoints(scheme='random', nb_points=s["n_points"], device=s["device"]),
        mytfs.RandomRotatePointCloud(device=s["device"])
    ])

    dataset_train = SingleModelDataset(
        s["training_root"], s["cats"], transform=tf_train, device=s["device"], n_repeats=s["n_repeats_train"])
    dataset_val = SingleModelDataset(
        s["val_root"], s["cats"], transform=tf_test, device=s["device"], n_repeats=s["n_repeats_val"])

    train_loader = DataLoader(
        dataset_train, batch_size=s["batch_size_train"], shuffle=True, drop_last=True)
    val_loader = DataLoader(
        dataset_val, batch_size=s["batch_size_train"], shuffle=True, drop_last=True)
    optimiser = torch.optim.Adam(model.parameters(), lr=s["lr"])
    criterion = torch.nn.CrossEntropyLoss().to(s["device"])
    tensorboard_params = {
        'name': s["tensorboard_name"],
        'dir': "runs/"+ s["tensorboard_name"],
        'cats': s["cats"],
        'nb_cats': len(s["cats"])
    }
    engineparams = {
        'epochs': s["epochs"]
    }

    engine = Engine(model=model, train_loader=train_loader, val_loader=val_loader,
                    tensorboard=tensorboard_params,
                    optimizer=optimiser, device=s["device"], engineparams=engineparams, criterion=criterion
                    )

    engine.load_model(s["model_to_load"])
    final_val=0

    # Column names for dataframe which will store all the 
    df = pd.DataFrame(columns=s["col_names"])

    ### Test over a range of proportions/ nb unique
    col_names = ["tensorboard_name", "scheme", "scheme-param", "tac_nof", "tac_filt", "vis_filt", 'final_val']
    df = pd.DataFrame(columns=col_names)
    # nb_points = torch.logspace(5,11,1,2) # 64->2048 in 13 in 13 steps.


    tactile_filt_dataset = CloudDataset(
        basedir=s["tactile_filtered_root"], categories=s["cats"], split='test', transform=tf_test, device=s["device"])
    # tactile_filt_dataset = SingleModelDataset(
            # s["tactile_filtered_root"], obj_names=s["cats"], transform=tf_test, device=s["device"], n_repeats=200)
    tactile_filt_dataloader = DataLoader(tactile_filt_dataset, batch_size=s["batch_size_test"],
                                        shuffle=True, drop_last=True)

    raw_tactile_dataset = CloudDataset(basedir=s["tactile_raw_root"],categories=s["cats"], split='test', transform=tf_test, device=s["device"])
    # raw_tactile_dataset = SingleModelDataset(
    #         s["tactile_raw_root"], obj_names=s["cats"], transform=tf_test, device=s["device"], n_repeats=200)
    raw_tactile_dataloader = DataLoader(raw_tactile_dataset, batch_size=s["batch_size_test"], shuffle=True, drop_last=True)

    vision_dataset_test = CloudDataset(
        basedir=s["vision_root"], categories=s["cats"],split='test', transform=tf_test, device=s["device"])
    # vision_dataset_test = SingleModelDataset(
    #         s["vision_root"], obj_names=s["cats"], transform=tf_test, device=s["device"], n_repeats=200)
    vision_test_loader = DataLoader(
        vision_dataset_test, batch_size=s["batch_size_test"], shuffle=True, drop_last=True)


    print("NB-POINTS")
    tf_test.transforms[0].scheme = "random"
    # print(tf_test.transforms[0])
    for nb in reversed(s["n_range"]):
        print(int(nb))
        tf_test.transforms[0].nb_points = int(nb)
        # engine.visualise_batch(tactile_dataloader, savefig=True, fname='tac_filt_'+str(nb_points))
        vis_filt = engine.predict(vision_test_loader, 'vis_filt' +str(int(nb)))
        tac_filt = engine.predict(tactile_filt_dataloader, 'tac_filt_split'+str(int(nb)))
        tac_nof = engine.predict(raw_tactile_dataloader,'tac_nof_split'+str(int(nb)) )
        df_itr = pd.DataFrame([[s["tensorboard_name"], 'nb_points', int(nb), tac_nof, tac_filt, vis_filt, final_val]], columns=col_names)
        df = pd.concat([df, df_itr])
        df.to_csv('runs/' + s["tensorboard_name"] + '/results.csv')
    
    return df 

if __name__ == "__main__":
    
    col_names = ["tensorboard_name", "p_kmeans", "tac_nof", "tac_filt", "vis_nof", "vis_filt", 'final_val']

    settings ={
        "n_runs": 1,
        "epochs": 80,
        "n_range": torch.logspace(4,10,13,2),
        "cats": ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'spam', 'shampoo', 'tape'],

        "tensorboard_name": "Feb22_curricA_npoints",
        # "model_to_load": "experiment1_b",
        # "model_to_load": "viskmeans_trial_19thfeb_10", #Best Curriculum puzzle trained 
        # "model_to_load": "viskmeans_control_20thfeb_00", #Best non - curriculum trained.
        "model_to_load": "Feb21st_viskmeans_curric_overnight50",

        # "tactile_filtered_root": "../datasets/kmeans_v2/tactile3/filtered/1.0/",
        # "tactile_raw_root": "../datasets/kmeans_v2/tactile3/raw/1.0/",
        # "vision_root": "../datasets/kmeans_v2/vision/filtered/1.0/",

        # original dataset
        "tactile_filtered_root": "../datasets/kmeans_v3/tactile3/filtered/1.0/",
        "tactile_raw_root": "../datasets/kmeans_v3/tactile3/filtered/1.0/",
        "vision_root": "../datasets/kmeans_v3/vision/filtered/1.0/",
        
        # To ignore
        "training_root": "../datasets/single/vision/real/filtered_chopped/train/",
        "val_root": "../datasets/single/vision/real/filtered_chopped/test/",

        "n_points": 1024,
        "lr": 1e-2,
        "batch_size_train": 12,
        "batch_size_test": 12,
        "device": 'cuda',
        "scf": 1.03,
        "training_noise": 0.05,
        "MLS_radius": 0.5,
        "leafsize": 0.35,
        "n_repeats_train": 200,
        "n_repeats_val": 84,
        "n_repeats_test": 200, 
        "col_names": col_names
    }

    df = pd.DataFrame(columns=col_names)
    tstart= time.time()
    tensorboard_base = settings["tensorboard_name"] 
    for run_idx in tqdm(range(settings["n_runs"])):
        settings["tensorboard_name"] = tensorboard_base + str(run_idx) 
        df_single_run = run(settings, evaluate=True)
        print("df_single_run: ", df_single_run)
        df = pd.concat([df, df_single_run])
        df_single_run.to_csv(settings["tensorboard_name"] + '.csv')
        print("Dataframe: ", df)
        df.to_csv(tensorboard_base + '.csv')
    print("total duration: {} minutes".format((time.time() - tstart)/60))
