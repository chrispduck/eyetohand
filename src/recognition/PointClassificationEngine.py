import time
from typing import Dict, Optional, Union

import matplotlib.pyplot as plt
from numpy.core.numeric import False_
import torch
import torch.utils.data
from matplotlib import rc
from torch.optim.optimizer import Optimizer
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm
import pandas as pd

import ConfusionTools as ct
import PRTools as pr
import VisualiseCloud as vc
from Curriculum import Curriculum


class ClassificationEngine():
    """
    ETH classification engine.


        Examples:
    # >>> model = kaolin.models.PointNet.PointNetClassifier()
    # >>> optimizer = torch.optim.Adam(model.parameters())
    # >>> criterion = torch.nn.CrossEntropyLoss()
    # >>> tensorboard = {'name': tensorboard_0001,
    #       'dir': "/home/username/ETH/dataset/runs"
    #       'cats': ['cat', 'dog'],
    #       'nb_cats': 2
    # >>> engineparams = {'epochs': 30}
    # >>> engine = PCEngine.ClassificationEngine(model, train_loader, val_loader, tensorboard,
    #                                    optimizer, args.device, engineparams)

    """

    def __init__(self, model: torch.nn.Module,
                 train_loader: torch.utils.data.DataLoader,
                 val_loader: torch.utils.data.DataLoader,
                 optimizer: Optimizer,
                 tensorboard=None,
                 hyperparams=None,
                 engineparams=None,
                 criterion: Optional[torch.nn.Module] = None,
                 device: Optional[Union[torch.device, str]] = None,
                 curriculum: Curriculum = None
                 ):
        if engineparams is None:
            engineparams = {}
        if hyperparams is None:
            hyperparams = {}
        if tensorboard is None:
            tensorboard = {}

        self.model = model  # Model to be trained.
        self.train_loader = train_loader  # Train dataloader.
        self.val_loader = val_loader  # Validataion dataloader.
        self.hyperparams = self._initialize_hyperparams(
            hyperparams)  # Hyperaparameters (learning rate, momentum, etc.).
        self.engineparams = self._initialize_engineparams(
            engineparams)  # Engine parameters (number of epochs to train for, etc.)
        # Optimizer.
        self.optimizer = optimizer
        # Loss function to use
        self.criterion = torch.nn.CrossEntropyLoss() if criterion is None else criterion
        # Device
        self.device = 'cpu' if device is None else device
        # Cast model to device.
        self.model = self.model.to(self.device)
        # Curriculum for changing tfs during training
        self.curriculum = curriculum
        """
        Engine stats.
        """
        self.current_epoch = 0
        self.train_loss_cur_epoch = 0.
        self.train_accuracy_cur_epoch = 0.
        self.val_loss_cur_epoch = 0.
        self.val_accuracy_cur_epoch = 0.
        # Number of minibatches thus far, in the current epoch.
        self.batch_idx = 0

        self.train_loss = []  # Train loss/accuracy history.
        self.train_accuracy = []
        self.val_loss = []  # Validation loss/accuracy history.
        self.val_accuracy = []

        self.test_loss = 0.
        self.test_accuracy = 0.

        "Tensorboard"
        self.tensorboard_info = tensorboard
        self.writer = SummaryWriter(tensorboard['dir'], flush_secs=1)
        self.nb_classes = tensorboard['nb_cats']
        self.categories = tensorboard['cats']
        self.cm_val_cur_epoch = torch.zeros(self.nb_classes, self.nb_classes)
        self.cm_test = torch.zeros(self.nb_classes, self.nb_classes)

        torch.cuda.empty_cache()  # to prevent RuntimeError: CUDA out of memory

    def _initialize_hyperparams(self, hyperparams: Dict):
        r"""Initializes hyperparameters for the training process. Uses defaults
        wherever user specifications are unavailable.

        Args:
            hyperparams (dict): User-specified hyperparameters ('lr', 'beta1',
                'beta2' for ADAM optimiser).

        """
        paramdict = {}
        paramdict['lr'] = hyperparams['lr'] if 'lr' in hyperparams else 1e-3
        paramdict['beta1'] = hyperparams['beta1'] if 'beta1' in hyperparams else 0.9
        paramdict['beta2'] = hyperparams['beta2'] if 'beta2' in hyperparams else 0.999

        return paramdict

    def _initialize_engineparams(self, engineparams: Dict):
        r"""Initializes engine parameters. Uses defaults wherever user
        specified values are unavilable.

        Args:
            engineparams (dict): User-specified engine parameters ('epochs',
                'validate-every', 'print-every', 'save', 'savedir').
                'epochs': number of epochs to train for.
                'validate-every': number of epochs after which validation
                    occurs.
                'print-every': number of iterations (batches) after which to
                    keep printing progress to stdout.
                'save': whether or not to save trained models.
                'savedir': directory to save trained models to.

        """
        paramdict = {}

        paramdict['epochs'] = engineparams['epochs'] if 'epochs' in engineparams else 10
        paramdict['validate-every'] = engineparams['validate-every'] if 'validate-every' in engineparams else 2
        paramdict['plot-every'] = engineparams['plot-every'] if 'plot-every' in engineparams else 10
        paramdict['print-every'] = engineparams['print-every'] if 'print-every' in engineparams else 10
        paramdict['save'] = engineparams['save'] if 'save' in engineparams else False
        paramdict['savedir'] = engineparams['savedir'] if 'savedir' in engineparams else None
        # Currently, we do not set a default 'savedir'.

        return paramdict

    def _compute_batch(self, batch, mode: str):
        r"""Train/val/test on one mini-batch.
        Args:
            batch (tuple): One mini-batch of test data.
            mode (str) : Batch mode type from ['train', 'val', 'test]
        Returns:
            loss (float): batch loss
            accuracy (float): batch accuracy
        """
        data, label = batch
        # data = data.permute(0, 2, 1)
        pred = self.model(data)
        loss = self.criterion(pred, label.view(-1).long())
        predlabel = torch.argmax(pred, dim=1)
        # print(predlabel.dtype, label.view(-1).dtype)
        accuracy = torch.mean((predlabel == label.view(-1).long()).float())

        if mode is 'train':
            self.train_loss_cur_epoch += loss.item()
            self.train_accuracy_cur_epoch += accuracy.detach().cpu().item()
        elif mode is 'val':
            self.val_loss_cur_epoch += loss.item()
            self.val_accuracy_cur_epoch += accuracy.detach().cpu().item()
            ct.AddToConfusionMat(cmatrix=self.cm_val_cur_epoch,
                                 p_labels=predlabel, gt_labels=label)
        elif mode is 'test':
            self.test_loss += loss.item()
            self.test_accuracy += accuracy.detach().cpu().item()
            ct.AddToConfusionMat(cmatrix=self.cm_test,
                                 p_labels=predlabel, gt_labels=label)

        self.batch_idx += 1

        return loss, accuracy

    def _optimize_batch(self, loss):
        r"""Optimize model parameters for one mini-batch.
        Args:
            loss: training loss computed over the mini-batch.
        """
        loss.backward()
        self.optimizer.step()
        self.optimizer.zero_grad()

    def fit(self):
        r"""Train the model using the specified engine parameters.
        """
        start_time = time.time()
        for epoch in range(self.current_epoch, self.engineparams['epochs']):
            self._print_epoch_number()

            # Train phase
            self.train_loss_cur_epoch = 0.
            self.train_accuracy_cur_epoch = 0.
            self.batch_idx = 0
            self.model.train()

            for idx, batch in enumerate(tqdm(self.train_loader)):
                loss, accuracy = self._compute_batch(batch, mode='train')
                self._optimize_batch(loss)

            # Store training stats
            self.train_loss.append(self.train_loss_cur_epoch)
            self.train_accuracy.append(
                self.train_accuracy_cur_epoch / self.batch_idx)
            self.writer.add_scalar(
                'EpochLoss/train', self.train_loss_cur_epoch / self.batch_idx, epoch)
            self.writer.add_scalar(
                'EpochAccuracy/train', self.train_accuracy_cur_epoch / self.batch_idx, epoch)
            self._print_train_stats()

            # Validation phase
            if self.current_epoch % self.engineparams['validate-every'] ==0:
                self.val_loss_cur_epoch = 0.
                self.val_accuracy_cur_epoch = 0.
                self.batch_idx = 0
                self.cm_val_cur_epoch = torch.zeros(
                    self.nb_classes, self.nb_classes)
                self.model.eval()

                with torch.no_grad():
                    for idx, batch in enumerate(tqdm(self.val_loader)):
                        loss, accuracy = self._compute_batch(batch, mode='val')

                # Store training stats
                self.val_loss.append(self.val_loss_cur_epoch)
                self.val_accuracy.append(
                    self.val_accuracy_cur_epoch / self.batch_idx)
                self._print_validation_stats()
                self.writer.add_scalar(
                    'EpochLoss/val', self.val_loss_cur_epoch / self.batch_idx, epoch)
                self.writer.add_scalar(
                    'EpochAccuracy/val', self.val_accuracy_cur_epoch / self.batch_idx, epoch)
                if self.current_epoch % self.engineparams['plot-every'] ==0:
                    self._process_val_confusion_matrix(epoch)
                    self._process_val_PR_chart(epoch)

            self.current_epoch += 1

            if self.curriculum:
                self.curriculum.increment_epoch()
        
        elapsed_time = (time.time() - start_time) / 60
        self.writer.add_text("Training time (s)", str(elapsed_time))
        print("Training time: {0} minutes".format(elapsed_time))

        with open(self.tensorboard_info['dir'] + '/' + "trained_stats.txt" , 'w') as f:
            # f.write("\n #### {0} ####\n".format())
            f.write("Model: {}\n\n".format(self.model))
            f.write("Training dataset: {}\n\n".format(self.train_loader.dataset.__str__()))
            f.write("Number Epochs: {0}\n".format(self.engineparams['epochs']))
            f.write("Number of classes: {}\n".format(self.nb_classes))
            f.write("Class names: {0}\n\n".format(self.categories))
            f.write("Optimiser: {}\n\n".format(self.optimizer))
            f.write("HyperParams: {}\n\n".format(self.hyperparams))
            f.write("Curriculum: {}\n\n".format(self.curriculum) )
            f.write("Final Val accuracy: {0}\n".format(self.val_accuracy_cur_epoch/self.batch_idx))
            f.write("Final Val loss: {0}\n".format(self.val_loss_cur_epoch/self.batch_idx))
            f.write("Training time: {0} ".format(elapsed_time))
        # Save training data as CSV at the end.
        pd.DataFrame([self.val_accuracy, self.val_loss], ['val_accuracy', 'val_loss']).to_csv()

    def predict(self, test_loader: torch.utils.data.DataLoader, data_name: str):
        r""" Evaluate the model on a test dataset.
        Args:
            test_loader (torch.utils.data.DataLoader): test data loader
            data_name (str): Name of the dataset. Results are saved under this name in tensorboard
        """
        self.test_loss = 0
        self.test_accuracy = 0.
        self.batch_idx = 0
        self.cm_test = torch.zeros(self.nb_classes, self.nb_classes)
        self.model.eval()

        with torch.no_grad():
            for idx, batch in enumerate(tqdm(test_loader)):
                loss, accuracy = self._compute_batch(batch, mode='test')

        final_accuracy = str(self.test_accuracy / self.batch_idx)
        final_loss = str(self.test_loss / self.batch_idx)
        self.writer.add_text('Accuracy/test_' + data_name, final_accuracy)
        self.writer.add_text('Loss/test_' + data_name, final_loss)
        print('Test loss:', final_loss)
        print('Test accuracy:', final_accuracy)
        self._process_test_confusion_matrix(data_name)
        self._process_test_PR_chart(data_name)
        with open(self.tensorboard_info['dir'] + '/' + "predict_" + data_name +'.txt' , 'w') as f:
            f.write("\n #### {0} ####\n".format(data_name))
            f.write("Accuracy: {0}\n".format(final_accuracy))
            f.write("Loss: {0}\n".format(final_loss))
        return float(final_accuracy)

    def _print_epoch_number(self):
        print('\n############################')
        print('Epoch:', self.current_epoch, ', remaining ',
              self.engineparams['epochs'] - self.current_epoch - 1)
        print('############################\n ')

    def _print_train_stats(self):
        r"""Print current stats.
        """
        print('\nTrain loss: {0} \nTrain accuracy: {1}'.format(
            self.train_loss_cur_epoch / self.batch_idx,
            self.train_accuracy_cur_epoch / self.batch_idx))

    def _print_validation_stats(self):
        r"""Print current stats.
        """
        print('\nVal loss: {0} \nVal accuracy: {1}'.format(
            self.val_loss_cur_epoch / self.batch_idx,
            self.val_accuracy_cur_epoch / self.batch_idx))

    def _process_val_confusion_matrix(self, epoch: int):
        r"""Creates a confusion matrix for tensorboard and saving as eps.
        Args:
            epoch (int): Validation epoch number
        """
        normalised_confusion_matrix, per_class_accuracy = ct.NormaliseConfusion(
            self.cm_val_cur_epoch)
        print("Per class accuracy \n", [
              float("{0:0.2f}".format(i)) for i in per_class_accuracy.tolist()])
        v_conf_fig, v_conf_ax = plt.subplots(1, 1)
        fig_path = self.tensorboard_info['dir'] + '/cm_val_ep' + str(epoch)
        ax = ct.PlotConfusion(self.tensorboard_info['cats'], cmatrix=normalised_confusion_matrix, ax=v_conf_ax,
                              saveplot=True,
                              path=fig_path,
                              filetype='eps')
        self.writer.add_figure(
            'ConfusionMatrix/val_epoch' + str(epoch), figure=v_conf_fig)

    def _process_test_confusion_matrix(self, data_name: str):
        r"""Creates a confusion matrix for tensorboard and saving as .eps
                Args:
            data_name (str): test dataset name. Important to note for cross-modal testing
        """
        per_class_accuracy = self.cm_test.diag() / self.cm_test.sum(1)
        print("Per class accuracy \n", [
              float("{0:0.2f}".format(i)) for i in per_class_accuracy.tolist()])
        # Turn into probabilities per GT class
        normalised_confusion_matrix = torch.nn.functional.normalize(
            self.cm_test, p=1, dim=1)
        # rc('font', **{'family': 'DejaVu Sans', 'serif': ['Computer Modern']})
        # rc('font', **{'family': 'DejaVu Sans', 'serif': ['Times']})
        rc('text', usetex=True)
        v_conf_fig, v_conf_ax = plt.subplots(1, 1)
        fig_path = self.tensorboard_info['dir'] + '/cm_test_' + data_name
        ax = ct.PlotConfusion(self.tensorboard_info['cats'], cmatrix=normalised_confusion_matrix, ax=v_conf_ax,
                              saveplot=True,
                              path=fig_path,
                              filetype='eps')
        self.writer.add_figure('ConfusionMatrix/test_' +
                               data_name, figure=v_conf_fig)

    def _process_val_PR_chart(self, epoch: int):
        r"""Creates a Precision recall matrix for tensorboard and saves it as a .eps in the log dir
        Args:
            epoch (int): Validation epoch number
        """
        prec, recall = pr.PRCalculator(cm=self.cm_val_cur_epoch)
        fig_path = self.tensorboard_info['dir'] + '/PR_val_ep' + str(epoch)
        fig, _ = pr.PlotPRBarchart(precision=prec, recall=recall, categories=self.tensorboard_info['cats'],
                                   saveplot=True, path=fig_path, filetype='eps')
        self.writer.add_figure('PR/test', figure=fig)

    def _process_test_PR_chart(self, data_name: str):
        r"""Creates a Precision recall matrix for tensorboard and saves it as a .eps in the log dir
        Args:
            data_name (str): test dataset name. Important to note for cross-modal testing
        """
        prec, recall = pr.PRCalculator(cm=self.cm_test)
        fig_path = self.tensorboard_info['dir'] + '/PR_test_' + data_name
        fig, _ = pr.PlotPRBarchart(precision=prec, recall=recall, categories=self.tensorboard_info['cats'],
                                   saveplot=True, path=fig_path, filetype='eps')
        self.writer.add_figure('PR/test', figure=fig)

    def save_model(self):
        path = self.tensorboard_info['dir'] + '/model.pth'
        torch.save(self.model.state_dict(), path)

    def load_model(self, tensorboard_name: str):
        """
        Loads a tensorboard model earlier created by the engine, into the existing Engine. Models must be compatible.
        Models are saved in the same directory as the tensorboards as model.pth.
        Provide only the tensorboard name.
        Args:
            tensorboard_name (str): The name of the tensorboard created when this model was saved

        Returns:

        """
        path = "/".join([*self.tensorboard_info['dir'].split('/')
                         [:-1], tensorboard_name, 'model.pth'])
        print(path)
        self.model.load_state_dict(torch.load(path))
        self.tensorboard_info['dir'] = "/".join(
            [*self.tensorboard_info['dir'].split('/')[:-1], tensorboard_name])

    def visualise_batch(self, dataloader, show_predictions: bool = False, savefig: bool =False, fname:str =""):
        batch, label = next(iter(dataloader))
        if show_predictions:
            pred = self.model(batch)
            predlabel = torch.argmax(pred, dim=1)
            vc.visualize_batch(
                batch, label, categories=self.categories, pred_labels=predlabel,savefig=savefig, fname=fname)
        else:
            vc.visualize_batch(batch, label, categories=self.categories, savefig=savefig, fname=fname)
