import torch
import CloudTransforms as mytfs, kaolin.transforms as tfs
from torch.utils.data import DataLoader
from CloudDataloading import SingleModelDataset
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from VisualiseCloud import fix_3D_aspect

gen_tactile_root = "../datasets/single/tactile/gen/filtered/"
real_tactile_root = "../datasets/single/tactile/real/filtered/"
vision_root = "../datasets/single/vision/real/filtered/"
device = "cpu"
num_points = 1024
batch_size = 10
cats = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'shampoo', 'soda_can',  'tape']
n_repeats = 1 

tf_test = tfs.Compose([
    mytfs.SamplePoints(scheme='random', nb_points=num_points),
    # mytfs.RandomRotatePointCloud(device=device),
])

# gen Tactile test
gen_tactile_dataset = SingleModelDataset(
    gen_tactile_root, obj_names=cats, transform=tf_test, device=device, n_repeats=1)
gen_tactile_dataloader = DataLoader(
    gen_tactile_dataset, batch_size=batch_size, shuffle=False)

# Real tactile test
real_tactile_dataset = SingleModelDataset(
    real_tactile_root, obj_names=cats, transform=tf_test, device=device, n_repeats=1)
real_tactile_dataloader = DataLoader(real_tactile_dataset, batch_size=batch_size,
                                    shuffle=False)

# Vision data
vision_dataset_test = SingleModelDataset(
    vision_root, cats, transform=tf_test, device=device, n_repeats=1)
vision_dataloader = DataLoader(
    vision_dataset_test, batch_size=batch_size, shuffle=False, drop_last=True)

gt_data, gt_label = next(iter(gen_tactile_dataloader))
rt_data,rt_label = next(iter(real_tactile_dataloader))
v_data, v_label = next(iter(vision_dataloader))

# Plot
for idx, cat in enumerate(cats):
    fig = plt.figure(figsize=(12,4))
    plt.suptitle(cat.title())
    for j, (data, name) in enumerate(zip([gt_data, rt_data, v_data],['Generated Tactile', 'Real Tactile', 'Vision'])):

        ax = fig.add_subplot(1,3,j+1, projection='3d')
        ax.scatter(data[idx, :,0], data[idx, :,1], data[idx, :,2], c=data[idx, :,2], cmap='viridis')
        plt.title(name)
        fix_3D_aspect(ax, gt_data[idx])
    plt.savefig('sampling_images/compare_clouds/'+cat+'.png' )


