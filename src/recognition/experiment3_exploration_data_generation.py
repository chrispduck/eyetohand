import numpy as np
from numpy.core.numeric import full
from numpy.lib.type_check import real
import pcl 
import time

class MLS_Voxel_Filter:
    """ A class to perform moving least squares and a voxel filtering to an input cloud"""

    def __init__(self, MLS_radius: float, leafsize: float):
        """
        Args:
            MLS_radius: Moving least squares k-nn radius
            leafsize: length of the voxel cube
        """
        self.MLS_radis = MLS_radius
        self.leafsize = leafsize

    def __call__(self, points: np.ndarray):
        # assert points is np.ndarray
        cloud = pcl.PointCloud(points.astype('float32'))
        # Moving least Squares and voxel filtering
        MLS_filtered_cloud = self.apply_MLS(cloud)
        MLS_and_voxel_cloud = self.apply_voxel_filter(MLS_filtered_cloud)
        return MLS_and_voxel_cloud.to_array()

    def apply_MLS(self, cloud: pcl.PointCloud) -> pcl.PointCloud:
        MLS = cloud.make_moving_least_squares()  # type: pcl.MovingLeastSquares
        MLS.set_search_radius(self.MLS_radis)
        return MLS.process()

    def apply_voxel_filter(self, cloud: pcl.PointCloud) -> pcl.PointCloud:
        voxel_filter = cloud.make_voxel_grid_filter()  # type pcl.VoxelGridFilter
        voxel_filter.set_leaf_size(self.leafsize, self.leafsize, self.leafsize)
        return voxel_filter.filter()



def create_sample_itr(fname_in, cat, outdir, p1, p2):
    
    filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)
    full_cloud = np.loadtxt(fname_in, delimiter=',')

    # Get previous sample
    if p1 == 0:
        # There is no previous sample  - take first 0.05 points, filter and save
        n = int(np.floor(full_cloud.shape[0]*p2))
        print("0.05, n: ", n)
        fname_out = outdir + '0.05/' + cat + '.csv'
        cloud_in = full_cloud[:n, :]
        cloud_out = filter(cloud_in)
        print("0.05 output shape", cloud_out.shape[0])
        np.savetxt(fname_out, cloud_out, delimiter=',')
    else: 
        # There is a previous sample to load
        fname_previous = outdir + str(p1) + '/' + cat + '.csv'
        previous_cloud = np.loadtxt(fname_previous, delimiter=',')
        # Select the next iteration of points and filter
        start_idx = int(np.round(full_cloud.shape[0] *p1))
        end_idx = int(np.round(full_cloud.shape[0]*p2))
        cloud_in = full_cloud[start_idx:end_idx+1, :]
        t1=time.time()
        filtered_cloud = filter(cloud_in)
        t2= time.time()
        print(t2-t1)
        print("Previous cloud points: ", previous_cloud.shape[0])
        print("new points to filter: ", end_idx-start_idx)
        print("nb new points after filtering", filtered_cloud.shape[0])
        # Concatenate with previous cloud 
        cloud_out = np.concatenate((previous_cloud, filtered_cloud), axis=0)
        print("Nb of points in whole cloud now, ", cloud_out.shape[0])

def create_sample(fname_in, cat, outdir, p):
    # Create MLS voxel filter
    filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)
    # Load whole pointcloud
    full_cloud = np.loadtxt(fname_in, delimiter=',')
    # Select the first n*p points
    end_idx = int(np.round(full_cloud.shape[0]*p))
    cloud_in = full_cloud[:end_idx+1, :]
    # Filter
    t1=time.time()
    cloud_out = filter(cloud_in)
    t2= time.time()    # Save
    print(t2-t1)
    fname_out = outdir + str(p) + '/' + cat + '.csv'
    np.savetxt(fname_out, cloud_out, delimiter=',')

    print("points to filter: ", cloud_in.shape[0])
    print("nb points after filtering", cloud_out.shape[0])


if __name__ == '__main__':
    # make directories
    prange1 = np.arange(start=0.00,stop=1.00,step=0.05).round(decimals=2)
    prange2 = np.arange(start=0.05,stop=1.05,step=0.05).round(decimals=2)

    basedir = "../datasets/single/tactile/real/raw/"
    outdir = "../datasets/raw_tactile_exploration/"
    cats = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'spam', 'shampoo', 'tape']
    # os.mkdir(outdir)
    # [os.mkdir(outdir + str(p)) for p in prange] 
    for cat in cats:
        for p1, p2 in zip(prange1, prange2):
            f_in = basedir + cat + '.csv'
            print(f_in)
            create_sample(fname_in=f_in, outdir=outdir, cat=cat, p=p2)
