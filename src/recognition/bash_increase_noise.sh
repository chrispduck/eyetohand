python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.0" --training-noise 0.0
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.05" --training-noise 0.05
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.10" --training-noise 0.10
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.15" --training-noise 0.15
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.20" --training-noise 0.20
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.25" --training-noise 0.25
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.30" --training-noise 0.30
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.35" --training-noise 0.35
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.40" --training-noise 0.40
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.45" --training-noise 0.45
python ETH_classification_engine_simplified.py --tensorboard-name "PointNet_noise_0.50" --training-noise 0.50
