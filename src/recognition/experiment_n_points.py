import torch, argparse, pandas as pd
from PointNetMedium import PointNet
from kaolin.models import dgcnn
from CloudDataloading import CloudDataset, SingleModelDataset
import kaolin.transforms as tfs
import CloudTransforms as mytfs
from torch.utils.data import DataLoader
from Curriculum import Curriculum
import torch
from PointClassificationEngine import ClassificationEngine as Engine
import os
import matplotlib as mpl

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--vision-root', type=str, default='../datasets/kmeans_v3/vision/filtered/1.0/',
                        help='Root directory of the Vision dataset.')
    parser.add_argument('--tactile-raw-root', type=str, default='../datasets/kmeans_v3/tactile3/raw/1.0/',
                        help='Root directory of the real Tactile dataset.')
    parser.add_argument('---tactile-filt-root', type=str, default='../datasets/kmeans_v3/tactile3/filtered/1.0/')
    parser.add_argument('--cats', type=str, nargs='+',
                        default=
                        # ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'shampoo', 'soda_can',  'tape']
                        ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'shampoo', 'spam',  'tape'],
                        help='list of object classes to use.')
    parser.add_argument('--num-points', type=int, default=1024,
                        help='Number of points to sample from meshes.')
    parser.add_argument('--epochs', type=int, default=75,
                        help='Number of train epochs.')
    parser.add_argument('-lr', '--learning-rate', type=float,
                        default=1e-2, help='Learning rate.')
    parser.add_argument('--batch-size', type=int,
                        default=12, help='Batch size.')
    parser.add_argument('--device', type=str, default='cuda',
                        help='Device to use.')
    parser.add_argument('--tensorboard-name', type=str, default='curric_test1',
                        help='The tensorboard filename')
    parser.add_argument('--training-noise', type=float, default=0.05,
                        help='Standard dev of noise added to all training samples')
    parser.add_argument('--curriculum', type=str, default=None)
    args = parser.parse_args()

    model = PointNet(classes=len(args.cats)).to(args.device)
    tf_train = tfs.Compose([
        mytfs.SamplePoints(scheme='random', nb_points=args.num_points, device=args.device),
        mytfs.RandomRotatePointCloud(device=args.device),
        mytfs.WhiteNoise(sd=args.training_noise, device=args.device),
        mytfs.RandomScalePointCloud(scf=1.03, device=args.device)
    ])
    tf_test = tfs.Compose([
        mytfs.SamplePoints(scheme='random', nb_points=args.num_points, device=args.device),
        mytfs.RandomRotatePointCloud(device=args.device),
    ])

    curriculum = None
    if args.curriculum == "on":
        print("curriculum toggled")
        curriculum_list = [(40, ('nb_points', 512)), (60, ('nb_points', 256))]
        curriculum = Curriculum(tf_train, curriculum_list, mode='normal')

    vision_dataset_train = CloudDataset(args.vision_root, split='train', categories=args.cats, transform=tf_train, device=args.device)
    # vision_dataset_train = SingleModelDataset(
        # args.vision_root, args.cats, transform=tf_train, device=args.device, n_repeats=200)
    
    # vision_dataset_train = CloudDataset(args.vision_root, split='test', categories=args.cats,transform=tf_train,device=args.device)
    # vision_dataset_val = SingleModelDataset(
        # args.vision_root, args.cats, transform=tf_train, device=args.device, n_repeats=84)

    vision_dataset_val = CloudDataset(args.tactile_filt_root, split='test', categories=args.cats, transform=tf_test, device=args.device)
    # vision_dataset_val = SingleModelDataset(
            # args.tactile_root, obj_names=args.cats, transform=tf_test, device=args.device, n_repeats=84)

    train_loader = DataLoader(
        vision_dataset_train, batch_size=args.batch_size, shuffle=True, drop_last=True)
    val_loader = DataLoader(
        vision_dataset_val, batch_size=args.batch_size, shuffle=True, drop_last=True)
    optimiser = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    criterion = torch.nn.CrossEntropyLoss().to(args.device)
    tensorboard_params = {
        'name': args.tensorboard_name,
        'dir': os.getcwd() + "/runs/"+ args.tensorboard_name,
        'cats': args.cats,
        'nb_cats': len(args.cats)
    }
    engineparams = {
        'epochs': args.epochs
    }

    engine = Engine(model=model, train_loader=train_loader, val_loader=val_loader,
                    tensorboard=tensorboard_params,
                    optimizer=optimiser, device=args.device, engineparams=engineparams,
                    curriculum=curriculum)
    try:
        mpl.rcParams['font.family'] = ['DejaVu Sans']
        mpl.rcParams['font.serif'] = ['Times']
        mpl.rcParams['font.size'] = 14

        engine.fit()
        engine.save_model()

        # Real tactile test
        print("Filtered Tactile Test")
        real_tactile_dataset = CloudDataset(args.tactile_filt_root, split='test', categories=args.cats, transform=tf_test,
                    device=args.device)
        # real_tactile_dataset = SingleModelDataset(
            # args.tactile_root, obj_names=args.cats, transform=tf_test, device=args.device, n_repeats=200)
        real_tactile_dataloader = DataLoader(real_tactile_dataset, batch_size=args.batch_size,
                                            shuffle=False, num_workers=0)
        engine.predict(real_tactile_dataloader, 'real_filtered_tactile')

        print("Raw Tactile Test")
        real_raw_tactile_dataset = CloudDataset(args.tactile_raw_root, split='test', categories=args.cats, transform=tf_test,
        device=args.device)
        # real_raw_tactile_dataset = SingleModelDataset(
        #     path=args.tactile_raw_root, obj_names=args.cats, transform=tf_test, device=args.device, n_repeats=200)
        real_raw_tactile_dataloader = DataLoader(real_raw_tactile_dataset, batch_size=args.batch_size)
        engine.predict(real_raw_tactile_dataloader, 'real_raw_tactile' )
        
        print("Vision Test")
        vision_dataset_test = CloudDataset(args.vision_root, split='test', categories=args.cats, transform=tf_test, device=args.device )
        # vision_dataset_test = SingleModelDataset(
        #     args.vision_root, args.cats, transform=tf_test, device=args.device, n_repeats=200)
        vision_test_loader = DataLoader(
            vision_dataset_test, batch_size=args.batch_size, shuffle=True, drop_last=True)
        final_val = engine.predict(vision_test_loader, "vision_filtered")

        ### Test over a range of proportions/ nb unique
        col_names = ["tensorboard_name", "scheme", "scheme-param", "tac_nof", "tac_filt", "vis_filt", 'final_val']
        df = pd.DataFrame(columns=col_names)
        # proportions = torch.linspace(1, 0.2, 1)
        # nb_points = torch.logspace(5,11,1,2) # 64->2048 in 13 in 13 steps.
#
        proportions = torch.linspace(1, 0.2, 17)
        nb_points = torch.logspace(4,11,15,2) # 64->2048 in 13 in 13 steps.
        # nb_points = [512] # 64->2048 in 13 in 13 steps.

        print("NB-POINTS")
        tf_test.transforms[0].scheme = "random"
        # print(tf_test.transforms[0])
        for nb in nb_points:
            tf_test.transforms[0].nb_points = int(nb)
            # engine.visualise_batch(real_tactile_dataloader, savefig=True, fname='tac_filt_'+str(nb_points))
            tac_nof = engine.predict(real_raw_tactile_dataloader,'tac_nof_'+str(int(nb)) )
            tac_filt = engine.predict(real_tactile_dataloader, 'tac_filt_'+str(int(nb)))
            vis_filt = engine.predict(vision_test_loader, 'vis_filt' +str(int(nb)))
            df_itr = pd.DataFrame([[args.tensorboard_name, 'nb_points', int(nb), tac_nof, tac_filt, vis_filt, final_val]], columns=col_names)
            df = pd.concat([df, df_itr])
            df.to_csv('runs/' + args.tensorboard_name + '/results.csv')
    except KeyboardInterrupt:
        print("####### Keyboard interrupt detected #########")
        print("####### Saving board #########")
        engine.save_model()
