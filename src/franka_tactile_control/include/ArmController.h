// Arm controller used for position control or velocity control of the panda robot.
// Control loop method must be launched in a new thread - see cartesian_arm_controller.cpp

#ifndef ArmController_H
#define ArmController_H

#include <franka/exception.h>
#include <franka/robot.h>
#include <franka/model.h>
#include <Eigen/Core>
#include <iostream>
#include <cmath>
#include "ros/ros.h"
#include "Message_Queue.h"
#include "examples_common.h"
#include "PseudoInverse.h"
#include "Arm_Msg.h"
#include "franka_tactile_control/aug_twist.h"
#include "franka_tactile_control/transform64.h"
#include <chrono>

class ArmController {

 public:
  ArmController(std::string robot_ip);
  ~ArmController();

  void initPubSub();
  void publishTransform(const franka::RobotState &);
  void arm_callback(const franka_tactile_control::aug_twist &msg);
  void print_cmd(const franka_tactile_control::aug_twist &msg);

  void controlLoop();
  void moveToInitialPosition();
  Eigen::VectorXd positionControl(const franka::RobotState &);
  Eigen::VectorXd velocityControl(const franka::RobotState &);
  Eigen::VectorXd eeRotation(const franka::RobotState &);

 private:
  franka::Robot robot;
  franka::Model model;
  franka::RobotState initial_state;
  std::array<double, 16> pose;      // Pose transform
  ros::Subscriber arm_cmd_sub;
  ros::Publisher robot_state_pub;
  franka_tactile_control::transform64 robot_info;
  Message_Queue<arm_msg> *Q;
  arm_msg cl_cmd;             // current command

  // Control related
  int publish_period = 1;     // loops per publish of robot_state
  double gamma_l = 1.2;       // linear gain
  double gamma_a = 0.8;       // angular gain
  double gamma_a7 = 0.8;
  double beta_l = 0.003;      // linear integral gain
  double beta_a = 0.01;        // angular integral gain
  double beta_a7 = 0.0000005;
  Eigen::VectorXd integral_error; // 6 element vector of integrated errors.
  float q7_error;             // joint 7 error
  double integral_error_q7;
  bool integrate_bool;    // True if integral gain is on.
  std::chrono::time_point<std::chrono::high_resolution_clock> t0; //start time for integral
  std::chrono::time_point<std::chrono::high_resolution_clock> t1; //finish time
  double mu2 = 0.1;         // damping factor for pinv
};

#endif // !ArmController_H
