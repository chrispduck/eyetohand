#ifndef ARM_MSG_H
#define ARM_MSG_H

#include <Eigen/Dense>

struct arm_msg {
  int cmd_type;
  Eigen::VectorXd goal;
};

#endif