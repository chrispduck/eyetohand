#ifndef MESSAGE_QUEUE_H
#define MESSAGE_QUEUE_H

#include <queue>
#include <Eigen/Dense>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

template <typename> T
class Message_Queue {
 private:
  mutable boost::mutex Q_mutex;
  boost::condition_variable Q_CondVar;
  std::queue<T> m_queue;
 public:
  Message_Queue() {};

  void m_push(T message) {
    boost::lock_guard<boost::mutex> lk(Q_mutex);
    m_queue.push(message);
    Q_CondVar.notify_one();
  }

  void wait_and_pop(T &message) {
    boost::unique_lock<boost::mutex> lk(Q_mutex);
    Q_CondVar.wait(lk, [this] { return !m_queue.empty(); });
    message = m_queue.front();
    m_queue.pop();
  }

  boost::shared_ptr<T> wait_and_pop() {
    boost::unique_lock<boost::mutex> lk(Q_mutex);
    Q_CondVar.wait(lk, [this] { return !m_queue.empty(); });
    boost::shared_ptr<T> res(boost::make_shared<T>(m_queue.front()));
    m_queue.pop();
    return res;
  }

  bool try_pop(T &message) {
    boost::lock_guard<boost::mutex> lk(Q_mutex);
    if (m_queue.empty())
      return false;
    message = m_queue.front();
    m_queue.pop();
    return true;
  }

  boost::shared_ptr<T> try_pop() {
    boost::lock_guard<boost::mutex> lk(Q_mutex);
    if (m_queue.empty())
      return boost::shared_ptr<T>();
    boost::shared_ptr<T> res(boost::make_shared<T>(m_queue.front()));
    m_queue.pop();
    return res;
  }

  bool empty() {
    boost::lock_guard<boost::mutex> lk(Q_mutex);
    return m_queue.empty();
  }

};

#endif
