#ifndef GRIPPERCONTROLLER_H
#define GRIPPERCONTROLLER_H

#include "ros/ros.h"
#include "franka_tactile_control/gripper_cmd.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "franka_skin/franka_skin_contacts.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <thread>
#include <tuple>
#include <boost/bind.hpp>
#include <vector>

#include <franka/exception.h>
#include <franka/gripper.h>
#include <franka/robot.h>
#include <franka/model.h>

class GripperController {
 public:
  GripperController(std::string robot_ip);

  void initPubSub();
  void publishState();
  void gripperCallback(const franka_tactile_control::gripper_cmd &msg);
  void printCmd(const franka_tactile_control::gripper_cmd &msg);
  void openMax();
  double estimateWidth();

 private:
  franka::Gripper gripper;
  franka::GripperState state;
  double current_width;
  ros::Subscriber gripper_cmd_sub;
  ros::Publisher gripper_state_pub;
  franka_tactile_control::gripper_cmd gripper_cmd;
  const float finger_offset = 0.008;

};

#endif // !GRIPPERCONTROLLER_H
