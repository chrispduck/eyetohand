#ifndef THREADGUARD_H
#define THREADGUARD_H

#include <thread>

class ThreadGuard {

 public:
  explicit ThreadGuard(std::thread &t_);
  ~ThreadGuard();
  ThreadGuard(ThreadGuard const &) = delete;
  ThreadGuard &operator=(ThreadGuard const &) = delete;

 private:
  std::thread &t;
};

#endif
