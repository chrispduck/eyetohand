#ifndef PSEUDOINVERSE_H
#define PSEUDOINVERSE_H

#include <Eigen/Core>
#include <Eigen/Dense>

using namespace Eigen;

MatrixXd PseudoInverseDLSSVD(MatrixXd J, MatrixXd W, double mu = 0.1);

#endif