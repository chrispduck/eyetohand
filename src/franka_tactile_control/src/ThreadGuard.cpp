#include "ThreadGuard.h"
#include <thread>

ThreadGuard::ThreadGuard(std::thread &t_) : t(t_) {}

ThreadGuard::~ThreadGuard() {
  if (t.joinable()) {
    t.join();
  }
}