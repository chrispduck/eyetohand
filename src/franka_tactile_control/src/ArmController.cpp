#include "ArmController.h"

ArmController::ArmController(std::string robot_ip) : robot(robot_ip), model(robot.loadModel()) {
  Q = new Message_Queue<arm_msg>;
  initial_state = robot.readOnce();
  integral_error = Eigen::VectorXd(6);
  integral_error << 0, 0, 0, 0, 0, 0;
  integrate_bool = false; // allow one pass of control loop before starting integral gain. (to calculate a dt)
  setDefaultBehavior(robot);
}

ArmController::~ArmController() {
  delete Q;
}

void ArmController::initPubSub() {
  ros::NodeHandle nh;
  arm_cmd_sub = nh.subscribe("/arm_command", 2, &ArmController::arm_callback, this);
  ROS_INFO("Arm Controller - Subscriber initialised successfully");
  robot_state_pub = nh.advertise<franka_tactile_control::transform64>("base_ee_tf", 1000);
  ROS_INFO("Arm Controller - Robot state publisher initialised successfully");

}

void ArmController::publishTransform(const franka::RobotState &robot_state_) {
  robot_info.tf.clear();
  // Publish robot base to ee tf and gripper width to base_ee_tf topic
  for (int i = 0; i < robot_state_.O_T_EE.size(); i++) {
    robot_info.tf.push_back(robot_state_.O_T_EE[i]);
  }
  robot_state_pub.publish(robot_info);
}

void ArmController::arm_callback(const franka_tactile_control::aug_twist &msg) {
  print_cmd(msg); //Log and print the command in roscore
  arm_msg cmd;
  cmd.cmd_type = msg.cmd_type;
  Eigen::VectorXd goal(6); // Assign goal vector locally
  goal << msg.cart.linear.x, msg.cart.linear.y, msg.cart.linear.z,
      msg.cart.angular.x, msg.cart.angular.y, msg.cart.angular.z;
  cmd.goal = goal;
  Q->m_push(cmd); //push to queue
}

void ArmController::print_cmd(const franka_tactile_control::aug_twist &msg) {
  ROS_INFO("Cmd received! Type %d, linear: [%f, %f, %f] angular: [%f, %f, %f]",
           msg.cmd_type, msg.cart.linear.x, msg.cart.linear.y, msg.cart.linear.z,
           msg.cart.angular.x, msg.cart.angular.y, msg.cart.angular.z);
}

void ArmController::controlLoop() {
  ROS_INFO("Arm Controller entered Operational Control Loop");
  double time = 0.0;
  int iteration = 0;
  robot.control([this, &time, &iteration](const franka::RobotState &robot_state, franka::Duration period)
                    -> franka::JointVelocities {
    time += period.toSec();
    // publish robot_state OO_EE
    if (iteration % publish_period == 0) { publishTransform(robot_state); }
    //update current goal
    if (Q->try_pop(cl_cmd)) { ROS_INFO("CMD received"); };
    //Create joint velocity vector
    franka::JointVelocities velocities = {{0, 0, 0, 0, 0, 0, 0}};
    Eigen::VectorXd q_dot(7);
    if (cl_cmd.cmd_type == 0) { q_dot = positionControl(robot_state); }
    else if (cl_cmd.cmd_type == 1) { q_dot = velocityControl(robot_state); }
    else if (cl_cmd.cmd_type == 2) { q_dot = eeRotation(robot_state); }
    else { q_dot << 0, 0, 0, 0, 0, 0, 0; }
    velocities = {q_dot(0), q_dot(1), q_dot(2), q_dot(3), q_dot(4), q_dot(5), q_dot(6)};
    return velocities;
  });
  ROS_INFO("Control loop SHUTDOWN");
}

Eigen::VectorXd ArmController::positionControl(const franka::RobotState &robot_state) {

  // Get jacobian and rotation matrix
  Matrix3d R(3, 3);
  std::array<double, 42> jacobian_array =
      model.zeroJacobian(franka::Frame::kEndEffector, robot_state);
  Eigen::Map<const Eigen::Matrix<double, 6, 7>> jacobian(jacobian_array.data());
  pose = model.pose(franka::Frame::kEndEffector, robot_state);
  R << pose[0], pose[4], pose[8], pose[1], pose[5], pose[9], pose[2], pose[6], pose[10];

  // Find linear and angular errors
  Vector3d e_l(3), e_a(3); //error linear and angular

  //linear
  e_l[0] = pose[12] - cl_cmd.goal[0]; //x
  e_l[1] = pose[13] - cl_cmd.goal[1]; //y
  e_l[2] = pose[14] - cl_cmd.goal[2]; //z

  //angular
  Vector3d EE_align(3), v(3), h(3), w_hat(3);
  double theta, x;
  EE_align << 0, 0, 1; //align z axis
  // std::cout << R.size() << " multiplied with" << EE_align.size() << std::endl;
  h = R * EE_align;
  // std::cout<< "h.size: "<<h.size() <<std::endl;
  v << cl_cmd.goal(3), cl_cmd.goal(4), cl_cmd.goal(5);
  w_hat = h.cross(v) / h.cross(v).norm();
  x = ((h.adjoint() * v) / (h.norm() * v.norm()))(0);
  theta = acos(x);
  // std::cout << "Theta" << theta << std::endl;
  // std::cout << "linear error norm: " << e_l.norm() << std::endl;
  // std::cout << "linear error:" << e_l << std::endl;
  e_a = -theta * w_hat;
  // std::cout << "e_a" << e_a << std::endl;
  // std::cout << "angular error norm: " << e_a.norm() << std::endl;

  // concatenate linear and angular errors:
  VectorXd gamma(6), beta(6), q_dot(7), e(6);
  e << e_l, e_a;

  // set up gains
  gamma << gamma_l, gamma_l, gamma_l, gamma_a, gamma_a, gamma_a;
  beta << beta_l, beta_l, beta_l, beta_a, beta_a, beta_a;

  //computer J*
  MatrixXd Jinv = PseudoInverseDLSSVD(jacobian, MatrixXd::Identity(7, 7), mu2);

  // Compute the integral error
  if (integrate_bool) {
    t1 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = t1 - t0;
    double dt = elapsed.count();  // time in seconds
    integral_error = integral_error + e * dt;
  } else {
    integrate_bool = true;  //allow one pass without integral gain (to get a t0)
  }
  t0 = std::chrono::high_resolution_clock::now();

  // ROS_INFO_STREAM("integral error: \n" << integral_error.norm() << "\n"<< integral_error);

  // ROS_INFO_STREAM("regular error: \n"
  //                 << e.norm() << "\n"
  //                 << e);

  // std::cout << gamma.asDiagonal().size() << "   x   " << Jinv.size() << "  x  " << e.size();

  // calculate joint velocities
  q_dot = -Jinv * (gamma.asDiagonal() * e + beta.asDiagonal() * integral_error);

  // std::cout << q_dot << std::endl;
  return q_dot;
}

Eigen::VectorXd ArmController::velocityControl(const franka::RobotState &robot_state) {
  // Get jacobian
  std::array<double, 42> jacobian_array =
      model.zeroJacobian(franka::Frame::kEndEffector, robot_state);
  Eigen::Map<const Eigen::Matrix<double, 6, 7>> jacobian(jacobian_array.data());
  Eigen::MatrixXd W = Eigen::MatrixXd::Identity(7, 7);
  return PseudoInverseDLSSVD(jacobian, W, mu2) * cl_cmd.goal;
}

Eigen::VectorXd ArmController::eeRotation(const franka::RobotState &robot_state) {
  q7_error = cl_cmd.goal[3] - robot_state.q[6];
  VectorXd q_dot(7);
  // if(ros::Time::now().sec % 10 == 0){
  // std::cout << "q7: " << robot_state.q[6] << std::endl;
  // std::cout << "q7 goal: " << cl_cmd.goal[3] << std::endl;
  // std::cout<< "q7 error: " << q7_error << std::endl;
  // };
  // Compute the integral error
  if (integrate_bool) {
    t1 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = t1 - t0;
    double dt = elapsed.count(); // time in seconds
    integral_error_q7 = integral_error_q7 + q7_error * dt;
  } else {
    integrate_bool = true; //allow one pass without integral gain (to get a t0)
  }
  q_dot << 0, 0, 0, 0, 0, 0, (gamma_a7 * q7_error + beta_a7 * integral_error_q7);
  return q_dot;
}

void ArmController::moveToInitialPosition() {
  std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
  MotionGenerator motion_generator(0.5, q_goal);
  robot.control(motion_generator);
}