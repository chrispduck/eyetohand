import time

import numpy as np
import rospy

from franka_tactile_control.msg import aug_twist


class Arm:
    def __init__(self):
        self.pub = rospy.Publisher('arm_command', aug_twist, queue_size=10)
        self.command = aug_twist()

    def move_to_position(self, x, y, z, vx=0, vy=0, vz=-1):
        """
        Cartesian Position Control to position xyz with alignment with vector v: vx, vy, vz
        Args:
            x: x target
            y: y target
            z: z target
            vx: target vector x component
            vy: target vector y component
            vz: target vector z component
        """
        self.command.cmd_type = 0
        self.command.cart.linear.x = x
        self.command.cart.linear.y = y
        self.command.cart.linear.z = z
        self.command.cart.angular.x = vx
        self.command.cart.angular.y = vy
        self.command.cart.angular.z = vz
        self.check_if_limits_violated()
        print
        "Moving to position ({}, {}, {})".format(x, y, z)
        self.pub.publish(self.command)
        time.sleep(6)
        print
        "Arm move complete"

    def rotate_ee(self, theta=0):
        """
        Rotate the end effector in rad
        Args:
            theta:

        """
        print
        "Rotating EE to {}".format(theta)
        self.command.cmd_type = 2
        self.command.cart.angular.x = theta + np.pi / 4  # filling angular X to send to gripper controller
        self.pub.publish(self.command)
        time.sleep(6)
        print
        "Arm rotation complete"

    def check_if_limits_violated(self):
        """
        Ensures the robot does not attempt to leave the workspace.
        """
        cmd = self.command.cart
        linear_cmd = [cmd.linear.x, cmd.linear.y, cmd.linear.z]
        linear_lower_limit = [-0.5, -0.5, 0.1]
        linear_upper_limit = [0.5, 0.5, 0.55]
        if not (linear_lower_limit <= linear_cmd <= linear_upper_limit):
            raise Exception("Command is not within the workspace : {}".format(linear_cmd))
        if cmd.angular.z > 0:
            raise Exception("vz must be <=0")
