import time
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
import rospy
from franka_skin.msg import franka_skin_contacts
from jg_experiments.taxel_light_up import active_finger_map
from std_msgs.msg import Float64


class Skin:
    """
    A class to record taxel data, and view the incoming data in real-time.
    """

    def __init__(self):
        """Set up Rospy subscribers to the gripper and taxel data. Create blank matrices for """
        # Note no need for rospy.spin as python ros subscribers have their own threads.
        self.sub = rospy.Subscriber('robot/skin/contact', franka_skin_contacts, self.skin_callback)
        self.sub = rospy.Subscriber('gripper_state', Float64, self.gripper_state_callback)
        # Plotting
        self.start_plotting()
        self.plot_number = 0
        self.start_plotting_map()
        self.start_time = time.time()
        # Data storage
        self.taxel_data = np.empty((0, 10))  # All taxel data
        self.taxel_dict = defaultdict(lambda: np.empty((0, 10)))  # All taxel data in dict
        self.clean_taxel_data = np.empty((0, 10))  # clean_taxel_data
        self.clean_taxel_dict = defaultdict(lambda: np.empty((0, 10)))  # All taxel data in dict
        self.record_clean_data = False
        # Bools and other to assist with grasping and estimating widths
        self.current_gripper_width = 0
        self.width_estimate = None
        self.true_width = None
        self.last_contact_ids = []  # A list of the tactile IDs from the latest non-zero message
        self.last_contacts = []  # A list of all tactile data from the latest non-zero message
        self.last_contacts = []  # A list of all tactile data from the latest non-zero message
        self.command_contacts = []  # A list of all tactile data from the current command (eg. estimate width)
        self.current_contact_ids = np.empty(
            0)  # A list of all tactile IDs from the current message (empty if no contact)

        # self.calibration_dict = defaultdict(lambda: np.empty(4))

    def skin_callback(self, msg):
        """ Record incoming taxel data and associate it with a time.
            IF certain that data is clean, then put into clean data structures"""
        # type: franka_skin_contacts -> None
        if msg.contacts:  # If there are any non-zero contact tazels
            run_time = time.time() - self.start_time
            new_taxel_data = np.array(
                [[t.UID, run_time, t.mean_response, self.current_gripper_width, t.xc_origin[0], t.xc_origin[1],
                  t.xc_origin[2], t.nc_origin[0], t.xc_origin[1], t.nc_origin[2]] for t in
                 msg.contacts])
            # Store both in a dictionary by taxel ID, and in a long numpy array.
            if self.taxel_dict[t.UID].size == 0:  # Add new data to dictionary
                self.taxel_dict[t.UID] = new_taxel_data[1:]  # UID is already stored as the key
            else:
                self.taxel_dict[t.UID] = np.concatenate((self.taxel_dict[t.UID], new_taxel_data), axis=0)
            self.taxel_data = np.concatenate((self.taxel_data, new_taxel_data), axis=0)  # Add taxel

            # If confident in getting clean data: put into the clean data structures
            if self.record_clean_data:
                # Store clean data seperate from the rest of the data
                if self.clean_taxel_dict[t.UID].size == 0:
                    self.clean_taxel_dict[t.UID] = new_taxel_data[1:]
                else:
                    self.clean_taxel_dict[t.UID] = np.concatenate((self.clean_taxel_dict[t.UID], new_taxel_data),
                                                                  axis=0)
                    self.clean_taxel_data = np.concatenate((self.clean_taxel_data, new_taxel_data), axis=0)  # Add taxel

            ## Temporally recent data (used for .estimate_width()) - last no longer needed but kept if wanted later.
            self.last_contact_ids = np.unique(new_taxel_data[:, 0])  # provide a list of UIDs of all taxels in contact
            self.last_contacts = msg.contacts  # All taxel data from last non-empty message
            self.command_contacts.extend(msg.contacts)  # Add data from this command
            self.current_contact_ids = np.unique(new_taxel_data[:, 0])
        else:
            self.current_contact_ids = np.empty(0)

    def gripper_state_callback(self, msg):
        self.current_gripper_width = float(msg.data)

    def numpy_save(self, folder, filename):
        """
        Save the tactile data as a CSV
        Args:
            folder: Path to folder
            filename: Filename with extension

        Returns:

        """
        print
        "clean data: " + str(len(self.clean_taxel_data))
        print
        "all data: " + str(len(self.taxel_data))
        clean_data_filename = folder + filename
        all_data_filename = folder + "all_" + filename
        np.savetxt(clean_data_filename, self.clean_taxel_data, delimiter=",")
        np.savetxt(all_data_filename, self.taxel_data, delimiter=",")
        print
        "Saved files: {}, {} ".format(clean_data_filename, all_data_filename)

    def save_txt(self, filename=r"/home/perla/Desktop/4yp/Matlab/Calibration_data/CalibrationHuman_2"):
        with open(filename, "w") as outfile:
            np.savetxt(outfile, self.clean_taxel_data, fmt="%.5f", delimiter=" ", newline="\n")

    def append_txt(self, filename=r"/home/perla/Desktop/4yp/Matlab/Calibration_data/Calibration", force=0):
        # taxel_data_with_force = np.append(self.taxel_data, force, axis=0)
        with open(filename, "a") as outfile:
            np.savetxt(outfile, self.clean_taxel_data, fmt="%.3f", delimiter=" ", newline="\n")
            outfile.write(b"0 0 0 0 0 0 0 0 0\n")  # add new line

    def clear_txt(self, filename=r"/home/perla/Desktop/4yp/Matlab/Calibration_data/Calibration"):
        # taxel_data_with_force = np.append(self.taxel_data, force, axis=0)
        open(filename, 'w').close()

    def is_double_contact(self):
        """  See if both fingers have texels in contact. """
        print
        "is_double_contacts sees {} items in command contacts".format(len(self.command_contacts))
        parent_frames = [contact.parent_id for contact in self.command_contacts]
        print
        parent_frames
        print
        0 in parent_frames and 1 in parent_frames
        return True if 0 in parent_frames and 1 in parent_frames else False

    def estimate_initial_width(self, the_gripper, finish_wide=True):
        """Performs sequential gripper.moves until taxels from both fingers are in contact"""
        self.command_contacts = []  # Reset command_contacts - Only interested in new contact data from this grasp
        the_gripper.open_wide()
        while not self.is_double_contact():
            print
            "Estimating width.... "
            print
            "double contact evaluates as: {}".format(self.is_double_contact())
            the_gripper.move(width=the_gripper.get_current_width() - 0.003, speed=0.2)
        self.width_estimate = the_gripper.get_current_width()
        print
        "Estimated width: ", self.width_estimate
        if finish_wide:
            the_gripper.open_wide()

    def record_clean_on(self):
        """ Records taxel data into a another data structure """
        print
        "Recording data as clean"
        self.record_clean_data = True  # Start storing clean data seperately.

    def record_clean_off(self):
        """ Revert to recording taxel data into the normal data structure """
        print
        "recording data as noisy"
        self.record_clean_data = False

    def clear_recording(self):
        """ Clear both clean data structures"""
        self.clean_taxel_data = np.empty((0, 9))
        self.clean_taxel_dict = defaultdict(lambda: np.empty((0, 10)))

    def find_true_initial_width(self, the_gripper, finish_wide=True):  # constant offset due to fingers diff
        """
        Similar to estimate initial width but increments more slowly when near the object, and at smaller intervals
        """
        t = 1.2
        the_gripper.move(self.width_estimate * t, 0.02)
        time.sleep(2)  # gives time for self.contacts to be updated
        self.command_contacts = []  # Reset command_contacts - Only interested in new contact data from this grasp
        while not self.is_double_contact():
            print
            "Finding true width.... "
            print
            "double contact evaluates as: {}".format(self.is_double_contact())
            the_gripper.move(the_gripper.get_current_width() - 0.0005, 0.01)
            time.sleep(0.05)
        self.true_width = the_gripper.get_current_width()
        print
        "True width: ", self.true_width
        if finish_wide:
            the_gripper.open_wide()

    def start_plotting(self):
        """
        Starts plotting the 3D live point cloud plot
        """
        plt.ion()
        self.fig = plt.figure()
        self.fig_map = plt.figure()
        self.ax = self.fig.add_subplot(111, projection="3d")

    def start_plotting_map(self):
        """
        Starts ployting a graph with taxels from each finger. When a taxel is in contact, it lights up
        """
        self.skin_finger_map = active_finger_map()
        self.ax_finger_map = self.fig_map.add_subplot(111)
        self.skin_finger_map.setup_axes(self.ax_finger_map)
        self.skin_finger_map.plot_active_finger_map(self.ax_finger_map)
        self.skin_finger_map.read_uids_into_dict()

    def update_plot(self):
        """ Update the point cloud plot with new taxel data"""
        self.ax.clear()
        self.ax.scatter(self.clean_taxel_data[:, 3], self.clean_taxel_data[:, 4], self.clean_taxel_data[:, 5],
                        color='b', s=5)

        self.ax.set_xlim(0.20, 0.45)
        self.ax.set_ylim(-0.125, 0.125)
        self.ax.set_zlim(-0.05, 0.20)
        self.ax.set_xlabel("x")
        self.ax.set_ylabel("y")
        self.ax.set_zlabel("z")
        title_text = "Number of points: " + str(len(self.taxel_data[:, 3])) + "    Plot iteration: " + str(
            self.plot_number)
        self.ax.set_title(title_text)
        self.ax.title.set_position([.5, 1.05])
        self.plot_number += 1

        plt.draw()
        plt.pause(0.0001)

    def update_map_plot(self):
        """ Update the plot showing which taxels are in contact"""
        self.ax_finger_map.clear()
        self.skin_finger_map.plot_active_finger_map(self.ax_finger_map)
        if self.current_contact_ids.size:
            for uids_in_contact in self.last_contact_ids:  # last/current contacts (memory/no memory)
                self.skin_finger_map.fill_circle(self.skin_finger_map.circle_from_uid(uids_in_contact),
                                                 self.ax_finger_map, 'r')

        plt.draw()
        plt.pause(0.0001)

    def static_plot(self):
        """ A static plot of the point clouds - Unused"""
        plt.show()
        fig = plt.figure()
        ax = plt.axes(projection="3d")
        ax.scatter3D(self.taxel_data[:, 2], self.taxel_data[:, 3], self.taxel_data[:, 4], cmap='hsv')

    # def load_force_calibration(self,
    #                            path=r"/home/perla/Desktop/4yp/Matlab/Calibration_data/calibration_coefficients_test.txt"):
    #     with open(path) as coefficients:
    #         for line in coefficients:
    #             self.calibration_dict[int(line.split()[0])] = [float(i) for i in line.split()[1:]]
    #
    # def get_force_from_response(self, id, response):
    #     x = self.calibration_dict[id]
    #     return -(1 / x[1]) * np.log(1 - (response - x[2]) / x[0])
    #
    # def get_response_from_force(self, id, force):
    #     x = self.calibration_dict[id]
    #     return x[0] * (1 - np.exp(-x[1] * force)) + x[2]
