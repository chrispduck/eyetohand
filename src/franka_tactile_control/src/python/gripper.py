import time

import rospy
from std_msgs.msg import Float64

from franka_tactile_control.msg import gripper_cmd


class Gripper:
    """
    Gripper class for interacting with the gripper with the avaliable: grasp, homing, and
    """

    def __init__(self):
        self.pub = rospy.Publisher('gripper_command', gripper_cmd, queue_size=10)
        self.command = gripper_cmd()
        self.sub = rospy.Subscriber('gripper_state', Float64, self.gripper_callback)
        self.current_width = Float64(0)

    def grasp(self, width, speed=0.03, force=1, eps_inner=0.0005, eps_outer=0.0005):
        """
        Grasp an object at a known width, reaching the width with given speed, and holding an object with a given force.
        Epsillon margin for error in the width.

        Args:
            width: Width in meters (max 0.088)
            speed: Speed in m/s
            force:  force in N
            eps_inner: maximum underestimation in width
            eps_outer: maximium overestimation in width

        Returns:

        """
        print
        "Performing grasp to width {}, speed: {}, force: {}".format(width, speed, force)
        self.command.width = width
        self.command.speed = speed
        self.command.force = force
        self.command.epsilon_inner = eps_inner
        self.command.epsilon_outer = eps_outer
        self.command.cmd_type = 2
        self.pub.publish(self.command)
        time.sleep(5)

    def move(self, width, speed=0.03):
        """
        Move at a given speed to a width
        Args:
            width: width in metres
            speed: speed in m/s

        Returns:

        """
        print
        "Opening gripper to width {}, speed: {}".format(width, speed)
        self.command.width = width
        self.command.speed = speed
        self.command.cmd_type = 1
        self.pub.publish(self.command)
        time.sleep(2)

    def homing(self):
        """
        Opens the gripper to full width, closes and opens again.
        Returns:
            None
        """
        print
        "Performing Homing"
        self.command.cmd_type = 0
        self.pub.publish(self.command)
        time.sleep(3)
        return 0

    def gripper_callback(self, msg):
        # ROS Callback to save the current width of the gripper
        self.current_width = float(msg.data)

    def get_current_width(self):
        return float(self.current_width)

    def open_wide(self):
        """
        Open to maximum width by using move command with speed 0.04m/s
        """
        print
        "Opening gripper wide"
        self.move(0.088, 0.04)
        time.sleep(3)

    def open_max(self):
        """
        Execute an open max (type 3 command)
        """
        print
        "Opening gripper max"
        self.command.cmd_type = 3
        self.pub.publish(self.command)
        time.sleep(3)
