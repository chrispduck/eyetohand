import numpy as np

from arm import Arm
from gripper import Gripper
from skin import Skin


class TestControl:
    """ A class to test the control of the robot. Use these instructions in exploration_main"""

    def __init__(self, arm, gripper, skin):
        # type: (Arm, Gripper, Skin) -> None
        self.arm = arm
        self.gripper = gripper
        self.skin = skin
        self.x_home = 0.33
        self.y_home = 0
        self.z_home = 0.1
        self.delta_x = 0.04
        self.delta_y = 0.10
        self.z_upper = 0.2
        self.step = 0.02

    def instructions(self):
        self._test_vertical()
        self._test_alignment()
        self._test_rotation()
        self._test_exploration()
        self._practice_save()

    def _test_vertical(self):
        # Test vertical
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper + 0.25)  # MOVE self.arm TO INITIAL POSITION
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper + 0.25)  # MOVE self.arm TO INITIAL POSITION
        # self.arm.move_to_position(self.x_home, self.y_home, self.z_upper + 0.20)    # MOVE ARM TO INITIAL POSITION
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper + 0.05)  # MOVE ARM TO INITIAL POSITION
        self.arm.move_to_position(self.x_home, self.y_home, self.z_home)
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper + 0.05)  # MOVE ARM TO INITIAL POSITION

    def _test_alignment(self):
        # Test orientation alignment
        self.arm.move_to_position(self.x_home + 0.1, self.y_home, self.z_upper + 0.05, vx=0, vy=0,
                                  vz=-1)  # MOVE ARM TO INITIAL POSITION
        self.arm.move_to_position(self.x_home + 0.1, self.y_home, self.z_upper + 0.05, vx=0.7, vy=0,
                                  vz=-1)  # MOVE ARM TO INITIAL POSITION
        self.arm.move_to_position(self.x_home + 0.1, self.y_home, self.z_upper + 0.05, vx=0, vy=0,
                                  vz=-1)  # MOVE ARM TO INITIAL POSITION
        self.arm.move_to_position(self.x_home + 0.1, self.y_home, self.z_upper + 0.05, vx=0, vy=-0.7,
                                  vz=-1)  # MOVE ARM TO INITIAL POSITION
        self.arm.move_to_position(self.x_home + 0.1, self.y_home, self.z_upper + 0.05, vx=0, vy=0,
                                  vz=-1)  # MOVE ARM TO INITIAL POSITION
        self.arm.move_to_position(self.x_home + 0.1, self.y_home, self.z_upper + 0.05, vx=0, vy=0,
                                  vz=-1)  # MOVE ARM TO INITIAL POSITION

    def _test_rotation(self):
        # Test rotation
        self.arm.rotate_ee(np.pi / 2)
        self.arm.rotate_ee(0)
        self.arm.rotate_ee(-np.pi / 2)
        self.arm.rotate_ee(0)

    def _test_exploration(self):
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper + 0.05)  # MOVE ARM TO INITIAL POSITION
        self.arm.move_to_position(self.x_home, self.y_home, self.z_home)  # LOWER ARM TO INITIAL POSITION

        self.skin.record_clean_on()  # START STORING DATA

        # const x move in y, then const y move in x:
        x = [self.x_home + i for i in np.arange(- self.delta_x / 2, self.delta_x / 2, self.step)]
        for xi in x:
            self.gripper.open_wide()
            self.arm.move_to_position(xi, self.y_home, self.z_home)  # MOVE GRIPPER TO CHOSEN POSITION
            self.gripper.homing()
            self.skin.estimate_initial_width(self.gripper)
            self.gripper.open_wide()
            self.gripper.open_wide()

        # Move up, rotate, and back down
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper)  # MOVE GRIPPER TO CHOSEN POSITION
        self.arm.rotate_ee(np.pi / 2)
        self.arm.move_to_position(self.x_home, self.y_home, self.z_home)

        y = [self.y_home + i for i in
             np.arange(self.y_home - self.delta_y / 2, self.y_home + self.delta_y / 2, self.step)]
        for yi in y:
            self.arm.move_to_position(self.x_home, yi, self.z_home)  # MOVE GRIPPER TO CHOSEN POSITION
            self.gripper.homing()

    def _practice_save(self):
        file_path = "tactile_objects/" + str(raw_input("Please enter a filename (no extension)")) + ".csv"
        self.skin.numpy_save(file_path)
        print
        "file saved as:  " + file_path
