import os
import time

import numpy as np
from arm import Arm
from gripper import Gripper
from skin import Skin


class ExplorationStrategies:
    """ A base class for creating exploration strategies for diffrent objects """

    def __init__(self, _skin, _gripper, _arm):
        # type: (Skin, Gripper, Arm) -> None
        self.skin = _skin  # type: Skin
        self.gripper = _gripper  # type: Gripper
        self.arm = _arm  # type: Arm
        # `home' values are centre of the workspace, marked with a cross
        self.x_home = 0.325
        self.y_home = -0.006
        self.z_home = 0.104
        self.z_start = 0.45
        self.z_upper = 0.32

    def _kick_start_arm(self):
        """ Used to check that commands are flowing to the franka arm successfully"""
        self._homing()
        time.sleep(2)
        self.arm.move_to_position(self.x_home, self.y_home, self.z_start, vx=0, vy=0,
                                  vz=-1)  # MOVE ARM TO INITIAL POSITION
        self.arm.move_to_position(self.x_home, self.y_home, self.z_start, vx=0, vy=0,
                                  vz=-1)  # MOVE ARM TO INITIAL POSITION
        self.arm.rotate_ee(0)

    def _explore_x_dirn(self, x, y, z, delta_x, step, f):
        x_range = [x + i for i in np.arange(- delta_x / 2, delta_x / 2, step)]
        for xi in x_range:
            self.arm.move_to_position(xi, y, z)
            f()
            self.skin.record_clean_on()  # See if this removes artifacts from first homing

    def _explore_y_dirn(self, x, y, z, delta_y, step, f):
        y_range = [y + i for i in np.arange(- delta_y / 2, delta_y / 2, step)]
        for yi in y_range:
            self.arm.move_to_position(x, yi, z)
            f()

    def _explore_z_dirn(self, x, y, z, delta_z, step, f):
        z_range = [z + i for i in np.arange(0, delta_z, step)]
        for zi in z_range:
            self.arm.move_to_position(x, y, zi)
            f()

    def _estimate_acc_width(self):
        """Estimate initial width, then perform a more accurate estimate whilst recording as clean data """
        self.skin.record_clean_off()
        self.skin.estimate_initial_width(self.gripper, finish_wide=True)
        self.skin.record_clean_on()
        self.skin.find_true_initial_width(self.gripper, finish_wide=True)
        self.skin.record_clean_off()

    def _homing(self):
        self.gripper.homing()

    def _double_homing(self):
        self.gripper.homing()
        time.sleep(4)
        self.gripper.homing()

    def _prompt_save(self):
        """ Save tactile data in the tactile_objects folder in the cwd """
        os.getcwd()
        folder = "tactile_objects/"
        file_name = str(raw_input("Please enter a filename (no extension)")) + ".csv"
        self.skin.numpy_save(folder, file_name)

    def explore_cube(self):
        """ Definintion of the exploration strategy for a cube"""
        self._kick_start_arm()
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper)
        self.arm.move_to_position(self.x_home, self.y_home, self.z_home)
        # self.skin.record_clean_on()
        self._explore_x_dirn(self.x_home, self.y_home, self.z_home, delta_x=0.06, step=0.0035, f=self._homing)
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper)
        self.arm.rotate_ee(np.pi / 2)
        self._explore_y_dirn(self.x_home, self.y_home, self.z_home, delta_y=0.06, step=0.0035, f=self._homing)
        self._prompt_save()

    def explore_pepsi(self):
        """ Definintion of the exploration strategy for a soda can"""
        h = 0.12
        h_finger = 0.10
        self._kick_start_arm()
        self.arm.move_to_position(self.x_home, self.y_home, self.z_home + h - h_finger)
        self.skin.record_clean_on()

        for theta in np.arange(0, -np.pi, np.pi / 6):
            self.arm.rotate_ee(theta)
            self._estimate_acc_width()
            time.sleep(3)
        self._prompt_save()
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper)
        c = 0.7
        b = np.sqrt((0.7 ** 2) / 2)
        orientations = [[c, 0, -1, 0], [b, b, -1, np.pi / 4], [0, c, -1, np.pi / 2], [-b, -b, -1, -np.pi / 4],
                        [0, -c, -1, -np.pi / 2]]  # x,y,z,theta

        for o in orientations:
            self.arm.rotate_ee(o[3])
            self.arm.move_to_position(self.x_home, self.y_home, self.z_upper, vx=0, vy=0, vz=-1)
            self.arm.move_to_position(self.x_home, self.y_home, self.z_upper, vx=o[0], vy=o[1], vz=o[2])
            self.arm.move_to_position(self.x_home, self.y_home, self.z_home, vx=o[0], vy=o[1], vz=o[2])
            self._explore_z_dirn(self.x_home, self.y_home, self.z_home, delta_z=h, step=0.004,
                                 f=self._estimate_acc_width)
            self.arm.move_to_position(self.x_home, self.y_home, self.z_upper, vx=o[0], vy=o[1], vz=o[2])
            self.arm.move_to_position(self.x_home, self.y_home, self.z_home, vx=0, vy=0, vz=-1)

        self._prompt_save()

    def control_demonstration(self):
        """ Demonstrate the control of the robot"""
        self.skin.record_clean_on()
        self._kick_start_arm()
        self.gripper.homing()
        self.gripper.move(0.08, 0.01)
        self.gripper.open_wide()
        self.gripper.move(0.0607454357669, 0.01)
        time.sleep(15)
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper)
        self.arm.move_to_position(self.x_home, self.y_home, self.z_upper + 0.20)
        self.arm.move_to_position(self.x_home, self.y_home, self.z_home + 0.05)
        self._prompt_save()

    def loop_any_function(self, f):
        while True:
            f(self)

    def one_command_and_wait(self):
        self._kick_start_arm()
        while True:
            time.sleep(2)
