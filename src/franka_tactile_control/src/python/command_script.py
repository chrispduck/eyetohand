import threading
import time

import rospy
from arm import Arm
from exploration_strategies import ExplorationStrategies
from gripper import Gripper
from skin import Skin

if __name__ == '__main__':
    # ROSpy try except
    try:
        # Initialise node and publishers
        skin = Skin()
        arm = Arm()
        gripper = Gripper()
        # Initislise command script node in ros
        rospy.init_node('command_script', anonymous=False)
        # Define the instructions for the robot
        E = ExplorationStrategies(skin, gripper, arm)
        instructions = E.explore_pepsi

        # Launch the command script and live plotting
        # Keyboard Try-Except
        try:
            instructions = threading.Thread(target=instructions)
            instructions.start()
            while not rospy.is_shutdown():
                # Update plots at 5Hz
                skin.update_plot()
                skin.update_map_plot()
                time.sleep(0.2)
            instructions.join()
        except KeyboardInterrupt:
            instructions.join()
            pass

        print
        "command_script.py exiting"

        skin.static_plot()

    except rospy.ROSInterruptException as e:
        rospy.loginfo(e)
        pass
