// Working Cartestian Controller for velocity.
// Refactored so that the ROS subscriber is handled within the class
// Also publishes robot state as uses ArmController5.
// Changed initsubsciber method to intPubSub

#include <thread>
#include "ros/ros.h"
#include "ThreadGuard.h"
#include "ArmController.h"

int main(int argc, char **argv) {
  try {
    ros::init(argc, argv, "arm_node");
    ArmController arm_controller("172.16.0.2");
    arm_controller.initPubSub();
    arm_controller.moveToInitialPosition();
    std::thread control_thread(&ArmController::controlLoop, &arm_controller);
    ThreadGuard tg(control_thread);
    ROS_INFO("Reached Arm ros spin");
    ros::spin();
  }
  catch (const franka::Exception &e) {
    ROS_ERROR("%s", e.what());
    return -1;
  }
  return 0;
}