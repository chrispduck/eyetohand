#include "GripperController.h"
#include <typeinfo>

// NOTE
// Our fingers make the gap between pincers 8mm wider than default.
// This is accounted for by adding finger_offset to readings and
// subtracting from commands.

int main(int argc, char **argv) {
  try {
    ros::init(argc, argv, "gripper_node");
    GripperController gripper_controller("172.16.0.2");
    gripper_controller.initPubSub();
    ROS_INFO("Gripper node initialisation complete");

    ros::Rate r(10);
    while (ros::ok()) {
      gripper_controller.publishState();
      ros::spinOnce();
      r.sleep();
    }
  }

  catch (const franka::Exception &e) {
    ROS_ERROR("%s", e.what());
    return -1;
  }
  return 0;
}

GripperController::GripperController(std::string robot_ip = "172.16.0.2") : gripper(robot_ip) {
  ros::Time::init();
  ROS_INFO("Gripper homing commencing");
  ros::Duration(2).sleep(); // Wait for 2s
  gripper.homing();
  state = gripper.readOnce();
}

void GripperController::initPubSub() {
  ros::NodeHandle nh;
  gripper_cmd_sub = nh.subscribe("/gripper_command", 1, &GripperController::gripperCallback, this);
  ROS_INFO("Gripper Controller - Gripper command subscriber initialised successfully");
  gripper_state_pub = nh.advertise<std_msgs::Float64>("gripper_state", 1000);
  ROS_INFO("Gripper Controller - Gripper state publisher initialised successfully");
}

void GripperController::publishState() {
  state = gripper.readOnce();
  gripper_state_pub.publish(state.width + finger_offset);
}

void GripperController::gripperCallback(const franka_tactile_control::gripper_cmd &msg) {
  printCmd(msg); // Log and print the command in roscore
  std::string cmd_type_str;
  if (msg.cmd_type == 0) {
    gripper.homing();
  } else if (msg.cmd_type == 1) {
    gripper.move(msg.width - finger_offset, msg.speed);
  } else if (msg.cmd_type == 2) {
    gripper.grasp(msg.width - finger_offset, msg.speed, msg.force, msg.epsilon_inner, msg.epsilon_outer);
  } else if (msg.cmd_type == 3) {
    openMax();
  } else { ROS_INFO("Invalid comman type. Command type must be 0 (Homing), 1 (Move), 2 (Grasp) or 3 (Open max)."); }
  ROS_INFO("Finished gripper command\n");
  publishState();
}

void GripperController::printCmd(const franka_tactile_control::gripper_cmd &msg) {
  std::string cmd_type_str;
  if (msg.cmd_type == 0) {
    cmd_type_str = "Homing";
  } else if (msg.cmd_type == 1) {
    cmd_type_str = "Move";
  } else if (msg.cmd_type == 2) {
    cmd_type_str = "Grasp";
  } else if (msg.cmd_type == 3) {
    cmd_type_str = "Open Max";
  }

  ROS_INFO_STREAM("Gripper command received! \n Type: " << cmd_type_str << " | Width: " << msg.width << " | Speed: "
                                                        << msg.speed << " | Force: " << msg.force
                                                        << " | Epsilon Inner: " << msg.epsilon_inner
                                                        << " | Epsilon Outer: " << msg.epsilon_outer);
}

void GripperController::openMax() {
  gripper.move(state.max_width - finger_offset, 0.01);
}


