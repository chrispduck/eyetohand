#include <Eigen/Core>
#include <Eigen/Dense>
#include "pseudoInverse.h"

Eigen::MatrixXd PseudoInverseDLSSVD(MatrixXd J, MatrixXd W, double mu) {
  MatrixXd W_1_2 = MatrixXd::Zero(W.cols(), W.cols());

  for (int i = 0; i < W.cols(); i++) {
    W_1_2(i, i) = 1 / sqrt(W(i, i));
  }

  J = J * W_1_2;

  JacobiSVD<MatrixXd> svd(J, ComputeFullU | ComputeFullV);
  MatrixXd SigmaPlus = Eigen::MatrixXd::Zero(J.cols(), J.rows());
  VectorXd eigen = svd.singularValues();

  int r = eigen.size();

  for (int i = 0; i < r; i++) {
    SigmaPlus(i, i) = eigen(i) / (eigen(i) * eigen(i) + mu * mu);
    //SigmaPlus(i,i) = 1/eigen(i);
  }

  MatrixXd V = svd.matrixV();
  MatrixXd U = svd.matrixU();

  return W_1_2 * V * SigmaPlus * U.transpose();

}