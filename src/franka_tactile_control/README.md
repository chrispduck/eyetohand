# Franka Tactile Control - Code snippets
This repository contains code snippets of the control system implemented for the panda by Franka Emika, an industrial  7 DOF manipulator.
### Launching the code :

* `roslaunch franka_tactile_control robot_control.launch`
* `rosrun franka_skin  franka_contact_publisher_no_tf src/franka_skin/calibration_files/franka_fingers-0001.json` (not included)
* `python2.7 src/python/command_script.py`


### **Architecture RQT Graph**:
![](images/rqt_graph.png)

#### Not included in repo:
* `cyskin_acquisition` node from `cyskin_acquisition` package
* `/skin_contact_publisher` node from `franka_skin` package

