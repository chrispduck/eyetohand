import numpy as np
import torch


def RotateX(dx: float):
    """    Rotate about X axis in Degrees"""
    rx = DegToRad(dx)
    return torch.tensor([[1, 0, 0],
                         [0, np.cos(rx), -np.sin(rx)],
                         [0, np.sin(rx), np.cos(rx)]])


def RotateY(dy: float):
    """    Rotate about Y axis in Degrees"""
    ry = DegToRad(dy)
    return torch.tensor([[np.cos(ry), 0, np.sin(ry)],
                         [0, 1, 0],
                         [-np.sin(ry), 0, np.cos(ry)]])


def RotateZ(dz: float):
    """    Rotate about Z axis in Degrees"""
    rz = DegToRad(dz)
    return torch.tensor([[np.cos(rz), -np.sin(rz), 0],
                         [np.sin(rz), np.cos(rz), 0],
                         [0, 0, 1]])


def DegToRad(deg: float):
    """ Conversion from degrees to radians"""
    return deg * 2 * np.pi / 360


def RemoveLB(cloud: torch.Tensor, lx: float, ly: float, lz: float):
    """" Remove all values below the lower bounds in x, y and z"""
    lower_bound = torch.tensor([lx, ly, lz])
    indices_lower = torch.all(cloud[:, :3] >= lower_bound, dim=1)
    filtered_points = cloud[indices_lower]
    return filtered_points


def RemoveUB(cloud: torch.Tensor, ux: float, uy: float, uz: float):
    """" Remove all values above the upper bounds in x, y and z"""
    upper_bound = torch.tensor([ux, uy, uz])
    indices_upper = torch.all(cloud[:, :3] <= upper_bound, dim=1)
    filtered_points = cloud[indices_upper]
    return filtered_points


def Remove2Norm(cloud: torch.Tensor, radius: float, centre_xy=torch.tensor([0, 0])):
    """Remove all points which are outside the 2-Norm in the xy plane centred at centre_xy"""
    two_norm = torch.tensor([radius])
    indices_2norm = torch.all((cloud[:, :2] - centre_xy).norm(p=2, dim=1).view(-1, 1) < two_norm, dim=1)
    filtered_points = cloud[indices_2norm]
    return filtered_points


def ReflectX(cloud: torch.Tensor, x: float):
    distance = cloud[:, 0] - x
    new_points = cloud.clone()
    new_points[:, 0] = new_points[:, 0] - 2 * distance
    return torch.cat([cloud, new_points], dim=0)


def ReflectY(cloud: torch.Tensor, y: float):
    # distance to y point
    distance = cloud[:, 1] - y
    new_points = cloud.clone()
    new_points[:, 1] = new_points[:, 1] - 2 * distance
    return torch.cat([cloud, new_points], dim=0)


def ReflectZ(cloud: torch.Tensor, z: float):
    # distance to z point
    distance = cloud[:, 2] - z
    new_points = cloud.clone()
    new_points[:, 2] = new_points[:, 2] - 2 * distance
    return torch.cat([cloud, new_points], dim=0)


def CentreAtOrigin(cloud: torch.Tensor):
    cloud[:, :3] = cloud[:, :3] - cloud[:, :3].mean(dim=0)
    return cloud
