import os

import numpy as np


def generate_samples(cloud: np.array, no_samples: int, sd: float, dir: str, start_index: int):
    """ Duplicate samples to generate a dataset"""
    for i in range(start_index, start_index + no_samples):
        filename = dir + '_' + str(i).zfill(4) + '.csv'
        np.savetxt(filename, cloud, delimiter=',')


indir = os.getcwd()
parent = os.path.dirname(os.getcwd())
outdir = parent + '/datasets/VisionSegmented'

categories = ['baseball', 'baseball_symm', 'beer', 'camera_box', 'soda_can', 'golf_ball', 'golf_ball_symm', 'orange',
              'pack_of_cards', 'rubix_cube', 'rugby_ball', 'shampoo', 'spam', 'tape']

for cat in categories:
    input_path = indir + '/segmented_points/' + cat + '.csv'
    cloud = np.loadtxt(input_path)
    train_folder = outdir + '/' + cat + '/train'
    test_folder = outdir + '/' + cat + '/test'
    os.makedirs(train_folder)
    os.makedirs(test_folder)

    train_path = train_folder + '/' + cat
    test_path = test_folder + '/' + cat

    generate_samples(cloud=cloud, no_samples=200, sd=0, dir=train_path, start_index=1)
    generate_samples(cloud=cloud, no_samples=84, sd=0, dir=test_path, start_index=201)
