import os

import matplotlib.pyplot as plt
import numpy as np
import torch
from ply import read_ply

from recognition.VisualiseCloud import fix_3D_aspect
from unified_representation.voxel_filter import voxel_filter
from .CloudProcessing import RotateX

categories = ['baseball', 'beer', 'camera_box', 'coke_can', 'golf_ball',
              'orange', 'pack_of_cards', 'rubix_cube', 'rugby_ball',
              'shampoo', 'spam', 'tape']

no_categories = len(categories)
cwd = os.getcwd()
filenames = [cwd + '/raw_points/' + cat + '.ply' for cat in categories]
fig = plt.figure()
plt.rcParams.update({'font.size': 12, 'figure.dpi': 400})

fig.suptitle("Unprocessed pointclouds")
Rx = RotateX(121.8 * 2 * np.pi / 360)
for idx, filename in enumerate(filenames, start=1):
    ply = read_ply(filename)
    df = ply['points']
    np_arr = df.to_numpy()
    points = np_arr[:, :3]
    # Downsample to enable visualisation
    fpoints = voxel_filter(torch.from_numpy(points).matmul(Rx), resolution=40)
    ax = fig.add_subplot(4, 3, idx, projection='3d')
    colours = np.abs(np_arr[:, 6:9]) / 256
    ax.scatter(fpoints[:, 0], fpoints[:, 1], fpoints[:, 2], s=2, c=fpoints[:, 2])

    category_name = ' '.join(word.title() for word in categories[idx - 1].split('_'))
    ax.set(title=category_name + '   # Points: ' + str(points.shape[0]))
    fix_3D_aspect(ax, fpoints)
plt.show()
