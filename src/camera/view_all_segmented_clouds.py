import os

import matplotlib.pyplot as plt
import numpy as np
import torch

from unified_representation.voxel_filter import voxel_filter

categories = ['baseball', 'baseball_symm', 'beer', 'camera_box', 'coke_can', 'golf_ball', 'golf_ball_symm', 'orange',
              'pack_of_cards', 'rubix_cube', 'rugby_ball', 'shampoo', 'spam', 'tape']
show_rgb = False
no_categories = len(categories)
cwd = os.getcwd()
dir = '/segmented_points_rgb/' if show_rgb else '/segmented_points/'
filenames = [cwd + dir + cat + '.csv' for cat in categories]
fig = plt.figure()
plt.rcParams.update({'font.size': 13, 'figure.dpi': 400})
fig.suptitle("Cleaned pointclouds")
plt.rcParams.update({'font.size': 12, 'figure.dpi': 400})
for idx, filename in enumerate(filenames, start=1):
    np_arr = np.loadtxt(filename)
    points = np_arr
    ax = fig.add_subplot(4, 4, idx, projection='3d')
    if show_rgb:
        fpoints = torch.from_numpy(points)
        colours = fpoints[:, -3:].numpy()
        ax.scatter(fpoints[:, 0], fpoints[:, 1], fpoints[:, 2], s=2, c=colours)
    else:
        fpoints = voxel_filter(torch.from_numpy(points), resolution=40)
        colours = fpoints[:, 2]
        ax.scatter(fpoints[:, 0], fpoints[:, 1], fpoints[:, 2], s=2, c=colours)

    category_name = ' '.join(word.title() for word in categories[idx - 1].split('_'))
    ax.set(title=category_name + '   # Points: ' + str(points.shape[0]))
    fix
# plt.tight_layout()
plt.show()
