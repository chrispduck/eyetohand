import matplotlib.pyplot as plt
import numpy as np


def generate_tactile_cylinder(r: float, h: float, xstep: float = 0.35, zstep: float = 0.4, noise_radii: float = 0.7,
                              z_offset: float = 0):
    """
    Generate points from a tactile-like cylinder aligned with the z axis. Calculations in cylindrical polar

    Args:
        r: (float) radius of cylinder
        h: (float) height of the cylinder
        xstep: (float) spacing between x points
        zstep: (float) spacing between z points
        noise_radii: (float) Radius from which planar white noise is generated
        z_offset: (float) z direction offset applied to all points

    Returns:
       points: (np.ndarray)

    """

    nb_points_circle = np.ceil(2 * np.pi * r / xstep)
    nb_layers_vert = np.floor(h / zstep)
    dtheta = 2 * np.pi / nb_points_circle
    dh = h / nb_layers_vert
    cylinder = np.empty((0, 3))

    for j in range(int(nb_layers_vert) + 1):
        for i in range(int(nb_points_circle) + 1):
            theta = i * dtheta
            r_hat = np.array([np.cos(theta), np.sin(theta)])
            theta_hat = np.array([[-np.sin(theta), np.cos(theta)]])
            tang_eror = noise_radii * (np.random.uniform() - 0.5)
            XY = r * r_hat + tang_eror * theta_hat  # type np.ndarray
            z = np.array([j * dh])
            data_point = np.array([np.concatenate([*XY, z], axis=0)])
            cylinder = np.concatenate((cylinder, data_point), axis=0)
    cylinder = cylinder - np.mean(cylinder, axis=0)
    cylinder[:, 2] = cylinder[:, 2] + z_offset
    return cylinder


def generate_tactile_tapered_cylinder(r1: float, r2: float, h: float, xstep: float = 0.35, zstep: float = 0.4,
                                      z_offset: float = 0.):
    """
    Generate a tapered portion of a cylinder.
    Args:
        r1: (float) outer radius
        r2: (float) inner radius
        h: (float) height
        xstep: (float) spacing between x points
        zstep: (float) spacing between z points
        z_offset: (float) z direction offset applied to all points

    Returns:
        points: (np.ndarray)
    """
    circum = 2 * np.pi * (r1 + r2) / 2
    nb_points_circle = np.ceil(2 * np.pi * (r1 + r2) / (2 * xstep))
    nb_points_vert = np.floor(h / zstep)
    dtheta = 2 * np.pi / nb_points_circle
    dh = h / nb_points_vert
    cylinder = np.empty((0, 3))

    for j in range(int(nb_points_vert) + 1):
        r = ((j / nb_points_vert * r2) + (1 - j / nb_points_vert) * r1)
        for i in range(int(nb_points_circle) + 1):
            theta = i * dtheta
            r_hat = np.array([np.cos(theta), np.sin(theta)])
            theta_hat = np.array([[-np.sin(theta), np.cos(theta)]])
            tang_eror = 0.7 * (np.random.uniform() - 0.5)
            XY = r * r_hat + tang_eror * theta_hat  # type np.ndarray
            z = np.array([j * dh])
            data_point = np.array([np.concatenate([*XY, z], axdis=0)])
            cylinder = np.concatenate((cylinder, data_point), axis=0)
    cylinder = cylinder - np.mean(cylinder, axis=0)
    cylinder[:, 2] = cylinder[:, 2] + z_offset

    return cylinder


def generate_tactile_spherically_curved_cylinder(r1: float, r2: float, h: float, xstep=0.35, zstep=0.4,
                                                 z_offset: float = 0.):
    """
    Generates the curved surface for a tapered cylinder
    Args:
        r1: outer radii
        r2: inner radii
        h: height of section
        xstep: step in x dirn
        zstep: step in z dirn
        z_offset: offset to all points in z dirn

    Returns:

    """
    circum = 2 * np.pi * (r1 + r2) / 2
    nb_points_circle = np.ceil(2 * np.pi * (r1 + r2) / (2 * xstep))
    nb_points_vert = np.floor(h / zstep)
    dtheta = 2 * np.pi / nb_points_circle
    dh = h / nb_points_vert
    cylinder = np.empty((0, 3))
    R = r2 - r1
    for j in range(int(nb_points_vert + 1)):
        current_h = dh * j
        r = (((j / nb_points_vert * r2) ** 0.5 + ((1 - j / nb_points_vert) * r1)) ** 0.5) ** 2
        for i in range(int(nb_points_circle) + 1):
            theta = i * dtheta
            r_hat = np.array([np.cos(theta), np.sin(theta)])
            theta_hat = np.array([[-np.sin(theta), np.cos(theta)]])
            tang_eror = 0.7 * (np.random.uniform() - 0.5)
            XY = r * r_hat + tang_eror * theta_hat  # type np.ndarray
            z = np.array([j * dh])
            data_point = np.array([np.concatenate([*XY, z], axis=0)])
            cylinder = np.concatenate((cylinder, data_point), axis=0)
    cylinder = cylinder - np.mean(cylinder, axis=0)
    cylinder[:, 2] = cylinder[:, 2] + z_offset

    return cylinder


if __name__ == '__main__':
    pepsi_lower = generate_tactile_cylinder(r=3.3, h=9.3)
    pepsi_upper = generate_tactile_tapered_cylinder(r1=3.3, r2=2.7, h=1.5, z_offset=9.3 / 2)
    pepsi = np.concatenate((pepsi_lower, pepsi_upper), axis=0)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.scatter(pepsi[:, 0], pepsi[:, 1], pepsi[:, 2])
    plt.show()

    shampoo_upper = generate_tactile_cylinder(r=2.7 / 2, h=2.7, z_offset=(13.4 + 2.7 + 2.2) / 2)
    shampoo_middle = generate_tactile_spherically_curved_cylinder(r1=5.5 / 2, r2=2.7 / 2, h=2.2, z_offset=7.8)
    shampoo_lower = generate_tactile_cylinder(r=5.5 / 2, h=13.4, z_offset=0)
    shampoo = np.concatenate((shampoo_upper, shampoo_middle, shampoo_lower), axis=0)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.scatter(shampoo[:, 0], shampoo[:, 1], shampoo[:, 2])
    plt.show()

    beer_upper = generate_tactile_tapered_cylinder(r1=3.3, r2=2.65, h=1.7, z_offset=(12.4 + 1.7) / 2)
    beer_lower = generate_tactile_cylinder(r=3.3, h=12.4)
    beer = np.concatenate((beer_lower, beer_upper), axis=0)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.scatter(beer[:, 0], beer[:, 1], beer[:, 2])
    plt.show()

    tape = generate_tactile_cylinder(r=10.1 / 2, h=4.9)
    tape_outer = tape.copy()
    tape_inner = generate_tactile_cylinder(r=7.6 / 2, h=4.9)
    tape_with_inner = np.concatenate((tape_outer, tape_inner), axis=0)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.scatter(tape_with_inner[:, 0], tape_with_inner[:, 1], tape_with_inner[:, 2])
    plt.show()
