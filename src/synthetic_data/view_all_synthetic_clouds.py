import os

import matplotlib.pyplot as plt
import numpy as np
import torch

from recognition.VisualiseCloud import fix_3D_aspect
from unified_representation.voxel_filter import voxel_filter

categories = ['beer', 'baseball', 'baseball_symm', 'camera_box', 'orange', 'golf_ball', 'golf_ball_symm',
              'pack_of_cards', 'shampoo', 'soda_can', 'spam', 'rubix_cube', 'rugby_ball', 'tape', 'tape_with_inner']
no_categories = len(categories)
parent = os.path.dirname(os.getcwd())
dir = parent + '/datasets/TactileUnprocessed/'
filenames = [dir + cat + '/train/' + cat + '_0001.csv' for cat in categories]
fig = plt.figure()
plt.rcParams.update({'font.size': 13, 'figure.dpi': 400})
fig.suptitle("Tactile pointclouds")
plt.rcParams.update({'font.size': 12, 'figure.dpi': 400})

for idx, filename in enumerate(filenames, start=1):
    points = np.loadtxt(filename, delimiter=',')
    ax = fig.add_subplot(4, 4, idx, projection='3d')
    plt.grid(False)
    fpoints = voxel_filter(torch.from_numpy(points), resolution=40)
    colours = fpoints[:, 2]
    ax.scatter(fpoints[:, 0], fpoints[:, 1], fpoints[:, 2], s=2, c=colours)
    category_name = ' '.join(word.title() for word in categories[idx - 1].split('_'))
    ax.set(title=category_name + '   # Points: ' + str(points.shape[0]))
    ax.set(xlabel='x (cm)', ylabel='y (cm)', zlabel='z (cm)')
    # Set scales
    fix_3D_aspect(ax, fpoints)
plt.tight_layout()
plt.show()
