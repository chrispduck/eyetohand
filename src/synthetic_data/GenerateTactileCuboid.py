import matplotlib.pyplot as plt

from camera.CloudProcessing import *


def generate_surface(x, z, xstep=0.35, zstep=0.4, deg=2, noise_sd=0.025, p_accept=0.9, local_samples=4):
    """
    Generate a planar surface to emulate the tactile generation process. This i) generates a grid of points ii) rotates
    the points x degrees. iii) rejects samples with probability 1-p. iv) Generates local samples from the remaining points
    by adding in-plane noise, further to jitter noise.
    Args:
        x: (float) width of surface
        z: (float)  height of surface
        xstep: (float) spacing of points in x dirn
        zstep: (float) spacing of points in z dirn
        deg: (float) Rotation in degrees
        noise_sd: standard devivation of the noise added to generate local samples
        p_accept: probability of a point not being rejected
        local_samples: number of local samples generated from each non-rejected grid point

    Returns:

    """
    # Create a grid of points
    x_r = np.arange(-x / 2, x / 2, xstep)
    z_r = np.arange(-z / 2, z / 2, zstep)
    xx, zz = np.meshgrid(x_r, z_r)
    X = xx.reshape(-1, 1)
    Y = np.zeros_like(X)
    Z = zz.reshape(-1, 1)
    face = np.concatenate([X, Y, Z], axis=1)
    theta = deg * np.pi / 180
    Rz = RotateZ(theta)
    face = np.matmul(face, Rz.numpy())

    # Reject some samples
    indices = np.random.choice(face.shape[0], size=int(np.random.uniform(face.shape[0]) > p_accept), replace=False)
    face = face[indices, :]

    # Add noise to the face
    noise = np.random.normal(0, scale=noise_sd, size=face.shape)
    final_face = face.copy() + noise
    for _ in range(local_samples):
        new_face = face
        new_face[:, 0] += np.random.normal(0, scale=noise_sd, size=face.shape[0])
        new_face[:, 2] += np.random.normal(0, scale=noise_sd, size=face.shape[0])
        final_face = np.concatenate([final_face, new_face], axis=0)
    final_face[:, 1] = np.zeros_like(final_face[:, 0]) + np.random.normal(0, scale=noise_sd / 5,
                                                                          size=final_face.shape[0])
    return final_face


def generate_cuboid(w: float, d: float, h: float):
    """
    Create a tactile-like cuboid
    Args:
        w: (float) width
        d: (float) depth
        h: (float) height

    Returns:
        cuboid: (np.ndarray)
    """
    front = generate_surface(w, h)
    front[:, 1] += d / 2
    back = generate_surface(w, h)
    back[:, 1] -= d / 2
    lside = generate_surface(d, h)
    lside = np.matmul(lside, RotateZ(90))
    lside[:, 0] -= w / 2
    rside = lside.clone()
    rside[:, 0] += w
    cuboid = np.concatenate([front, back, lside, rside])
    return cuboid


if __name__ is '__main__':
    cloud = generate_cuboid(5, 5, 5)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.scatter(cloud[:, 0], cloud[:, 1], cloud[:, 2], s=3)
    plt.show()
