import os
from typing import Callable

import numpy as np


def generate_samples(obj_name: str, obj_generator: Callable[[object], np.ndarray], dataset_name: str, nb_train: int,
                     nb_test: int, dataset_dir='/home/chris/Documents/4yp/datasets/'):
    """
    Generate samples of objects and saves under 'train' and 'test' folders as a .CSV
        e.g. dataset_dir/datasets/object1/train/object1_0001.csv
    Args:
        obj_name: (str) object name
        obj_generator: generator object which produces a sample when called.
        dataset_name: (str) save name for the dataset
        nb_train: (int)  number of training samples
        nb_test:   (int) number of test samples
        dataset_dir:  (str) directory in which to save the samples

    Returns:

    """
    nb_points = []
    train_dir = dataset_dir + dataset_name + '/' + obj_name + '/train/'
    test_dir = dataset_dir + dataset_name + '/' + obj_name + '/test/'
    os.makedirs(train_dir)
    os.makedirs(test_dir)
    for idx in range(1, 1 + nb_train):
        sample = obj_generator()
        sample = sample - np.mean(sample, axis=0)
        nb_points.append(sample.shape[0])
        filename = train_dir + obj_name + '_' + str(idx).zfill(4) + '.csv'
        np.savetxt(filename, sample, delimiter=',')

    for idx in range(1 + nb_train, 1 + nb_train + nb_test):
        sample = obj_generator()
        sample = sample - np.mean(sample, axis=0)
        nb_points.append(sample.shape[0])
        filename = test_dir + obj_name + '_' + str(idx).zfill(4) + '.csv'
        np.savetxt(filename, sample, delimiter=',')
    return nb_points
