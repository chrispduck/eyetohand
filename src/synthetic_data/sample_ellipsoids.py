import os

from .GenerateSamples import generate_samples
from .GenerateTactileEllipsoid import rejection_sampling_ellipsoid

parent = os.path.dirname(os.getcwd())
dataset_dir = parent + '/datasets/TactileUnprocessed/'
nb_train = 200
nb_test = 86


class OrangeGenerator:
    def __call__(self):
        orange = rejection_sampling_ellipsoid(a=7.7 / 2, b=7.7 / 2, c=8.6 / 2, t=0.15, nb_points=2000)
        return orange[orange[:, 2] > -2.87]


class RugbyBallGenerator:
    def __call__(self):
        rugby_ball = rejection_sampling_ellipsoid(a=10.7 / 2, b=10.7 / 2, c=16 / 2, t=0.15, nb_points=4000)
        return rugby_ball[rugby_ball[:, 2] > -3]


orange_factory = OrangeGenerator()
rugby_ball_factory = RugbyBallGenerator()
generate_samples('orange', orange_factory, 'TactileUnprocessed', nb_train=nb_train, nb_test=nb_test,
                 dataset_dir=dataset_dir)
generate_samples('rugby_ball', rugby_ball_factory, 'TactileUnprocessed', nb_train=nb_train, nb_test=nb_test,
                 dataset_dir=dataset_dir)
