import matplotlib.pyplot as plt
import numpy as np


def rejection_sampling_ellipsoid(a, b, c, t, nb_points=2000):
    """
        Generate an ellipsoid of thickness t through rejection sampling
    Args:
        a: x scaling
        b: y scaling
        c: z scaling
        t: thickness
        nb_points: number of points

    Returns:
        points: (np.ndarray)
    """
    al, bl, cl = a - t / 2, b - t / 2, c - t / 2
    au, bu, cu = a + t / 2, b + t / 2, c + t / 2

    points = np.empty((0, 3))
    while points.shape[0] < nb_points:
        point_unit_cube = np.random.uniform(size=(1, 3))
        point_cube = point_unit_cube - np.array([0.5, 0.5, 0.5])
        point = point_cube * np.array([2 * au, 2 * bu, 2 * cu])
        if calculate_ellipsoid(point[0], al, bl, cl) > 1 and calculate_ellipsoid(point[0], au, bu, cu) < 1:
            points = np.concatenate((point, points), axis=0)
    return points


def calculate_ellipsoid(point, a, b, c):
    """ Return the scalar value from the ellipsoid equation (x/a)**2 + (y/b)**2 + (z/c)*2)"""
    return (point[0] / a) ** 2 + (point[1] / b) ** 2 + (point[2] / c) ** 2


if __name__ == '__main__':
    ellipsoid = rejection_sampling_ellipsoid(a=7.7 / 2, b=7.7 / 2, c=8.6 / 2, t=0.15)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.scatter(ellipsoid[:, 0], ellipsoid[:, 1], ellipsoid[:, 2])
    fpoints = ellipsoid
    X = fpoints[:, 0]
    Y = fpoints[:, 1]
    Z = fpoints[:, 2]
    max_range = np.array([X.max() - X.min(), Y.max() - Y.min(), Z.max() - Z.min()]).max() / 2.0
    mid_x = (X.max() + X.min()) * 0.5
    mid_y = (Y.max() + Y.min()) * 0.5
    mid_z = (Z.max() + Z.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)
    plt.show()
