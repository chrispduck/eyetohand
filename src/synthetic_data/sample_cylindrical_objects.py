import os

from .GenerateSamples import generate_samples
from .GenerateTactileCylinder import *

nb_train = 200
nb_test = 86
parent = os.path.dirname(os.getcwd())
dataset_dir = parent + '/datasets/TactileUnprocessed/'


class CokeCanGenerator:
    def __call__(self):
        coke_upper = generate_tactile_cylinder(r=3.3, h=9.3)
        coke_lower = generate_tactile_tapered_cylinder(r1=3.3, r2=2.7, h=1.5, z_offset=9.3 / 2)
        coke_can = np.concatenate((coke_upper, coke_lower), axis=0)
        return coke_can


class ShampooGenerator:
    def __call__(self):
        shampoo_upper = generate_tactile_cylinder(r=2.7 / 2, h=2.7, z_offset=(13.4 + 2.7 + 2.2) / 2)
        shampoo_middle = generate_tactile_spherically_curved_cylinder(r1=5.5 / 2, r2=2.7 / 2, h=2.2, z_offset=7.8)
        shampoo_lower = generate_tactile_cylinder(r=5.5 / 2, h=13.4, z_offset=0)
        shampoo = np.concatenate((shampoo_lower, shampoo_middle, shampoo_upper), axis=0)
        return shampoo


class BeerGenerator:
    def __call__(self):
        beer_upper = generate_tactile_tapered_cylinder(r1=3.3, r2=2.65, h=1.7, z_offset=(12.4 + 1.7) / 2)
        beer_lower = generate_tactile_cylinder(r=3.3, h=12.4)
        beer = np.concatenate((beer_lower, beer_upper), axis=0)
        return beer


class TapeGenerator:
    def __call__(self):
        tape = generate_tactile_cylinder(r=10.1 / 2, h=4.9)
        return tape


class TapeWithInnerGenerator:
    def __call__(self):
        tape_outer = generate_tactile_cylinder(r=10.1 / 2, h=4.9)
        tape_inner = generate_tactile_cylinder(r=7.6 / 2, h=4.9)
        tape_with_inner = np.concatenate((tape_outer, tape_inner), axis=0)
        return tape_with_inner


coke_can_factory = CokeCanGenerator()
shampoo_factory = ShampooGenerator()
beer_factory = BeerGenerator()
tape_factory = TapeGenerator()
tape_with_inner_factory = TapeWithInnerGenerator()

generate_samples(obj_name='soda_can', obj_generator=coke_can_factory, nb_train=nb_train, nb_test=nb_test,
                 dataset_dir=dataset_dir)
generate_samples(obj_name='shampoo', obj_generator=shampoo_factory, nb_train=nb_train, nb_test=nb_test,
                 dataset_dir=dataset_dir)
generate_samples(obj_name='beer', obj_generator=beer_factory, nb_train=nb_train, nb_test=nb_test,
                 dataset_dir=dataset_dir)
generate_samples(obj_name='tape', obj_generator=tape_factory, nb_train=nb_train, nb_test=nb_test,
                 dataset_dir=dataset_dir)
generate_samples(obj_name='tape_with_inner', obj_generator=tape_with_inner_factory, nb_train=nb_train,
                 nb_test=nb_test, dataset_dir=dataset_dir)
