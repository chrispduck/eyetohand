import os

from .GenerateSamples import generate_samples
from .GenerateTactileCuboid import generate_cuboid

parent = os.path.dirname(os.getcwd())
dataset_dir = parent + '/datasets/TactileUnprocessed/'
nb_train = 200
nb_test = 86


class CameraBoxGenerator:
    def __call__(self):
        camera_box = generate_cuboid(w=9.2, d=5.2, h=14.2)
        return camera_box


class PackOfCardsGenerator:
    def __call__(self):
        pack_of_cards = generate_cuboid(w=6.6, d=1.7, h=9.1)
        return pack_of_cards


class SpamGenerator:
    def __call__(self):
        spam = generate_cuboid(w=9.4, d=4.75, h=5.6)
        return spam


camera_factory = CameraBoxGenerator()
pack_of_cards_factory = PackOfCardsGenerator()
spam_factory = SpamGenerator()
generate_samples('camera_box', camera_factory, nb_train=nb_train, nb_test=nb_test, dataset_dir=dataset_dir)
generate_samples('pack_of_cards', pack_of_cards_factory, nb_train=nb_train, nb_test=nb_test, dataset_dir=dataset_dir)
generate_samples('spam', spam_factory, nb_train=nb_train, nb_test=nb_test, dataset_dir=dataset_dir)
