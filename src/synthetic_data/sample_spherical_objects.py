import os

from .GenerateSamples import generate_samples
from .GenerateTactileSphere import *

parent = os.path.dirname(os.getcwd())
dataset_dir = parent + '/datasets/TactileUnprocessed/'
nb_train = 200
nb_test = 86


class BaseballGenerator:
    def __call__(self):
        baseball = generate_tactile_sphere(r=7.35 / 2)
        baseball = baseball[baseball[:, 2] > 0]
        return baseball


class GolfBallGenerator:
    def __call__(self):
        golf_ball = generate_tactile_sphere(r=4.3 / 2)
        golf_ball = golf_ball[golf_ball[:, 2] > 0]
        return golf_ball


class BaseballSymmGenerator:
    def __call__(self):
        baseball = generate_tactile_sphere(r=7.35 / 2)
        return baseball


class GolfBallSymmGenerator:
    def __call__(self):
        golf_ball = generate_tactile_sphere(r=4.3 / 2)
        return golf_ball


baseball_factory = BaseballGenerator()
golf_ball_factory = GolfBallGenerator()
baseball_symm_factory = BaseballSymmGenerator()
golf_ball_symm_factory = GolfBallSymmGenerator()

generate_samples('baseball', baseball_factory, nb_train=nb_train, nb_test=nb_test, dataset_dir=dataset_dir)
generate_samples('golf_ball', golf_ball_factory, nb_train=nb_train, nb_test=nb_test, dataset_dir=dataset_dir)
generate_samples('baseball_symm', baseball_symm_factory, nb_train=nb_train, nb_test=nb_test, dataset_dir=dataset_dir)
generate_samples('golf_ball_symm', golf_ball_symm_factory, nb_train=nb_train, nb_test=nb_test, dataset_dir=dataset_dir)
