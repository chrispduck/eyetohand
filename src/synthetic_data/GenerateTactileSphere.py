import matplotlib.pyplot as plt
import numpy as np


# Conventions https://en.wikipedia.org/wiki/Spherical_coordinate_system
def generate_tactile_sphere(r, xstep=0.35, zstep=0.4, p_accept=0.9, noise_radii=0.7, samples_per_location=4):
    """
    Generate a tactile sphere in stages i) generate points spaced by xstep and ystep on the sphere. ii) Reject points
    with probability 1-p. iii) generate local samples by adding white noise
    """
    area_of_sphere = 4 * np.pi * (r ** 2)
    num_points = 0.25 * area_of_sphere / (xstep * zstep)
    n = num_points * 60
    A = np.empty((0, 3))
    N_count = 0
    a = 4 * np.pi * r ** 2 / n
    d = np.sqrt(a)
    M_theta = round(np.pi / d)
    d_theta = np.pi / M_theta
    d_phi = a / d_theta
    for m in range(int(M_theta)):
        theta = np.pi * (m + 0.5) / M_theta
        M_phi = round(2 * np.pi * np.sin(theta) / d_phi)
        for n in range(int(M_phi)):
            phi = 2 * np.pi * n / M_phi
            N_count += 1
            if np.random.normal() < p_accept:
                for _ in range(samples_per_location):
                    new_point = np.array(
                        [[r * np.sin(theta) * np.cos(phi), r * np.sin(theta) * np.sin(phi), r * np.cos(theta)]])
                    new_point = new_point + calculate_noise(theta, phi, noise_radii=0.7)
                    A = np.concatenate((A, new_point), axis=0)
    return A


def calculate_noise(theta, phi, noise_radii):
    """Calculate noise which is orthogonal to r_hat, and within the unit circle"""
    theta_hat = np.array([[np.cos(theta) * np.cos(phi), np.cos(theta) * np.sin(phi), -np.sin(theta)]
                          ])
    phi_hat = np.array([-np.sin(phi), np.cos(phi), 0])
    x1, x2 = random_unit_circle()
    noise = noise_radii / 2 * x1 * theta_hat + noise_radii / 2 * x2 * phi_hat
    return noise


def random_unit_circle():
    """Select a point uniformly from a unit circle"""
    t = 2 * np.pi * np.random.uniform()
    u = np.random.uniform() + np.random.uniform()
    r = 2 - u if u > 1 else u
    x, y = r * np.cos(t), r * np.sin(t)
    return x, y


if __name__ == '__main__':
    r = 7.35 / 2
    arr = generate_tactile_sphere(10)
    arr = arr[arr[:, 2] > 0]
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    print(arr.shape)
    ax.scatter(arr[:, 0], arr[:, 1], arr[:, 2])
    plt.show()
