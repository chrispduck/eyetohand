from PointFilters import MLS_Voxel_Filter
import pcl, numpy as np

cats = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'shampoo', 'spam', 'tape']
indirs = ["../datasets/single/vision/real/raw_chopped/", "../datasets/single/tactile/real/raw/"]
outdirs = ["../datasets/single/vision/real/MLS/", "../datasets/single/tactile/real/MLS/"]


filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)

for indir, outdir in zip(indirs, outdirs):
    for cat in cats:
        fname_in = indir + cat + '.csv'
        fname_out = outdir + cat + '.csv'
        points = np.loadtxt(fname_in, delimiter=',')
        cloud = pcl.PointCloud(points.astype('float32'))
        MLS_filtered_cloud = filter.apply_MLS(cloud).to_array()
        # print(fname_in, fname_out)
        np.savetxt(fname_out, MLS_filtered_cloud, delimiter=',')
