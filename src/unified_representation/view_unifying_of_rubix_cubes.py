import os

import matplotlib.pyplot as plt
import numpy as np
import pcl

from recognition.VisualiseCloud import fix_3D_aspect
from PointFilters import MLS_Voxel_Filter

# Select Plot
# 1 = Tactile unprocessed
# 2 = Tactile MLS Filter
# 3 = Tactile MLS & Voxel
# 4 = Vision unprocessed
# 5 = Vision MLS Filter
# 6 = Vision MLS & Voxel
plot_option = 6
plot_dim = '2d'  # '2d' or '3d'

plt.rcParams.update({'font.size': 16})

# Load rubix cube vision and tactile clouds
cwd = os.getcwd()
parent = os.path.dirname(os.getcwd())
dataset_dir = parent + '/datasets/'
dir_tactile_unprocessed = dataset_dir + 'TactileUnprocessed/rubix_cube/train/rubix_cube_0001.csv'
dir_vision_unprocessed = dataset_dir + 'VisionSegmented/rubix_cube/train/rubix_cube_0001.csv'
tpoints = np.loadtxt(dir_tactile_unprocessed, delimiter=',').astype('float32')
vpoints = np.loadtxt(dir_vision_unprocessed, delimiter=',').astype('float32')

# Create Filter
cloud_filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)
# Apply Moving least squares filter
t_MLS_points = cloud_filter.apply_MLS(pcl.PointCloud(tpoints)).to_array()
v_MLS_points = cloud_filter.apply_MLS(pcl.PointCloud(vpoints)).to_array()
# Apply Voxel Filter
t_MLS_vox_points = cloud_filter.apply_voxel_filter(pcl.PointCloud(t_MLS_points)).to_array()
v_MLS_vox_points = cloud_filter.apply_voxel_filter(pcl.PointCloud(v_MLS_points)).to_array()

clouds = [tpoints, t_MLS_points, t_MLS_vox_points, vpoints, v_MLS_points, v_MLS_vox_points]
points = clouds[plot_option - 1]

# Generate plots
fig = plt.figure()
if plot_dim is '3d':
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    scatter = ax.scatter(points[:, 0], points[:, 1], points[:, 2], c=points[:, 2], s=6)
    ax.set(xlabel='x (cm)', ylabel='y (cm)', zlabel='z (cm)')
    fix_3D_aspect(ax, points)
    cb = plt.colorbar(scatter, pad=0.1)
    cb.set_label('z (cm)')

elif plot_dim is '2d':
    ax = fig.add_subplot(1, 1, 1)
    scatter = ax.scatter(points[:, 0], points[:, 2], s=5)
    ax.set(xlabel='x (cm)', ylabel='z (cm)')
    ax.minorticks_on()
    ax.grid(b=True, which='major', linestyle='-', color='gray')
    ax.grid(b=True, which='minor', linestyle='--', color='gray')
    X = points[:, 0]
    Y = points[:, 2]
    max_range = np.array([X.max() - X.min(), Y.max() - Y.min()]).max() / 2.0
    mid_x = (X.max() + X.min()) * 0.5
    mid_y = (Y.max() + Y.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
plt.tight_layout()
# plt.axis('off')
plt.show()
