from PointFilters import MLS_Voxel_Filter
import numpy as np
import os 
cat = 'spam'
indir = '../datasets/single/tactile/real/raw/'
outdir = '../datasets/single/tactile/real/filtered/'
fname_in  = indir + cat + '.csv'
fname_out = outdir + cat + '.csv'
print(fname_out)

input_cloud = np.loadtxt(fname_in, delimiter=',')
filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)
filtered_cloud = filter(input_cloud)
print(input_cloud.shape, filtered_cloud.shape)
np.savetxt(fname=fname_out, X=filtered_cloud, delimiter=',')
