import os

import matplotlib.pyplot as plt
import numpy as np

cwd = os.getcwd()
parent = os.path.dirname(cwd)
vision_path = cwd + '/segmented_points/rubix_cube.csv'
tactile_path = parent + '/datasets/TactileUnprocessed/rubix_cube/train/rubix_cube_0001.csv'

tactile_points = np.loadtxt(tactile_path, delimiter=',')
vision_points = np.loadtxt(vision_path)
dotsize = 2
fig = plt.figure()

plt.rcParams.update({'font.size': 12, 'figure.dpi': 400})
fig.suptitle("Difference In Unprocessed Representations")
plt.rcParams.update({'font.size': 11, 'figure.dpi': 400})

ax = fig.add_subplot(1, 2, 1)
ax.scatter(vision_points[:, 0], vision_points[:, 2], c=vision_points[:, 1], s=dotsize, cmap='Blues')
ax.set(title='Vision Representation', xlabel='x', ylabel='y')
ax.grid(axis='both')
ax.set_aspect('equal')
plt.axis('off')

ax = fig.add_subplot(1, 2, 2)
ax.scatter(tactile_points[:, 0], tactile_points[:, 2], c=tactile_points[:, 1], s=dotsize, cmap='Blues')
ax.set(title='Tactile Representation', xlabel='x', ylabel='y')
ax.grid(axis='both')
ax.set_aspect('equal')
plt.axis('off')
plt.tight_layout()
plt.show()
