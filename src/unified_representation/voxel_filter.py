import os
import time

import numpy as np
import torch

from recognition.VisualiseCloud import plot_single_cloud


def voxel_filter(cloud: torch.Tensor, resolution: int = 100, device='cuda'):
    # Define the Voxelgrid
    # xmin, xmax, ymin, ymax, zmin, zmax
    t1 = time.time()
    no_points = cloud.shape[0]
    min = cloud.min(dim=0)[0]
    max = cloud.max(dim=0)[0]
    leafsizes = torch.sub(max, min).div(resolution)
    min_tensor = min.view(-1).repeat(no_points, 1)
    leaftensor = leafsizes.view(-1).repeat(no_points, 1)
    indices = torch.floor(((cloud - min_tensor) / leaftensor))

    unique = torch.unique(indices, dim=0)
    leaf_resized_tensor = leafsizes.view(-1).repeat(unique.shape[0], 1)

    filtered_cloud = min.view(-1).repeat(unique.shape[0], 1) + 0.5 * leaf_resized_tensor + unique * leaf_resized_tensor
    return filtered_cloud


if __name__ == '__main__':
    parent = os.path.dirname(os.getcwd())
    path = parent + '/datasets/TactileData/rubix_cube/train/rubix_cube_0001.csv'
    arr = np.loadtxt(path, delimiter=',')
    A = torch.from_numpy(arr)
    print(A.shape)
    plot_single_cloud(A)
    output = voxel_filter(A, resolution=10)
    plot_single_cloud(output)
