import os

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc

from recognition.VisualiseCloud import fix_3D_aspect

# categories = ['beer', 'baseball', 'baseball_symm', 'camera_box', 'orange', 'golf_ball', 'golf_ball_symm', 'pack_of_cards', 'shampoo', 'soda_can', 'spam', 'rubix_cube', 'rugby_ball', 'tape']
categories = ['beer', 'baseball', 'camera_box', 'orange', 'golf_ball', 'pack_of_cards']
# categories = ['shampoo', 'soda_can', 'spam',
#               'rubix_cube', 'rugby_ball', 'tape']

cat_offset = int(0)  # Set to 6 to view the second half of pointclouds
no_categories = len(categories)
parent = os.path.dirname(os.getcwd())
dataset_dir = parent + '/datasets/'
dir_tactile = dataset_dir + 'TactileProcessed/'
dir_vision = dataset_dir + 'VisionProcessed/'
tactile_filenames = [dir_tactile + cat + '/train/' + cat + '_0001.csv' for cat in categories]
vision_filenames = [dir_vision + cat + '/train/' + cat + '_0001.csv' for cat in categories]

# rc('font', **{'family': 'DejaVu Sans', 'serif': ['Computer Modern']})
rc('font', **{'family': 'DejaVu Sans', 'serif': ['Times']})

rc('text', usetex=True)
plt.rcParams.update({'font.size': 12})
fig = plt.figure()

for idx in range(1, len(categories) + 1, 1):
    cat = categories[idx - 1]
    vpoints = np.loadtxt(vision_filenames[idx - 1], delimiter=',')
    tpoints = np.loadtxt(tactile_filenames[idx - 1], delimiter=',')
    category_name = ' '.join(word.title() for word in categories[idx - 1].split('_'))
    vision_title = 'Vision ' + category_name + '\quad   \# Points: ' + str(vpoints.shape[0])
    tactile_title = 'Tactile ' + category_name + '\quad  \# Points: ' + str(tpoints.shape[0])
    # print(idx)
    for i, (plt_index, fpoints, title) in enumerate(
            zip([3 * np.floor((idx - 1) / 3) + idx, 3 * np.floor((idx - 1) / 3) + idx + 3], [vpoints, tpoints],
                [vision_title, tactile_title])):
        print(plt_index)
        ax = fig.add_subplot(4, 3, int(plt_index), projection='3d')
        plt.grid(False)
        if i is 0:
            ax.scatter(fpoints[:, 0], fpoints[:, 1], fpoints[:, 2], s=2, c=fpoints[:, 2])
        if i is 1:
            ax.scatter(fpoints[:, 0], fpoints[:, 1], fpoints[:, 2], s=2, c=fpoints[:, 2], cmap='magma')
        ax.set(title=title, xlabel='x (cm)', ylabel='y (cm)', zlabel='z (cm)')
        fix_3D_aspect(ax, fpoints)

plt.tight_layout()
plt.show()
