import os
from typing import Callable

import numpy as np

from PointFilters import MLS_Voxel_Filter


def convert_object(obj_name: str, in_dataset: str, out_dataset: str, cloud_filter: Callable, nb_train: int,
                   nb_test: int):
    """ A function to filter an input cloud and save the resulting pointcloud models as a .csv """
    parent = os.path.dirname(os.getcwd())
    dir_datasets = parent + '/datasets/'
    train_in_dir = dir_datasets + in_dataset + '/' + obj_name + '/train/'
    train_out_dir = dir_datasets + out_dataset + '/' + obj_name + '/train/'

    test_in_dir = dir_datasets + in_dataset + '/' + obj_name + '/test/'
    test_out_dir = dir_datasets + out_dataset + '/' + obj_name + '/test/'

    os.makedirs(train_out_dir)
    os.makedirs(test_out_dir)

    for idx in range(1, 1 + nb_train):
        filename_in = train_in_dir + obj_name + '_' + str(idx).zfill(4) + '.csv'
        cloud_in = np.loadtxt(filename_in, delimiter=',')
        filename_out = train_out_dir + obj_name + '_' + str(idx).zfill(4) + '.csv'
        cloud_out = cloud_filter(cloud_in)
        np.savetxt(filename_out, cloud_out, delimiter=',')

    for idx in range(1 + nb_train, 1 + nb_train + nb_test):
        filename_in = test_in_dir + obj_name + '_' + str(idx).zfill(4) + '.csv'
        cloud_in = np.loadtxt(filename_in, delimiter=',')
        filename_out = test_out_dir + obj_name + '_' + str(idx).zfill(4) + '.csv'
        cloud_out = cloud_filter(cloud_in)
        np.savetxt(filename_out, cloud_out, delimiter=',')

    return obj_name + ' complete'


nb_train, nb_test = 200, 84
MLS_voxel_filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)

# for each train and test sample in TactileUnprocessed
tactile_categories = ['beer', 'baseball', 'baseball_symm', 'camera_box', 'orange', 'golf_ball', 'golf_ball_symm',
                      'pack_of_cards', 'shampoo', 'soda_can', 'spam', 'rubix_cube', 'rugby_ball', 'tape',
                      'tape_with_inner']
for cat in tactile_categories:
    convert_object(obj_name=cat, in_dataset='TactileUnprocessed', out_dataset='TactileProcessed',
                   cloud_filter=MLS_voxel_filter,
                   nb_train=nb_train, nb_test=nb_test)

# # for each train and test sample in VisionSegmented
# vision_categories = ['baseball', 'baseball_symm', 'beer', 'camera_box', 'golf_ball', 'golf_ball_symm', 'orange',
#                      'pack_of_cards', 'rubix_cube', 'rugby_ball', 'shampoo', 'soda_can', 'spam', 'tape']
# for cat in vision_categories:
#     convert_object(obj_name=cat, in_dataset='VisionSegmented', out_dataset='VisionProcessed',
#                    cloud_filter=MLS_voxel_filter,
#                    nb_train=nb_train, nb_test=nb_test)
