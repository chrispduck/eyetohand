import os

import matplotlib.pyplot as plt
import numpy as np

from recognition.VisualiseCloud import fix_3D_aspect

categories = ['beer', 'baseball', 'camera_box', 'orange', 'golf_ball', 'pack_of_cards', 'shampoo', 'soda_can', 'spam',
              'rubix_cube', 'rugby_ball', 'tape']

no_categories = len(categories)
parent = os.path.dirname(os.getcwd())
dataset_dir = parent + '/datasets/'
dir_tactile = dataset_dir + 'TactileProcessed/'
dir_vision = dataset_dir + 'VisionProcessed/'
plt.rcParams.update({'font.size': 11})

for dir in [dir_tactile, dir_vision]:
    filenames = [dir + cat + '/train/' + cat + '_0001.csv' for cat in categories]
    fig = plt.figure()
    fig.suptitle("Tactile pointclouds")
    for idx, filename in enumerate(filenames, start=1):
        fpoints = np.loadtxt(filename, delimiter=',')
        ax = fig.add_subplot(4, 3, idx, projection='3d')
        plt.grid(False)
        colours = fpoints[:, 2]
        ax.scatter(fpoints[:, 0], fpoints[:, 1], fpoints[:, 2], s=2, c=colours)
        category_name = ' '.join(word.title() for word in categories[idx - 1].split('_'))
        ax.set(title=category_name + '   # Points: ' + str(fpoints.shape[0]))
        ax.set(xlabel='x (cm)', ylabel='y (cm)', zlabel='z (cm)')
        fix_3D_aspect(ax, fpoints)
    plt.tight_layout()
    plt.show()
