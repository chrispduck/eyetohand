import pandas as pd, os, numpy as np
from PointFilters import MLS_Voxel_Filter

cats = ['baseball', 'beer', 'camera_box', 'golf_ball', 'orange', 'pack_of_cards', 'rubix_cube', 'shampoo', 'spam',
        'tape']
indir = '../datasets/single/vision/real/raw/'
outdir = '../datasets/single/vision/real/filtered/'
# os.mkdir(outdir + 'train/')
# os.mkdir(outdir + 'test/')
filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)

for cat in cats:
    for split in ['train/', 'test/']:
        df = pd.read_csv(indir + split + cat + '.csv')
        print(df)
        arr = filter(df.to_numpy())
        np.savetxt(X=arr, fname=outdir+split+cat+'.csv', delimiter=',')
        print(df.shape, np.shape)
