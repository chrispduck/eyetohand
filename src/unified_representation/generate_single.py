# Filter a single pointcloud from a raw (at metre scale) tactile pointcloud
import os
from typing import Callable, Union
import pandas as pd
import torch
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from PointFilters import MLS_Voxel_Filter

def main():
    raw_dir = '../datasets/unfiltered_real_tactile/'
    # object_names = ['baseball', 'golf', 'pack_of_cards', 'tape']
    # object_names = ['rubix_cube', 'beer']
    object_names = ['orange', 'box', 'shampoo']

    filtered_dir = '../datasets/filtered_tactile_objs/'
    for object_name in object_names:
        create_filtered_csv(raw_dir, object_name, filtered_dir)

def create_filtered_csv(dataset_dir, object_name, out_dir):
    """
        Converts from metres to cm 
        Translates to origin
    """
    directory =  dataset_dir + object_name + '.csv'
    print(directory, os.getcwd())
    cube = pd.read_csv(directory)
    pcl_filter = MLS_Voxel_Filter(MLS_radius=0.5, leafsize=0.35)

    # print(cube)

    points = cube.loc[:,['x', 'y', 'z']].to_numpy()*100
    # print(type(points))

    filtered_points = pcl_filter(points)
    print("filtered points: ", filtered_points.shape)
    filtered_points = filtered_points - filtered_points.mean(axis=0)
    print(filtered_points.mean(axis=0))

    unique_before = np.unique(points, axis=0)
    unique_after = np.unique(filtered_points, axis=0)
    np.savetxt(fname= out_dir + object_name + '.csv', X=filtered_points, delimiter=',')
    print("unique_beofre:", unique_before.shape)
    print("unique_after:" , unique_after.shape)
    plot_comparison(points, filtered_points, object_name)

def plot_comparison(raw, filtered , fname):
    fig = plt.figure()
    for idx, (cloud, title) in enumerate(zip([raw, filtered], ['raw', 'filtered'])):
        ax = fig.add_subplot(1,2, idx +1, projection='3d')
        ax.scatter(cloud[:, 0], cloud[:, 1], cloud[:, 2], s=2, c=cloud[:,2], cmap='Blues')
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")
        ax.set_title(title) 
        fix_3D_aspect(ax, torch.Tensor(cloud))
    plt.savefig(fname)

def fix_3D_aspect(ax: plt.Axes, cloud: torch.Tensor):
    X = cloud[:, 0]
    Y = cloud[:, 1]
    Z = cloud[:, 2]
    [print(X.min(), X.max()) for X in [X, Y, Z]] 
    max_range = np.array([X.max() - X.min(), Y.max() - Y.min(), Z.max() - Z.min()]).max() / 2.0
    mid_x = (X.max() + X.min()) * 0.5
    mid_y = (Y.max() + Y.min()) * 0.5
    mid_z = (Z.max() + Z.min()) * 0.5
    print(mid_x, max_range)
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)



if __name__ == '__main__':
    main()